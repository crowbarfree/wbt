﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;

using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Windows.Markup;

namespace icu
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (1 > args.Length)
            {
                MessageBox.Show("Use file *.icd as first argument of command line!");
            }
            else if ("--check_xaml" == args[0])
            {
                CheckXaml(args);
            }
            else
            {
                string icd_file_path = args[0];
                WBT.CheckingsForm frm = new WBT.CheckingsForm();
                frm.Watch(icd_file_path);
                frm.ShowInTaskbar = true;
                Application.Run(frm);
            }
        }

        static void CheckXaml(string[] args)
        {
            string xaml_file_path= args[1];
            string file_path_prefix= xaml_file_path.Substring(0,xaml_file_path.Length - Path.GetExtension(xaml_file_path).Length);
            string icd_file_path= file_path_prefix + ".icd";
            string xps_file_path= file_path_prefix + ".xps";

            File.Delete(xps_file_path);
            using (XpsDocument document = new XpsDocument(xps_file_path, FileAccess.ReadWrite))
            {
                try
                {
                    using (XpsPackagingPolicy packagePolicy = new XpsPackagingPolicy(document))
                    using (XpsSerializationManager serializationMgr = new XpsSerializationManager(packagePolicy, false))
                    using (FileStream srcXamlStream = new FileStream(xaml_file_path, FileMode.Open))
                    {
                        object parsedDocObject = XamlReader.Load(srcXamlStream);
                        serializationMgr.SaveAsXaml(parsedDocObject);
                    }
                }
                finally
                {
                    document.Close();
                }
            }

            WBT.SignalingCheckings scheckings= new WBT.SignalingCheckings();
            scheckings.PreCheckXps(xps_file_path);

            WBT.Checkings checkings= new WBT.Checkings();
            checkings.Capacity= scheckings.Count;
            checkings.AddRange(scheckings.Select(c => new WBT.Checking(c)));
            checkings.FixFileNamesAsRelativePathForRootPath(Path.GetDirectoryName(icd_file_path));
            checkings.WriteToXmlFile(icd_file_path);
        }
    }
}
