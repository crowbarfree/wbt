﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.IO.Packaging;

namespace TestConsole
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if (0 == args.Length)
            {
            }
            else
            {
                string cmd= args[0];
                switch (cmd)
                {
                    case "xps_params":      TraceXpsParams(args[1]); break;
                    case "fc":              CheckBinaries(args[1],args[2]); break;
                    case "fc_xps":          WBT.XpsHelper.CompareXps(args[1],args[2]); break;
                    case "xps2png":         WBT.XpsHelper.SaveDocumentPagesToImages(args[1],args[2],args[3],args[4]); break;
                    case "fc_png":          WBT.ImageHelper.DiffPng(args[1],args[2],args[3]); break;
                    case "checkings_frm":   TestCheckingForms(); break;
                    case "log_frm":         TestLogForm(); break;
                    case "find_opened_xps": FindOpenedXps(args[1]); break;
                }
            }
        }

        static void TraceXpsParams(string fpath)
        {
            using (Package package = Package.Open(fpath, FileMode.Open))
            {
                PackageProperties props= package.PackageProperties;

                Console.WriteLine("props.Category= \"{0}\"",props.Category);
                Console.WriteLine("props.ContentStatus= \"{0}\"",props.ContentStatus);
                Console.WriteLine("props.ContentType= \"{0}\"",props.ContentType);
                Console.WriteLine("props.Created= \"{0}\"",props.Created);
                Console.WriteLine("props.Creator= \"{0}\"",props.Creator);
                Console.WriteLine("props.Description= \"{0}\"",props.Description);
                Console.WriteLine("props.Identifier= \"{0}\"",props.Identifier);
                Console.WriteLine("props.Keywords= \"{0}\"",props.Keywords);
                Console.WriteLine("props.Language= \"{0}\"",props.Language);
                Console.WriteLine("props.LastModifiedBy= \"{0}\"",props.LastModifiedBy);
                Console.WriteLine("props.LastPrinted= \"{0}\"",props.LastPrinted);
                Console.WriteLine("props.Modified= \"{0}\"",props.Modified);
                Console.WriteLine("props.Revision= \"{0}\"",props.Revision);
                Console.WriteLine("props.Subject= \"{0}\"",props.Subject);
                Console.WriteLine("props.Title= \"{0}\"",props.Title);
                Console.WriteLine("props.Version= \"{0}\"",props.Version);
            }
        }

        static void CheckBinaries(string fpath1, string fpath2)
        {
            using (Stream s1 = new FileStream(fpath1, FileMode.Open))
            using (Stream s2 = new FileStream(fpath2, FileMode.Open))
            {
                if (s1.Length != s2.Length)
                {
                    Console.WriteLine("({0}).Length({1})!=({2}).Length({3})",fpath1,s1.Length,fpath2,s2.Length);
                }
                else
                {
                    byte[] bytes1= new byte[s1.Length];
                    byte[] bytes2= new byte[s2.Length];
                    s1.Read(bytes1,0,bytes1.Length);
                    s2.Read(bytes2,0,bytes2.Length);
                    for (int i = 0; i < bytes1.Length; i++)
                    {
                        if (bytes1[i] != bytes2[i])
                        {
                            Console.WriteLine("({0})[{1}]={2} != ({3})[{4}]={5}",fpath1,i,bytes1[i],fpath2,i,bytes2[i]);
                        }
                    }
                }
            }
        }

        const string txtLog= @"17:28:56 - precall ""D:\work\vva\outsourcing\infotrust\cpw\work\src\wbt_tests\pre.bat""
17:28:58 - finished with code 0
17:28:58 - pre goto to ""index.php?r=site/index""
17:29:02 - web document completed
17:29:02 - cmd ""wait_click_full_text"" ""Тестовая организация""
17:29:03 - element with text ""Тестовая организация"" is clicked
17:29:03 - cmd ""wait_click_id"" ""auth-request""
17:29:03 - id auth-request is clicked
17:29:03 - cmd ""wait_text"" ""МедСервис""
17:29:04 - text ""МедСервис"" is present
17:29:04 - cmd ""sleep"" ""100""
17:29:04 - sleep 100 milliseconds
17:29:04 - cmd ""wait_text_disappeared"" ""Загрузка...""
17:29:04 - value ""Загрузка..."" is not present
17:29:04 - cmd ""shot_hide_class"" ""time-value""
17:29:04 - cmd ""shot_hide_class"" ""ui-pg-selbox""
17:29:04 - cmd ""shot_hide_attribute_value"" ""aria-describedby"" ""jqGrid_created_at""
17:29:04 - cmd ""shot_check_png"" ""first.png""
17:29:04 - take screenshot
17:29:04 - pre check screenshot
17:29:04 - # медосмотр расширенный
17:29:04 - cmd ""wait_click_text"" ""МедСервис""
17:29:04 - text ""МедСервис"" is clicked
17:29:04 - cmd ""wait_text"" ""Предварительные осмотры""
17:29:05 - text ""Предварительные осмотры"" is present
17:29:05 - cmd ""wait_click_class_if_present_class"" ""header-switch"" ""container header-slim""
17:29:05 - cmd ""sleep"" ""25""
17:29:05 - sleep 25 milliseconds
17:29:05 - cmd ""shot_check_png"" ""meo.png""
17:29:05 - take screenshot
17:29:05 - pre check screenshot
17:29:05 - # медосмотр сжатый
17:29:05 - cmd ""wait_click_class"" ""header-switch""
17:29:05 - class ""header-switch"" is clicked
17:29:05 - cmd ""sleep"" ""25""
17:29:05 - sleep 25 milliseconds
17:29:05 - cmd ""shot_check_png"" ""meo-small.png""
17:29:05 - take screenshot
17:29:06 - pre check screenshot
17:29:06 - cmd ""start_store_lines_as"" ""customer_fields_1""
17:29:06 - start store lines as ""customer_fields_1""
17:29:06 - store line >  type_id title              ""Рога и копыта""
17:29:06 - store line >  type_id ogrn               ""1111111111111""
17:29:06 - store line >  type_id okved              ""1, 2, 3, 4, 5""
17:29:06 - store line >
17:29:06 - store line >  click_id_value budget 1
17:29:06 - store line >
17:29:06 - store line >  type_id address            ""ул Пушкина, дом 1, кв 10""
17:29:06 - store line >  type_id address_city       ""Магадан""
17:29:06 - store line >  type_id address_province   ""Магаданская область""
17:29:06 - store line >
17:29:06 - store line >  type_id     head_position  ""джамахер""
17:29:06 - store line >  type_id     head_name      ""Каддафи Муамар""
17:29:06 - store line >  type_id employer_position  ""нач по кадрам""
17:29:06 - store line >  type_id employer_name      ""Лётчиков Анатолий Петрович""
17:29:06 - store line >  type_id    agent_position  ""бугор""
17:29:06 - store line >  type_id    agent_name      ""Трахтенберг Чёрт Анатольевич""
17:29:06 - stop stored lines
17:29:06 - cmd ""start_store_lines_as"" ""customer_fields_2""
17:29:06 - start store lines as ""customer_fields_2""
17:29:06 - store line >  type_id title              ""Чикагский чапельниковый завод""
17:29:06 - store line >  type_id ogrn               ""2222222111111""
17:29:06 - store line >  type_id okved              ""5, 6, 7""
17:29:06 - store line >
17:29:06 - store line >  click_id_value budget 0
17:29:06 - store line >
17:29:06 - store line >  type_id address            ""ул Ленина, дом 2, кв 3""
17:29:06 - store line >  type_id address_city       ""Пермь""
17:29:06 - store line >  type_id address_province   ""Пермьская область""
17:29:06 - store line >
17:29:06 - store line >  type_id     head_position  ""директор""
17:29:06 - store line >  type_id     head_name      ""Яковлев Яков Яковлевич""
17:29:06 - store line >  type_id employer_position  ""кадровик""
17:29:06 - store line >  type_id employer_name      ""Юминов Ю Ю""
17:29:06 - store line >  type_id    agent_position  ""бригадир""
17:29:06 - store line >  type_id    agent_name      ""Трещёткин Т Т""
17:29:06 - stop stored lines
17:29:06 - ####################
17:29:06 - # организации
17:29:06 - ####################
17:29:06 - cmd ""start_store_lines_as"" ""test_customers""
17:29:06 - start store lines as ""test_customers""
17:29:06 - store line >  # добавить организацию
17:29:06 - store line >    wait_click_text ""Добавить организацию""
17:29:06 - store line >    wait_text ""Новая организация""
17:29:06 - store line >    shot_check_png add-medical-customer.png
17:29:06 - store line >
17:29:06 - store line >    play_stored_lines customer_fields_1
17:29:06 - store line >    shot_check_png add-medical-customer-filled.png
17:29:06 - store line >
17:29:06 - store line >    wait_click_text Cохранить
17:29:06 - store line >    wait_text ""Добавить организацию""
17:29:06 - store line >    shot_check_png medical-customer-added.png
17:29:06 - store line >
17:29:06 - store line >  # редактировать организацию
17:29:06 - store line >    click_text ""Рога и копыта""
17:29:06 - store line >    wait_click_text ""Редактировать организацию""
17:29:06 - store line >    wait_text ""Данные организации""
17:29:06 - store line >
17:29:06 - store line >    check_stored_lines customer_fields_1
17:29:06 - store line >
17:29:06 - store line >    play_stored_lines customer_fields_2
17:29:06 - store line >    shot_check_png add-medical-customer-edited.png
17:29:06 - store line >
17:29:06 - store line >    wait_click_text Cохранить
17:29:06 - store line >    wait_text ""Добавить организацию""
17:29:06 - store line >    shot_check_png medical-customer-edited-list.png
17:29:06 - store line >    click_text ""Чикагский чапельниковый завод""
17:29:06 - store line >    wait_click_text ""Редактировать организацию""
17:29:06 - store line >    wait_text ""Данные организации""
17:29:06 - store line >
17:29:06 - store line >    check_stored_lines customer_fields_2
17:29:06 - store line >
17:29:06 - store line >    click_text ""Вернуться к списку организаций без сохранения""
17:29:06 - store line >    shot_check_png medical-customer-edited-list.png
17:29:06 - store line >
17:29:06 - store line >  # удалить организацию
17:29:06 - store line >    click_text ""Чикагский чапельниковый завод""
17:29:06 - store line >    wait_click_text ""Удалить организацию""
17:29:06 - store line >    wait_text_disappeared ""Чикагский чапельниковый завод""
17:29:06 - store line >    shot_check_png meo-small.png
17:29:06 - stop stored lines
17:29:06 - cmd ""start_store_lines_as"" ""inspection_fields_1""
17:29:06 - start store lines as ""inspection_fields_1""
17:29:06 - store line >  type_id       title             ""Осмотр 2014 года""
17:29:06 - store line >  type_id       date              ""08.05.2014""
17:29:06 - store line >  set_value_id  status            ""a""
17:29:06 - store line >  type_id       description       ""Обычный годовой осмотр""
17:29:06 - store line >  type_id       conclusion        ""Пока нет никаких комментариев""
17:29:06 - stop stored lines
17:29:06 - cmd ""start_store_lines_as"" ""inspection_fields_2""
17:29:06 - start store lines as ""inspection_fields_2""
17:29:06 - store line >  type_id       title             ""Осмотр 2013 года""
17:29:06 - store line >  type_id       date              ""08.05.2013""
17:29:06 - store line >  set_value_id  status            ""n""
17:29:06 - store line >  type_id       description       ""Необычный годовой осмотр""
17:29:06 - store line >  type_id       conclusion        ""не помню""
17:29:06 - stop stored lines
17:29:06 - ####################
17:29:06 - # осмотры
17:29:06 - ####################
17:29:06 - cmd ""start_store_lines_as"" ""test_inspections""
17:29:06 - start store lines as ""test_inspections""
17:29:06 - store line >  # добавить осмотр
17:29:06 - store line >    double_click_id 2
17:29:06 - store line >    wait_text ""Добавить осмотр""
17:29:06 - store line >    shot_check_png inspections-start.png
17:29:06 - store line >    click_text ""Добавить осмотр""
17:29:06 - store line >    wait_text ""Данные осмотра""
17:29:06 - store line >    sleep 25
17:29:06 - store line >    shot_check_png inspection-add.png
17:29:06 - store line >    play_stored_lines inspection_fields_1
17:29:06 - store line >    click_text ""Cохранить""
17:29:06 - store line >    wait_text ""Добавить осмотр""
17:29:06 - store line >    shot_check_png inspection-added.png
17:29:07 - store line >
17:29:07 - store line >  # редактировать осмотр
17:29:07 - store line >    click_text ""Осмотр 2014 года""
17:29:07 - store line >    click_text ""Редактировать осмотр""
17:29:07 - store line >    wait_text ""Данные осмотра""
17:29:07 - store line >    sleep 25
17:29:07 - store line >    shot_check_png inspection-edit.png
17:29:07 - store line >    check_stored_lines inspection_fields_1
17:29:07 - store line >    play_stored_lines inspection_fields_2
17:29:07 - store line >    click_text ""Cохранить""
17:29:07 - store line >    wait_text ""Добавить осмотр""
17:29:07 - store line >    shot_check_png inspection-edited.png
17:29:07 - store line >
17:29:07 - store line >  # удалить осмотр
17:29:07 - stop stored lines
17:29:07 - cmd ""start_store_lines_as"" ""record_fields_1""
17:29:07 - start store lines as ""record_fields_1""
17:29:07 - store line >  type_id last_name             ""Шариков""
17:29:07 - store line >  type_id first_name            ""Полиграф""
17:29:07 - store line >  type_id middle_name           ""Полиграфович""
17:29:07 - store line >
17:29:07 - store line >  type_id birthday              ""17.05.1977""
17:29:07 - store line >  click_id_value sex m
17:29:07 - store line >
17:29:07 - store line >  type_id snils                 ""11111111111""
17:29:07 - store line >
17:29:07 - store line >  set_value_id disability       ""1""
17:29:07 - store line >
17:29:07 - store line >  type_id speciality            ""слесарь""
17:29:07 - store line >  type_id appointment           ""главный слесарь""
17:29:07 - store line >  type_id department            ""слесарня""
17:29:07 - store line >
17:29:07 - store line >  set_value_id experienceYears  2
17:29:07 - store line >  set_value_id experienceMonths 3
17:29:07 - store line >
17:29:07 - store line >  type_id passport_series       ""2222""
17:29:07 - store line >  type_id passport_number       ""333333""
17:29:07 - store line >  type_id passport_date         ""17.05.2002""
17:29:07 - store line >  type_id passport_office       ""Ленинским РОВД""
17:29:07 - store line >
17:29:07 - store line >  type_id insurance_number      ""444444""
17:29:07 - store line >  type_id insurance_company     ""МРСК""
17:29:07 - store line >
17:29:07 - store line >  type_id phone                 ""322223""
17:29:07 - store line >  type_id address               ""Лихвинцева 56-67""
17:29:07 - store line >  type_id address_fact          ""Буммашевская 44-47""
17:29:07 - store line >
17:29:07 - store line >  type_id domiciliary_hospital  ""МСЧ 11""
17:29:07 - store line >  type_id comment               ""Очень любит рыбий жыр""
17:29:07 - stop stored lines
17:29:07 - cmd ""start_store_lines_as"" ""record_fields_2""
17:29:07 - start store lines as ""record_fields_2""
17:29:07 - store line >  type_id last_name             ""Полищук""
17:29:07 - store line >  type_id first_name            ""Любовь""
17:29:07 - store line >  type_id middle_name           ""Карловна""
17:29:07 - store line >
17:29:07 - store line >  type_id birthday              ""11.05.1977""
17:29:07 - store line >  click_id_value sex f
17:29:07 - store line >
17:29:07 - store line >  type_id snils                 ""11111111112""
17:29:07 - store line >
17:29:07 - store line >  set_value_id disability       ""0""
17:29:07 - store line >
17:29:07 - store line >  type_id speciality            ""медсестра""
17:29:07 - store line >  type_id appointment           ""старшая медсестра""
17:29:07 - store line >  type_id department            ""сестринская""
17:29:07 - store line >
17:29:07 - store line >  set_value_id experienceYears  4
17:29:07 - store line >  set_value_id experienceMonths 5
17:29:07 - store line >
17:29:07 - store line >  type_id passport_series       ""2223""
17:29:07 - store line >  type_id passport_number       ""333334""
17:29:07 - store line >  type_id passport_date         ""17.05.2003""
17:29:07 - store line >  type_id passport_office       ""Сталинским РОВД""
17:29:07 - store line >
17:29:07 - store line >  type_id insurance_number      ""55555""
17:29:07 - store line >  type_id insurance_company     ""Доверие""
17:29:07 - store line >
17:29:07 - store line >  type_id phone                 ""447276""
17:29:07 - store line >  type_id address               ""Ленина 100""
17:29:07 - store line >  type_id address_fact          ""Пушкинская 222""
17:29:07 - store line >
17:29:07 - store line >  type_id domiciliary_hospital  ""МСЧ 17""
17:29:07 - store line >  type_id comment               ""Вечно сипит""
17:29:07 - stop stored lines
17:29:07 - ####################
17:29:07 - # карты
17:29:07 - ####################
17:29:07 - cmd ""start_store_lines_as"" ""test_records""
17:29:07 - start store lines as ""test_records""
17:29:07 - store line >  # добавить предварительную карту
17:29:07 - store line >    click_text ""Предварительные осмотры""
17:29:07 - store line >    wait_text ""Добавить карту""
17:29:07 - store line >    shot_check_png preliminary-records-start.png
17:29:07 - store line >    click_text ""Добавить карту""
17:29:07 - store line >    sleep 25
17:29:07 - store line >    shot_check_png preliminary-record-add.png
17:29:07 - store line >    play_stored_lines record_fields_1
17:29:07 - store line >    click_text ""Cохранить""
17:29:07 - store line >    wait_text ""Добавить карту""
17:29:07 - store line >    shot_check_png preliminary-record-added.png
17:29:07 - store line >
17:29:07 - store line >  # редактировать предварительную карту
17:29:07 - store line >    click_text ""Предварительные осмотры""
17:29:07 - store line >    wait_click_text ""Шариков Полиграф Полиграфович""
17:29:07 - store line >    wait_click_text ""Редактировать карту""
17:29:07 - store line >    check_stored_lines record_fields_1
17:29:07 - store line >    play_stored_lines record_fields_2
17:29:07 - store line >    shot_check_png preliminary-record-edit-filled2.png
17:29:07 - store line >    click_text ""Cохранить""
17:29:07 - store line >    wait_text ""Добавить карту""
17:29:07 - store line >    shot_check_png preliminary-record-edited.png
17:29:07 - store line >
17:29:07 - store line >  # удалить предварительную карту
17:29:07 - store line >    click_text ""Предварительные осмотры""
17:29:07 - store line >    wait_click_text ""Полищук Любовь Карловна""
17:29:07 - store line >    wait_click_text ""Удалить карту""
17:29:07 - store line >    sleep 25
17:29:07 - store line >    wait_text_disappeared ""Удалить карту""
17:29:08 - store line >    wait_text_disappeared ""Шифрование данных""
17:29:08 - store line >    shot_check_png preliminary-records-start.png
17:29:08 - store line >
17:29:08 - store line >  # добавить карту в осмотр
17:29:08 - store line >  # редактирвоать карту в осмотре
17:29:08 - store line >  # удалить карту из осмотра
17:29:08 - stop stored lines
17:29:08 - ####################
17:29:08 - # профиль
17:29:08 - ####################
17:29:08 - cmd ""start_store_lines_as"" ""profile_fields_1""
17:29:08 - start store lines as ""profile_fields_1""
17:29:08 - store line >  type_id title           ""Первая клиническая больница""
17:29:08 - store line >  type_id title_short     ""1КБ""
17:29:08 - store line >  type_id ogrn            ""1111111111111""
17:29:08 - store line >  type_id lpu             ""1""
17:29:08 - store line >  type_id address         ""Кирова 11""
17:29:08 - store line >  click_id_value          budget no
17:29:08 - stop stored lines
17:29:08 - cmd ""start_store_lines_as"" ""profile_fields_2""
17:29:08 - start store lines as ""profile_fields_2""
17:29:08 - store line >  type_id title           ""Вторая клиническая больница""
17:29:08 - store line >  type_id title_short     ""2КБ""
17:29:08 - store line >  type_id ogrn            ""1111111111112""
17:29:08 - store line >  type_id lpu             ""2""
17:29:08 - store line >  type_id address         ""Кирова 12""
17:29:08 - store line >  click_id_value          budget yes
17:29:08 - stop stored lines
17:29:08 - cmd ""start_store_lines_as"" ""test_profile""
17:29:08 - start store lines as ""test_profile""
17:29:08 - store line >  # редактирование профиля ЛПУ
17:29:08 - store line >  click_text ""Профиль ЛПУ""
17:29:08 - store line >  wait_text ""Код ЛПУ""
17:29:08 - store line >  sleep 25
17:29:08 - store line >  shot_check_png profile-start.png
17:29:08 - store line >  play_stored_lines profile_fields_1
17:29:08 - store line >  shot_check_png profile-fields-1.png
17:29:08 - store line >  click_value ""Сохранить""
17:29:08 - store line >  wait_text ""Данные сохранены""
17:29:08 - store line >  click_text ""Ок""
17:29:08 - store line >  click_text ""Периодические осмотры""
17:29:08 - store line >  click_text ""Профиль ЛПУ""
17:29:08 - store line >  wait_text ""Код ЛПУ""
17:29:08 - store line >  sleep 25
17:29:08 - store line >  shot_check_png profile-fields-1.png
17:29:08 - store line >  play_stored_lines profile_fields_2
17:29:08 - store line >  shot_check_png profile-fields-2.png
17:29:08 - store line >  click_value ""Сохранить""
17:29:08 - store line >  wait_text ""Данные сохранены""
17:29:08 - store line >  click_text ""Ок""
17:29:08 - store line >  click_text ""Периодические осмотры""
17:29:08 - store line >  click_text ""Профиль ЛПУ""
17:29:08 - store line >  wait_text ""Код ЛПУ""
17:29:08 - store line >  sleep 25
17:29:08 - store line >  shot_check_png profile-fields-2.png
17:29:08 - stop stored lines
17:29:08 - cmd ""start_store_lines_as"" ""test_prices""
17:29:08 - start store lines as ""test_prices""
17:29:08 - store line >  # редактирвоание цен
17:29:08 - store line >  click_text ""Цены""
17:29:08 - store line >  wait_text ""Аллерголог""
17:29:08 - store line >  shot_check_png prices-start.png
17:29:08 - stop stored lines
17:29:08 - #play_stored_lines test_customers # не сохраняется регион
17:29:08 - cmd ""play_stored_lines"" ""test_inspections"" ""#"" ""не""
17:29:08 - start play stored lines ""test_inspections""
17:29:08 - # добавить осмотр
17:29:08 - cmd ""double_click_id"" ""2""
17:29:08 - id 2 is double clicked
17:29:08 - cmd ""wait_text"" ""Добавить осмотр""
17:29:08 - text ""Добавить осмотр"" is present
17:29:08 - cmd ""shot_check_png"" ""inspections-start.png""
17:29:08 - take screenshot
17:29:08 - pre check screenshot
17:29:08 - !!!!!!! etalon image not exist!
17:29:08 - cmd ""click_text"" ""Добавить осмотр""
17:29:08 - text ""Добавить осмотр"" is clicked
17:29:08 - cmd ""wait_text"" ""Данные осмотра""
17:29:08 - text ""Данные осмотра"" is present
17:29:08 - cmd ""sleep"" ""25""
17:29:08 - sleep 25 milliseconds
17:29:08 - cmd ""shot_check_png"" ""inspection-add.png""
17:29:08 - take screenshot
17:29:09 - pre check screenshot
17:29:09 - !!!!!!! etalon image not exist!
17:29:09 - cmd ""play_stored_lines"" ""inspection_fields_1""
17:29:09 - start play stored lines ""inspection_fields_1""
17:29:09 - cmd ""type_id"" ""title"" ""Осмотр 2014 года""
17:29:09 - typed
17:29:09 - cmd ""type_id"" ""date"" ""08.05.2014""
17:29:09 - typed
17:29:09 - cmd ""set_value_id"" ""status"" ""a""
17:29:09 - set
17:29:09 - cmd ""type_id"" ""description"" ""Обычный годовой осмотр""
17:29:09 - typed
17:29:09 - cmd ""type_id"" ""conclusion"" ""Пока нет никаких комментариев""
17:29:09 - typed
17:29:09 - stop play stored lines ""inspection_fields_1""
17:29:09 - cmd ""click_text"" ""Cохранить""
17:29:09 - text ""Cохранить"" is clicked
17:29:09 - cmd ""wait_text"" ""Добавить осмотр""
17:29:09 - text ""Добавить осмотр"" is present
17:29:09 - cmd ""shot_check_png"" ""inspection-added.png""
17:29:09 - take screenshot
17:29:09 - pre check screenshot
17:29:09 - !!!!!!! etalon image not exist!
17:29:09 - # редактировать осмотр
17:29:09 - cmd ""click_text"" ""Осмотр 2014 года""
17:29:09 - text ""Осмотр 2014 года"" is clicked
17:29:09 - cmd ""click_text"" ""Редактировать осмотр""
17:29:09 - text ""Редактировать осмотр"" is clicked
17:29:09 - cmd ""wait_text"" ""Данные осмотра""
17:29:09 - text ""Данные осмотра"" is present
17:29:09 - cmd ""sleep"" ""25""
17:29:09 - sleep 25 milliseconds
17:29:09 - cmd ""shot_check_png"" ""inspection-edit.png""
17:29:09 - take screenshot
17:29:09 - pre check screenshot
17:29:09 - !!!!!!! etalon image not exist!
17:29:09 - cmd ""check_stored_lines"" ""inspection_fields_1""
17:29:09 - start check stored lines ""inspection_fields_1""
17:29:09 - cmd ""type_id"" ""title"" ""Осмотр 2014 года""
17:29:09 - right value!
17:29:09 - cmd ""type_id"" ""date"" ""08.05.2014""
17:29:09 - right value!
17:29:09 - cmd ""set_value_id"" ""status"" ""a""
17:29:09 - right value!
17:29:09 - cmd ""type_id"" ""description"" ""Обычный годовой осмотр""
17:29:09 - right value!
17:29:09 - cmd ""type_id"" ""conclusion"" ""Пока нет никаких комментариев""
17:29:09 - right value!
17:29:09 - stop check stored lines ""inspection_fields_1""
17:29:09 - cmd ""play_stored_lines"" ""inspection_fields_2""
17:29:09 - start play stored lines ""inspection_fields_2""
17:29:09 - cmd ""type_id"" ""title"" ""Осмотр 2013 года""
17:29:09 - typed
17:29:09 - cmd ""type_id"" ""date"" ""08.05.2013""
17:29:09 - typed
17:29:09 - cmd ""set_value_id"" ""status"" ""n""
17:29:09 - set
17:29:09 - cmd ""type_id"" ""description"" ""Необычный годовой осмотр""
17:29:09 - typed
17:29:09 - cmd ""type_id"" ""conclusion"" ""не помню""
17:29:09 - typed
17:29:09 - stop play stored lines ""inspection_fields_2""
17:29:09 - cmd ""click_text"" ""Cохранить""
17:29:09 - text ""Cохранить"" is clicked
17:29:09 - cmd ""wait_text"" ""Добавить осмотр""
17:29:09 - text ""Добавить осмотр"" is present
17:29:09 - cmd ""shot_check_png"" ""inspection-edited.png""
17:29:09 - take screenshot
17:29:10 - pre check screenshot
17:29:10 - !!!!!!! etalon image not exist!
17:29:10 - # удалить осмотр
17:29:10 - stop play stored lines ""test_inspections""
17:29:10 - #play_stored_lines test_records # не могу войти двойным кликом в переодические
17:29:10 - #play_stored_lines test_profile
17:29:10 - #play_stored_lines test_prices # не могу редактировать цены двойным кликом
17:29:10 - cmd ""exit""
";

        static void TestLogForm()
        {
            WBT.LogForm frm= new WBT.LogForm();
            frm.DataLoad(txtLog);
            frm.ShowDialog();
        }

        static void FindOpenedXps(string prefix)
        {
            string fname= WBT.XpsHelper.FindOpenedXpsAndCloseWindow(prefix);
        }

        static void TestCheckingForms()
        {
            WBT.SignalingCheckings checkings= new WBT.SignalingCheckings();

            WBT.IChecking c= checkings.Start("1");
            c.CheckedFile= "D:\\work\\vva\\outsourcing\\infotrust\\cpw\\work\\src\\wbt_tests\\meo\\first.etalon.png";
            c.FailDescription= "color difference is 0,00013494";

            c= checkings.Start("2");
            c.CheckedFile= "D:\\work\\vva\\outsourcing\\infotrust\\cpw\\work\\src\\wbt_tests\\meo\\meo.etalon.png";
            c.FailDescription= "color difference is 0,02390319";

            c= checkings.Start("3");
            c.CheckedFile= "D:\\work\\vva\\outsourcing\\infotrust\\cpw\\work\\src\\wbt_tests\\meo\\meo-small.etalon.png";
            c.FailDescription= "color difference is 0,03064683";

            c= checkings.Start("4");
            c.CheckedFile= "D:\\work\\vva\\outsourcing\\infotrust\\cpw\\work\\src\\wbt_tests\\meo\\add-medical-customer.etalon.png";

            c= checkings.Start("5");
            c.CheckedFile= "D:\\work\\vva\\outsourcing\\infotrust\\cpw\\work\\src\\wbt_tests\\meo\\add-medical-customer-filled.etalon.png";
            c.FailDescription= "etalon image not exist!";

            WBT.CheckingsForm frm= new WBT.CheckingsForm();
            frm.DataLoad(checkings);
            frm.ShowDialog();
        }
    }
}
