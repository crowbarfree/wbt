﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    static class Program
    {
        static ILog log= LogManager.GetLogger(typeof(Program));

        [STAThread]
        static void Main(string[] args)
        {
            Settings settings = null;
            if (!SafeStartExecutable(args, ref settings))
            {
                log4net.Util.LogLog.QuietMode = true;
                log4net.Config.XmlConfigurator.Configure();
                log.ErrorFormat("started {0}", Arguments.ExeFileName);

                Application.ThreadException += Application_ThreadException;
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                MainForm mainform = new MainForm();
                if (Arguments.ProcessArguments(args, mainform.player, settings))
                {
                    if (mainform.player.PipeServerMode)
                        mainform.Text += " (pipe server mode)";
                    mainform.WindowState = Arguments.WhichWindowState(mainform.player);
                    Application.Run(mainform);
                }
            }
        }

        static bool SafeStartExecutable(string[] args, ref Settings settings)
        {
            foreach (string arg in args)
            {
                if (!arg.StartsWith("-"))
                {
                    string settings_file_path = arg;
                    settings = Settings.SafeReadFromFile(settings_file_path);
                    if (!string.IsNullOrEmpty(settings.ExecutableFilePrefix))
                    {
                        StartExecutable(args, settings_file_path, settings);
                        return true;
                    }
                }
            }
            return false;
        }

        private const uint SW_MINIMIZE = 0x06;
        [System.Runtime.InteropServices.DllImport("USER32.DLL")]
        public static extern int ShowWindow(uint hWnd, uint cmd);

        static void StartExecutable(string[] args, string settings_file_path, Settings settings)
        {
            StringBuilder sb = new StringBuilder();
            string ea_arg= null;
            string wbt_arg = prepare_wbt_args(sb, args, ref ea_arg);

            string process_arguments, cmd;
            prepare_process_arguments(settings.ExecutableFilePrefix, sb, wbt_arg, ea_arg, out cmd, out process_arguments);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo sinfo = process.StartInfo;
            sinfo.FileName = cmd;
            sinfo.Arguments = process_arguments;
            sinfo.UseShellExecute = false;
            sinfo.RedirectStandardOutput = true;
            process.OutputDataReceived += (sender, dargs) => Console.WriteLine(dargs.Data);
            if (!process.Start())
            {
                Console.Error.WriteLine("can not start wbt!");
                log.Error("can not start wbt!");
                Environment.ExitCode = -1;
            }
            else
            {
                process.BeginOutputReadLine();
                ShowWindow((uint)process.MainWindowHandle, SW_MINIMIZE);
                process.WaitForExit();
                Environment.ExitCode = process.ExitCode;
            }
        }

        static void prepare_process_arguments(string ExecutableFilePrefix, StringBuilder sb
            , string wbt_arg, string ea_arg, out string cmd, out string process_arguments)
        {
            string[] parts = CommandLineHelper.CommandLineToArgs(ExecutableFilePrefix);
            cmd = parts[0];
            if (1 == parts.Length)
            {
                process_arguments= ea_arg;
            }
            else
            {
                sb.Length = 0;
                for (int i = 1; i < parts.Length - 1; i++)
                    sb.Append(parts[i]).Append(" ");
                string last_part = parts[parts.Length - 1];
                sb.Append($"\"{last_part}{wbt_arg}\"");
                if (!string.IsNullOrEmpty(ea_arg))
                    sb.Append(" ").Append(ea_arg);
                process_arguments = sb.ToString();
            }
        }

        const string ea_arg_prefix = "-ea=";
        static string prepare_wbt_args(StringBuilder sb, IEnumerable<string> args, ref string ea_arg)
        {
            sb.Length = 0;
            string splitter = "";
            foreach (string arg in args)
            {
                int pos = arg.IndexOf(ea_arg_prefix);
                if (0<=pos)
                {
                    ea_arg= arg.Substring(ea_arg_prefix.Length);
                }
                else
                {
                    sb.Append(splitter);
                    splitter = " ";
                    string escaped_arg = !arg.Contains(" ") ? arg : $"\"{arg}\"";
                    escaped_arg = escaped_arg
                        .Replace("&", "^&")
                        .Replace("<", "^<")
                        .Replace(">", "^>")
                        .Replace("|", "^|")
                        .Replace("\"", "\"\"")
                        ;
                    sb.Append(escaped_arg);
                }
            }
            string wbt_arg = sb.ToString();
            return wbt_arg;
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            log.Error("CurrentDomain_UnhandledException");
            log.ErrorFormat("ExceptionObject={0}", e.ExceptionObject);
            Environment.ExitCode = -1;
            Application.Exit();
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            log.Error("Application_ThreadException", e.Exception);
            Environment.ExitCode = -1;
            Application.Exit();
        }

    }
}
