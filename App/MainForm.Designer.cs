﻿namespace WBT
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.btnExecute = new System.Windows.Forms.Button();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.panelForWebBrowser = new System.Windows.Forms.Panel();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.bBackward = new System.Windows.Forms.Button();
            this.bForward = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnPauseResume = new System.Windows.Forms.Button();
            this.lScenario = new System.Windows.Forms.Label();
            this.linkLabelScenario = new System.Windows.Forms.LinkLabel();
            this.linkCheckings = new System.Windows.Forms.LinkLabel();
            this.txtElementText = new System.Windows.Forms.TextBox();
            this.txtElementId = new System.Windows.Forms.TextBox();
            this.txtElementValue = new System.Windows.Forms.TextBox();
            this.lElementText = new System.Windows.Forms.Label();
            this.lElementId = new System.Windows.Forms.Label();
            this.lElementValue = new System.Windows.Forms.Label();
            this.lElement = new System.Windows.Forms.Label();
            this.lElementType = new System.Windows.Forms.Label();
            this.txtElementTagName = new System.Windows.Forms.TextBox();
            this.cbElementSelecting = new System.Windows.Forms.CheckBox();
            this.cbElementSelected = new System.Windows.Forms.CheckBox();
            this.lElementPath = new System.Windows.Forms.Label();
            this.txtElementPath = new System.Windows.Forms.TextBox();
            this.lElementClass = new System.Windows.Forms.Label();
            this.txtElementClass = new System.Windows.Forms.TextBox();
            this.lStartUrl = new System.Windows.Forms.Label();
            this.linkStartUrl = new System.Windows.Forms.LinkLabel();
            this.lCallBeforeStart = new System.Windows.Forms.Label();
            this.linkCallBeforeStart = new System.Windows.Forms.LinkLabel();
            this.linkLog = new System.Windows.Forms.LinkLabel();
            this.cbElementGrabForm = new System.Windows.Forms.CheckBox();
            this.btnStep = new System.Windows.Forms.Button();
            this.txtExecutingCommand = new System.Windows.Forms.TextBox();
            this.panelForWebBrowser.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1024, 768);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowser_Navigated);
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Enabled = false;
            this.btnExecute.Location = new System.Drawing.Point(1263, 516);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(75, 23);
            this.btnExecute.TabIndex = 1;
            this.btnExecute.Text = "exec";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // txtCommand
            // 
            this.txtCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommand.Enabled = false;
            this.txtCommand.Location = new System.Drawing.Point(1043, 519);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(214, 20);
            this.txtCommand.TabIndex = 2;
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtLog.Location = new System.Drawing.Point(1042, 212);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(295, 294);
            this.txtLog.TabIndex = 3;
            // 
            // panelForWebBrowser
            // 
            this.panelForWebBrowser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelForWebBrowser.Controls.Add(this.webBrowser);
            this.panelForWebBrowser.Location = new System.Drawing.Point(9, 51);
            this.panelForWebBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.panelForWebBrowser.Name = "panelForWebBrowser";
            this.panelForWebBrowser.Size = new System.Drawing.Size(1024, 768);
            this.panelForWebBrowser.TabIndex = 4;
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.Location = new System.Drawing.Point(67, 4);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.ReadOnly = true;
            this.txtUrl.Size = new System.Drawing.Size(966, 20);
            this.txtUrl.TabIndex = 5;
            // 
            // bBackward
            // 
            this.bBackward.Location = new System.Drawing.Point(9, 2);
            this.bBackward.Name = "bBackward";
            this.bBackward.Size = new System.Drawing.Size(22, 23);
            this.bBackward.TabIndex = 6;
            this.bBackward.Text = "< ";
            this.bBackward.UseVisualStyleBackColor = true;
            this.bBackward.Click += new System.EventHandler(this.bBackward_Click);
            // 
            // bForward
            // 
            this.bForward.Location = new System.Drawing.Point(37, 2);
            this.bForward.Name = "bForward";
            this.bForward.Size = new System.Drawing.Size(24, 23);
            this.bForward.TabIndex = 7;
            this.bForward.Text = ">";
            this.bForward.UseVisualStyleBackColor = true;
            this.bForward.Click += new System.EventHandler(this.bForward_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestart.Location = new System.Drawing.Point(1042, 157);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(85, 23);
            this.btnRestart.TabIndex = 8;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnPauseResume
            // 
            this.btnPauseResume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPauseResume.Location = new System.Drawing.Point(1146, 157);
            this.btnPauseResume.Name = "btnPauseResume";
            this.btnPauseResume.Size = new System.Drawing.Size(85, 23);
            this.btnPauseResume.TabIndex = 9;
            this.btnPauseResume.Text = "Pause";
            this.btnPauseResume.UseVisualStyleBackColor = true;
            this.btnPauseResume.Click += new System.EventHandler(this.btnPauseResume_Click);
            // 
            // lScenario
            // 
            this.lScenario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lScenario.AutoSize = true;
            this.lScenario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lScenario.Location = new System.Drawing.Point(1040, 89);
            this.lScenario.Name = "lScenario";
            this.lScenario.Size = new System.Drawing.Size(65, 16);
            this.lScenario.TabIndex = 11;
            this.lScenario.Text = "Scenario:";
            // 
            // linkLabelScenario
            // 
            this.linkLabelScenario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelScenario.Location = new System.Drawing.Point(1054, 111);
            this.linkLabelScenario.Name = "linkLabelScenario";
            this.linkLabelScenario.Size = new System.Drawing.Size(283, 37);
            this.linkLabelScenario.TabIndex = 12;
            this.linkLabelScenario.TabStop = true;
            this.linkLabelScenario.Text = "scenario is absent";
            // 
            // linkCheckings
            // 
            this.linkCheckings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkCheckings.AutoSize = true;
            this.linkCheckings.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkCheckings.Location = new System.Drawing.Point(1051, 188);
            this.linkCheckings.Name = "linkCheckings";
            this.linkCheckings.Size = new System.Drawing.Size(71, 13);
            this.linkCheckings.TabIndex = 13;
            this.linkCheckings.TabStop = true;
            this.linkCheckings.Text = "no checkings";
            this.linkCheckings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCheckings_LinkClicked);
            // 
            // txtElementText
            // 
            this.txtElementText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementText.Location = new System.Drawing.Point(1123, 747);
            this.txtElementText.Name = "txtElementText";
            this.txtElementText.ReadOnly = true;
            this.txtElementText.Size = new System.Drawing.Size(214, 20);
            this.txtElementText.TabIndex = 14;
            this.txtElementText.Click += new System.EventHandler(this.txtElementText_Click);
            // 
            // txtElementId
            // 
            this.txtElementId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementId.Location = new System.Drawing.Point(1123, 692);
            this.txtElementId.Name = "txtElementId";
            this.txtElementId.ReadOnly = true;
            this.txtElementId.Size = new System.Drawing.Size(214, 20);
            this.txtElementId.TabIndex = 15;
            this.txtElementId.Click += new System.EventHandler(this.txtElementId_Click);
            // 
            // txtElementValue
            // 
            this.txtElementValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementValue.Location = new System.Drawing.Point(1123, 773);
            this.txtElementValue.Name = "txtElementValue";
            this.txtElementValue.ReadOnly = true;
            this.txtElementValue.Size = new System.Drawing.Size(214, 20);
            this.txtElementValue.TabIndex = 17;
            this.txtElementValue.Click += new System.EventHandler(this.txtElementValue_Click);
            // 
            // lElementText
            // 
            this.lElementText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementText.AutoSize = true;
            this.lElementText.Location = new System.Drawing.Point(1063, 750);
            this.lElementText.Name = "lElementText";
            this.lElementText.Size = new System.Drawing.Size(54, 13);
            this.lElementText.TabIndex = 18;
            this.lElementText.Text = "innerText:";
            // 
            // lElementId
            // 
            this.lElementId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementId.AutoSize = true;
            this.lElementId.Location = new System.Drawing.Point(1099, 695);
            this.lElementId.Name = "lElementId";
            this.lElementId.Size = new System.Drawing.Size(18, 13);
            this.lElementId.TabIndex = 19;
            this.lElementId.Text = "id:";
            // 
            // lElementValue
            // 
            this.lElementValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementValue.AutoSize = true;
            this.lElementValue.Location = new System.Drawing.Point(1081, 776);
            this.lElementValue.Name = "lElementValue";
            this.lElementValue.Size = new System.Drawing.Size(36, 13);
            this.lElementValue.TabIndex = 21;
            this.lElementValue.Text = "value:";
            // 
            // lElement
            // 
            this.lElement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElement.AutoSize = true;
            this.lElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lElement.Location = new System.Drawing.Point(1039, 551);
            this.lElement.Name = "lElement";
            this.lElement.Size = new System.Drawing.Size(158, 16);
            this.lElement.TabIndex = 22;
            this.lElement.Text = "html element parameters:";
            // 
            // lElementType
            // 
            this.lElementType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementType.AutoSize = true;
            this.lElementType.Location = new System.Drawing.Point(1064, 669);
            this.lElementType.Name = "lElementType";
            this.lElementType.Size = new System.Drawing.Size(54, 13);
            this.lElementType.TabIndex = 23;
            this.lElementType.Text = "tag name:";
            // 
            // txtElementTagName
            // 
            this.txtElementTagName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementTagName.Location = new System.Drawing.Point(1124, 666);
            this.txtElementTagName.Name = "txtElementTagName";
            this.txtElementTagName.ReadOnly = true;
            this.txtElementTagName.Size = new System.Drawing.Size(214, 20);
            this.txtElementTagName.TabIndex = 24;
            this.txtElementTagName.Click += new System.EventHandler(this.txtElementTagName_Click);
            // 
            // cbElementSelecting
            // 
            this.cbElementSelecting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbElementSelecting.AutoSize = true;
            this.cbElementSelecting.Location = new System.Drawing.Point(1063, 570);
            this.cbElementSelecting.Name = "cbElementSelecting";
            this.cbElementSelecting.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbElementSelecting.Size = new System.Drawing.Size(107, 17);
            this.cbElementSelecting.TabIndex = 25;
            this.cbElementSelecting.Text = "Store first clicked";
            this.cbElementSelecting.UseVisualStyleBackColor = true;
            this.cbElementSelecting.CheckedChanged += new System.EventHandler(this.cbElementSelecting_CheckedChanged);
            // 
            // cbElementSelected
            // 
            this.cbElementSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbElementSelected.AutoSize = true;
            this.cbElementSelected.Location = new System.Drawing.Point(1176, 570);
            this.cbElementSelected.Name = "cbElementSelected";
            this.cbElementSelected.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbElementSelected.Size = new System.Drawing.Size(85, 17);
            this.cbElementSelected.TabIndex = 26;
            this.cbElementSelected.Text = "Show stored";
            this.cbElementSelected.UseVisualStyleBackColor = true;
            this.cbElementSelected.CheckedChanged += new System.EventHandler(this.cbElementSelected_CheckedChanged);
            // 
            // lElementPath
            // 
            this.lElementPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementPath.AutoSize = true;
            this.lElementPath.Location = new System.Drawing.Point(1086, 593);
            this.lElementPath.Name = "lElementPath";
            this.lElementPath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lElementPath.Size = new System.Drawing.Size(31, 13);
            this.lElementPath.TabIndex = 27;
            this.lElementPath.Text = "path:";
            this.lElementPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtElementPath
            // 
            this.txtElementPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementPath.Location = new System.Drawing.Point(1123, 593);
            this.txtElementPath.Multiline = true;
            this.txtElementPath.Name = "txtElementPath";
            this.txtElementPath.ReadOnly = true;
            this.txtElementPath.Size = new System.Drawing.Size(214, 67);
            this.txtElementPath.TabIndex = 28;
            // 
            // lElementClass
            // 
            this.lElementClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lElementClass.AutoSize = true;
            this.lElementClass.Location = new System.Drawing.Point(1083, 721);
            this.lElementClass.Name = "lElementClass";
            this.lElementClass.Size = new System.Drawing.Size(34, 13);
            this.lElementClass.TabIndex = 29;
            this.lElementClass.Text = "class:";
            // 
            // txtElementClass
            // 
            this.txtElementClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtElementClass.Location = new System.Drawing.Point(1123, 718);
            this.txtElementClass.Name = "txtElementClass";
            this.txtElementClass.ReadOnly = true;
            this.txtElementClass.Size = new System.Drawing.Size(214, 20);
            this.txtElementClass.TabIndex = 30;
            this.txtElementClass.Click += new System.EventHandler(this.txtElementClass_Click);
            // 
            // lStartUrl
            // 
            this.lStartUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lStartUrl.AutoSize = true;
            this.lStartUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lStartUrl.Location = new System.Drawing.Point(1039, 53);
            this.lStartUrl.Name = "lStartUrl";
            this.lStartUrl.Size = new System.Drawing.Size(68, 16);
            this.lStartUrl.TabIndex = 31;
            this.lStartUrl.Text = "Start URL:";
            // 
            // linkStartUrl
            // 
            this.linkStartUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkStartUrl.AutoSize = true;
            this.linkStartUrl.Location = new System.Drawing.Point(1054, 71);
            this.linkStartUrl.Name = "linkStartUrl";
            this.linkStartUrl.Size = new System.Drawing.Size(97, 13);
            this.linkStartUrl.TabIndex = 32;
            this.linkStartUrl.TabStop = true;
            this.linkStartUrl.Text = "start URL is absent";
            // 
            // lCallBeforeStart
            // 
            this.lCallBeforeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lCallBeforeStart.AutoSize = true;
            this.lCallBeforeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lCallBeforeStart.Location = new System.Drawing.Point(1040, 5);
            this.lCallBeforeStart.Name = "lCallBeforeStart";
            this.lCallBeforeStart.Size = new System.Drawing.Size(104, 16);
            this.lCallBeforeStart.TabIndex = 33;
            this.lCallBeforeStart.Text = "Call before start:";
            // 
            // linkCallBeforeStart
            // 
            this.linkCallBeforeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkCallBeforeStart.Location = new System.Drawing.Point(1054, 25);
            this.linkCallBeforeStart.Name = "linkCallBeforeStart";
            this.linkCallBeforeStart.Size = new System.Drawing.Size(284, 28);
            this.linkCallBeforeStart.TabIndex = 34;
            this.linkCallBeforeStart.TabStop = true;
            this.linkCallBeforeStart.Text = "path to call before start is absent";
            // 
            // linkLog
            // 
            this.linkLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLog.AutoSize = true;
            this.linkLog.Location = new System.Drawing.Point(1282, 188);
            this.linkLog.Name = "linkLog";
            this.linkLog.Size = new System.Drawing.Size(28, 13);
            this.linkLog.TabIndex = 35;
            this.linkLog.TabStop = true;
            this.linkLog.Text = "Log:";
            this.linkLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLog_LinkClicked);
            // 
            // cbElementGrabForm
            // 
            this.cbElementGrabForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbElementGrabForm.AutoSize = true;
            this.cbElementGrabForm.Location = new System.Drawing.Point(1267, 570);
            this.cbElementGrabForm.Name = "cbElementGrabForm";
            this.cbElementGrabForm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbElementGrabForm.Size = new System.Drawing.Size(72, 17);
            this.cbElementGrabForm.TabIndex = 36;
            this.cbElementGrabForm.Text = "Grab form";
            this.cbElementGrabForm.UseVisualStyleBackColor = true;
            this.cbElementGrabForm.CheckedChanged += new System.EventHandler(this.cbElementGrabForm_CheckedChanged);
            // 
            // btnStep
            // 
            this.btnStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep.Location = new System.Drawing.Point(1252, 157);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(85, 23);
            this.btnStep.TabIndex = 37;
            this.btnStep.Text = "Step";
            this.btnStep.UseVisualStyleBackColor = true;
            this.btnStep.Click += new System.EventHandler(this.btnStep_Click);
            // 
            // txtExecutingCommand
            // 
            this.txtExecutingCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExecutingCommand.Location = new System.Drawing.Point(9, 27);
            this.txtExecutingCommand.Name = "txtExecutingCommand";
            this.txtExecutingCommand.ReadOnly = true;
            this.txtExecutingCommand.Size = new System.Drawing.Size(1024, 20);
            this.txtExecutingCommand.TabIndex = 38;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1349, 828);
            this.Controls.Add(this.txtExecutingCommand);
            this.Controls.Add(this.btnStep);
            this.Controls.Add(this.cbElementGrabForm);
            this.Controls.Add(this.linkLog);
            this.Controls.Add(this.linkCallBeforeStart);
            this.Controls.Add(this.lCallBeforeStart);
            this.Controls.Add(this.linkStartUrl);
            this.Controls.Add(this.lStartUrl);
            this.Controls.Add(this.txtElementClass);
            this.Controls.Add(this.lElementClass);
            this.Controls.Add(this.txtElementPath);
            this.Controls.Add(this.lElementPath);
            this.Controls.Add(this.cbElementSelected);
            this.Controls.Add(this.cbElementSelecting);
            this.Controls.Add(this.txtElementTagName);
            this.Controls.Add(this.lElementType);
            this.Controls.Add(this.lElement);
            this.Controls.Add(this.lElementValue);
            this.Controls.Add(this.lElementId);
            this.Controls.Add(this.lElementText);
            this.Controls.Add(this.txtElementValue);
            this.Controls.Add(this.txtElementId);
            this.Controls.Add(this.txtElementText);
            this.Controls.Add(this.linkCheckings);
            this.Controls.Add(this.linkLabelScenario);
            this.Controls.Add(this.lScenario);
            this.Controls.Add(this.btnPauseResume);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.bForward);
            this.Controls.Add(this.bBackward);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.panelForWebBrowser);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.txtCommand);
            this.Controls.Add(this.btnExecute);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WeBrowser testing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelForWebBrowser.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExecute;
        internal System.Windows.Forms.TextBox txtCommand;
        private System.Windows.Forms.Panel panelForWebBrowser;
        internal System.Windows.Forms.WebBrowser webBrowser;
        internal System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Button bBackward;
        private System.Windows.Forms.Button bForward;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnPauseResume;
        private System.Windows.Forms.Label lScenario;
        private System.Windows.Forms.LinkLabel linkLabelScenario;
        private System.Windows.Forms.LinkLabel linkCheckings;
        private System.Windows.Forms.TextBox txtElementText;
        private System.Windows.Forms.TextBox txtElementId;
        private System.Windows.Forms.TextBox txtElementValue;
        private System.Windows.Forms.Label lElementText;
        private System.Windows.Forms.Label lElementId;
        private System.Windows.Forms.Label lElementValue;
        private System.Windows.Forms.Label lElement;
        private System.Windows.Forms.Label lElementType;
        private System.Windows.Forms.TextBox txtElementTagName;
        private System.Windows.Forms.CheckBox cbElementSelecting;
        private System.Windows.Forms.CheckBox cbElementSelected;
        private System.Windows.Forms.Label lElementPath;
        private System.Windows.Forms.TextBox txtElementPath;
        private System.Windows.Forms.Label lElementClass;
        private System.Windows.Forms.TextBox txtElementClass;
        private System.Windows.Forms.Label lStartUrl;
        private System.Windows.Forms.LinkLabel linkStartUrl;
        private System.Windows.Forms.Label lCallBeforeStart;
        private System.Windows.Forms.LinkLabel linkCallBeforeStart;
        private System.Windows.Forms.LinkLabel linkLog;
        private System.Windows.Forms.CheckBox cbElementGrabForm;
        private System.Windows.Forms.Button btnStep;
        internal System.Windows.Forms.TextBox txtExecutingCommand;
    }
}

