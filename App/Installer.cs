﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace WBT
{
    [RunInstaller(true)]
    public partial class InstallerWBT : System.Configuration.Install.Installer
    {
        public InstallerWBT()
        {
            InitializeComponent();
        }

        protected override void OnAfterInstall(System.Collections.IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            AppendPathVariable(AppPath(),PathVariableName,Target);
            Environment.SetEnvironmentVariable(HomeVariableName, AppPath(), Target);
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            RemovePathVariable(AppPath(),PathVariableName,Target);
            base.OnBeforeUninstall(savedState);
            Environment.SetEnvironmentVariable(HomeVariableName, string.Empty, Target);
        }

        const string HomeVariableName = "WBTHOME";
        const string PathVariableName= "Path";
        const EnvironmentVariableTarget Target= EnvironmentVariableTarget.Machine;

        string AppPath()
        {
            return Path.GetDirectoryName(Context.Parameters["assemblypath"].ToString());
        }

        static void RemovePathVariable(string appPath, string VariableName, EnvironmentVariableTarget target)
        {
            string current_value = Environment.GetEnvironmentVariable(VariableName, target);
            string current_value_upper= current_value.ToUpper();
            string to_remove_upper= ";" + appPath.ToUpper();
            int pos= current_value_upper.IndexOf(to_remove_upper);
            if (-1!=pos)
            {
                current_value = current_value.Remove(pos,to_remove_upper.Length);
                Environment.SetEnvironmentVariable(VariableName, current_value, target);
            }
        }

        static void AppendPathVariable(string appPath, string VariableName, EnvironmentVariableTarget target)
        {
            string current_value = System.Environment.GetEnvironmentVariable(VariableName, target);
            if (!current_value.ToUpper().Contains(appPath.ToUpper()))
            {
                current_value = current_value + ";" + appPath;
                System.Environment.SetEnvironmentVariable(VariableName, current_value, target);
            }
        }
    }
}
