﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using log4net;

namespace WBT
{
    public partial class MainForm : Form
    {
        static ILog log= LogManager.GetLogger(typeof(MainForm));

        public pipe.IPlayer player;

        public MainForm()
        {
            InitializeComponent();
            player = new pipe.Player(webBrowser, this, (args, player) => { Arguments.ProcessArguments(args, player); });
        }

        IGrizzly m_Grizzly = new Grizzly();

        [ComVisible(true)]
        public class ObjectForScripting
        {
            IPlayer helper;

            public ObjectForScripting(IPlayer helper)
            {
                this.helper = helper;
            }

            public void Log(string txt)
            {
                helper.Log(txt);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            MainForm mainForm = this;

            player.show_the_line = show_the_line();

            player.show_log_line_check_if_empty = show_log_line_check_if_empty();

            if (!string.IsNullOrEmpty(player.ScenarioPath))
                linkLabelScenario.Text= player.ScenarioPath;
            if (!string.IsNullOrEmpty(player.PreUrl))
                linkStartUrl.Text=  player.PreUrl;
            if (!string.IsNullOrEmpty(player.PreCall))
                linkCallBeforeStart.Text=  player.PreCall;

            player.ThreadChanged += new EventHandler(player_ThreadChanged);
            player.CheckingsChanged += new EventHandler(player_CheckingsChanged);

            webBrowser.Document.MouseOver += new HtmlElementEventHandler(Document_MouseOver);
            webBrowser.Document.Click += new HtmlElementEventHandler(Document_Click);
            webBrowser.ObjectForScripting = new ObjectForScripting(player);

            m_Grizzly.Bind_and_Show(this, webBrowser, player);

            if (player.PipeServerMode)
            {
                player.SafeStartListenPipe();
            }
            else
            {
                player.StartScenario();
            }
        }

        Action<string> show_the_line()
        {
            MainForm mainForm = this;

            return (string txt) =>
            {
                mainForm.Invoke(new MethodInvoker(delegate {
                    mainForm.txtExecutingCommand.Text =
                    mainForm.txtCommand.Text = txt;
                }));
            };
        }

        Action<string, bool> show_log_line_check_if_empty()
        {
            MainForm mainForm = this;

            return (string text, bool ifnotempty) =>
            {
                try
                {
                    mainForm.Invoke(new MethodInvoker(delegate
                    {
                        if (!ifnotempty || !string.IsNullOrEmpty(mainForm.txtLog.Text))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(DateTime.Now.ToLongTimeString()).Append(" ").Append(text).AppendLine();
                            mainForm.txtLog.AppendText(sb.ToString());
                        }
                    }));
                }
                catch (System.InvalidOperationException ex)
                {
                    log.Error("can not write text to log window", ex);
                }
            };
        }

        bool m_ElementSelectForForm= false;
        HtmlElement m_ElementToGrabForm= null;
        bool m_ElementSelecting= false;
        bool m_ElementShowingStored= false;

        void Document_Click(object sender, HtmlElementEventArgs e)
        {
            if (m_ElementSelecting)
            {
                m_ElementShowingStored= true;
                cbElementSelected.Checked= true;
                e.ReturnValue= true;
            }
            if (m_ElementSelectForForm)
            {
                if (null == m_ElementToGrabForm)
                {
                    MessageBox.Show(this, "Select container with items at first!", "Form items", MessageBoxButtons.OK);
                }
                else
                {
                    HtmlElement element = m_ElementToGrabForm;
                    StringBuilder sb = new StringBuilder();
                    BuildElementFormItems(element, sb);
                    string form_items = sb.ToString();
                    if (DialogResult.OK == MessageBox.Show(this, form_items, "Form items", MessageBoxButtons.OKCancel))
                        Clipboard.SetText(form_items, TextDataFormat.UnicodeText);
                    e.ReturnValue = true;
                }
            }
            cbElementGrabForm.Checked= false;
            cbElementSelecting.Checked= false;
            m_ElementSelecting= false;
            m_ElementSelectForForm= false;
        }

        private void cbElementSelecting_CheckedChanged(object sender, EventArgs e)
        {
            m_ElementSelecting= cbElementSelecting.Checked;
            if (m_ElementSelecting)
            {
                m_ElementShowingStored = false;
                cbElementSelected.Checked = false;
                m_ElementSelectForForm= false;
                cbElementGrabForm.Checked= false;
            }
        }

        private void cbElementGrabForm_CheckedChanged(object sender, EventArgs e)
        {
            m_ElementSelectForForm= cbElementGrabForm.Checked;
            if (m_ElementSelectForForm)
            {
                m_ElementShowingStored = false;
                cbElementSelected.Checked = false;
                m_ElementSelecting= false;
                cbElementSelecting.Checked= false;
            }
        }

        private void cbElementSelected_CheckedChanged(object sender, EventArgs e)
        {
            m_ElementShowingStored= cbElementSelected.Checked;
        }

        void BuildElementFormItems(HtmlElement element, StringBuilder sb)
        {
            int tabPos= 40;
            string TagName= element.TagName.ToUpper();
            if ("INPUT" == TagName)
            {
                string type= element.GetAttribute("type").ToUpper();
                if ("TEXT" == type)
                {
                    sb.Append(string.Format("  type_id      {0}",element.Id).PadRight(tabPos,' '));
                    sb.AppendLine("\"\"");
                }
            }
            if ("SELECT" == TagName)
            {
                sb.Append(string.Format    ("  set_value_id {0}",element.Id).PadRight(tabPos,' '));
                sb.AppendLine("\"\"");
            }
            if ("TEXTAREA" == TagName)
            {
                sb.Append(string.Format    ("  type_id      {0}",element.Id).PadRight(tabPos,' '));
                sb.AppendLine("\"\"");
            }
            foreach (HtmlElement child in element.Children)
                BuildElementFormItems(child,sb);
        }

        void BuildElementPath(HtmlElement element, StringBuilder sb)
        {
            if (null != element)
            {
                HtmlElement parent= element.Parent;
                BuildElementPath(parent,sb);
                if (0 != sb.Length)
                sb.Append(" /");
                sb.Append(element.TagName);
                if (!string.IsNullOrEmpty(element.Id))
                {
                    sb.Append("#");
                    sb.Append(element.Id);
                }
            }
        }

        string BuildElementPath(HtmlElement element)
        {
            StringBuilder sb= new StringBuilder();
            BuildElementPath(element,sb);
            return sb.ToString();
        }

        void Document_MouseOver(object sender, HtmlElementEventArgs e)
        {
            if (!m_ElementShowingStored)
            {
                Invoke(new MethodInvoker(delegate
                {
                    HtmlElement element = e.ToElement;
                    txtElementId.Text = element.Id;
                    txtElementValue.Text = HtmlHelper.GetElementValue(element);
                    txtElementClass.Text = HtmlHelper.GetElementClass(element);
                    txtElementText.Text = element.InnerText;
                    txtElementTagName.Text = element.TagName;
                    txtElementPath.Text= BuildElementPath(element);
                }));
            }
            if (m_ElementSelectForForm)
                m_ElementToGrabForm= e.ToElement;
        }

        Color? DefaultLinkColor= null;

        void player_CheckingsChanged(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                linkCheckings.Text= 
                    0== player.checkings_Count ? "no checkings" :
                    0== player.checkings_CountFailed ? string.Format("{0} checkings", player.checkings_Count) :
                    string.Format("{0} checkings, {1} failed", player.checkings_Count, player.checkings_CountFailed);
                if (null == DefaultLinkColor)
                    DefaultLinkColor= linkCheckings.LinkColor;
                linkCheckings.LinkColor= (0== player.checkings_CountFailed) ? DefaultLinkColor.Value : Color.Red;
            }));
        }

        void player_ThreadChanged(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (!player.thread_is_active)
                {
                    lScenario.Text = "Scenario (inactive):";
                }
                else if (player.signal_to_stread_to_stop)
                {
                    lScenario.Text = "Scenario (stopping):";
                }
                else if (player.thread_is_paused)
                {
                    lScenario.Text = "Scenario (paused):";
                }
                else
                {
                    lScenario.Text = "Scenario (running):";
                }
                btnPauseResume.Text= player.thread_is_paused ? "Resume" : "Pause";
                btnStep.Enabled= player.thread_is_paused;
                btnExecute.Enabled= 
                txtCommand.Enabled= player.thread_is_paused;
            }));
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            string line= txtCommand.Text;
            player.ProcessCommandLineDoStop(line,false);
        }

        private void webBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            txtUrl.Text= webBrowser.Url.ToString();
        }

        private void bBackward_Click(object sender, EventArgs e)
        {
            webBrowser.GoBack();
        }

        private void bForward_Click(object sender, EventArgs e)
        {
            webBrowser.GoForward();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            player.thread_is_paused= false;
            m_Grizzly.SafeShow();
            player.StartScenario();
        }

        private void btnPauseResume_Click(object sender, EventArgs e)
        {
            player.thread_is_paused= !player.thread_is_paused;
            if (player.thread_is_paused)
            {
                m_Grizzly.SafeHide();
            }
            else
            {
                m_Grizzly.SafeShow();
            }
        }

        private void btnStep_Click(object sender, EventArgs e)
        {
            player.signal_to_thread_to_step= true;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            player.signal_to_stread_to_stop= true;
            player.StopAsyncActiveX();
        }

        private void linkCheckings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (0 == player.checkings_Count)
            {
                MessageBox.Show("no checkings to view!");
            }
            else
            {
                CheckingsForm frm= new CheckingsForm();
                ICheckable checkable = player as ICheckable;
                if (null==checkable)
                {
                    MessageBox.Show("undefined checkings to view!");
                }
                else
                {
                    frm.DataLoad(checkable.Checkings);
                    frm.ShowDialog(this);
                }
            }
        }

        private void linkLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LogForm frm= new LogForm();
            frm.DataLoad(txtLog.Text);
            frm.ShowDialog(this);
        }

        private void txtElementTagName_Click(object sender, EventArgs e)
        {
            txtElementTagName.SelectAll();
        }

        private void txtElementId_Click(object sender, EventArgs e)
        {
            txtElementId.SelectAll();
        }

        private void txtElementClass_Click(object sender, EventArgs e)
        {
            txtElementClass.SelectAll();
        }

        private void txtElementText_Click(object sender, EventArgs e)
        {
            txtElementText.SelectAll();
        }

        private void txtElementValue_Click(object sender, EventArgs e)
        {
            txtElementValue.SelectAll();
        }
    }
}
