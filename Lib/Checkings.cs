﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml.Serialization;

using System.IO;

using System.Windows.Documents;
using System.Runtime.InteropServices;


namespace WBT
{
    public interface IChecking
    {
        string    CheckedFile      { get; set; }
        string    FailDescription  { get; set; }
    }

    public static class ICheckingMembers
    {
        public static string ResultFile(this IChecking checking)
        {
            return checking.CheckedFile.Replace(".etalon",".result");
        }

        public static string DifferenceFile(this IChecking checking)
        {
            return checking.CheckedFile.Replace(".etalon",".zdiff");
        }

        public static string OrigFile(this IChecking checking)
        {
            return checking.CheckedFile.Replace(".etalon",".zorig");
        }

        public static void SafeFixFailTime(this IChecking checking)
        {
            if (!string.IsNullOrEmpty(checking.FailDescription))
                checking.FailDescription= string.Format("{0} ({1})", checking.FailDescription, DateTime.Now);
        }
    }

    public class Checking : IChecking
    {
        public string CheckedFile       { get; set; }
        public string FailDescription   { get; set; }

        public static void CopyFromTo(IChecking from, IChecking to)
        {
            to.CheckedFile= from.CheckedFile;
            to.FailDescription= from.FailDescription;
        }

        static string MakeRelative(string fromDirectory, string toPath)
        {
          if (fromDirectory == null)
            throw new ArgumentNullException("fromDirectory");

          if (toPath == null)
            throw new ArgumentNullException("toPath");

          bool isRooted = (Path.IsPathRooted(fromDirectory) && Path.IsPathRooted(toPath));

          if (isRooted)
          {
            bool isDifferentRoot = (string.Compare(Path.GetPathRoot(fromDirectory), Path.GetPathRoot(toPath), true) != 0);

            if (isDifferentRoot)
              return toPath;
          }

          List<string> relativePath = new List<string>();
          string[] fromDirectories = fromDirectory.Split(Path.DirectorySeparatorChar);

          string[] toDirectories = toPath.Split(Path.DirectorySeparatorChar);

          int length = Math.Min(fromDirectories.Length, toDirectories.Length);

          int lastCommonRoot = -1;

          // find common root
          for (int x = 0; x < length; x++)
          {
            if (string.Compare(fromDirectories[x], toDirectories[x], true) != 0)
              break;

            lastCommonRoot = x;
          }

          if (lastCommonRoot == -1)
            return toPath;

          // add relative folders in from path
          for (int x = lastCommonRoot + 1; x < fromDirectories.Length; x++)
          {
            if (fromDirectories[x].Length > 0)
              relativePath.Add("..");
          }

          // add to folders to path
          for (int x = lastCommonRoot + 1; x < toDirectories.Length; x++)
          {
            relativePath.Add(toDirectories[x]);
          }

          // create relative path
          string[] relativeParts = new string[relativePath.Count];
          relativePath.CopyTo(relativeParts, 0);

          string newPath = string.Join(Path.DirectorySeparatorChar.ToString(), relativeParts);

          return newPath;
        }

        public void FixFileNameAsAbsolutePathForRootPath(string path)
        {
            CheckedFile= Path.Combine(path,CheckedFile);
        }

        public void FixFileNameAsRelativePathForRootPath(string path)
        {
            CheckedFile= MakeRelative(path, CheckedFile);
        }

        public Checking() { }
        public Checking(IChecking c)
        {
            CopyFromTo(c,this);
        }
    }

    public class Checkings : List<Checking>
    {
        public static Checkings ReadFromXmlFile(string xml_file_name)
        {
            XmlSerializer serializer= new XmlSerializer(typeof(Checkings));
            using (TextReader reader= new StreamReader(xml_file_name))
            {
                return (Checkings)serializer.Deserialize(reader);
            }
        }
        public void WriteToXmlFile(string xml_file_name)
        {
            XmlSerializer serializer= new XmlSerializer(typeof(Checkings));
            using (TextWriter writer = new StreamWriter(xml_file_name))
            {
                serializer.Serialize(writer, this);
            }
        }
        public void FixFileNamesAsRelativePathForRootPath(string path)
        {
            foreach (Checking checking in this)
            {
                checking.FixFileNameAsRelativePathForRootPath(path);
            }
        }
        public void FixFileNamesAsAbsolutePathForRootPath(string path)
        {
            foreach (Checking checking in this)
            {
                checking.FixFileNameAsAbsolutePathForRootPath(path);
            }
        }
    }

    public interface ICheckingFabric
    {
        IChecking Start(string message);
    }

    public class SignalingChecking : Checking, IChecking
    {
        SignalingCheckings m_Checkings;
        public SignalingChecking(SignalingCheckings checkings)
        {
            m_Checkings= checkings;
        }

        public string SignalFail
        {
            get { return FailDescription; }
            set
            {
                FailDescription= value;
                m_Checkings.SignalChange();
                if (null != m_Checkings.LogFailed)
                    m_Checkings.LogFailed("!!!!!!! " + value);
            }
        }
    }

    public class SignalingCheckings : Checkings, ICheckingFabric
    {
        public Action<string> Log;
        public Action<string> LogFailed;
        public IChecking Start(string message)
        {
            if (null != Log)
                Log(message);
            SignalingChecking res= new SignalingChecking(this);
            Add(res);
            if (null != Changed)
                Changed(this,null);
            return res;
        }
        internal void SignalChange()
        {
            if (null != Changed)
                Changed(this,null);
        }
        public int CountFailed
        {
            get
            {
                return this.Count((c) => { return !string.IsNullOrEmpty(c.FailDescription); });
            }
        }
        public void ClearAndSignalChange()
        {
            base.Clear();
            SignalChange();
        }
        public event EventHandler Changed;

        void PreCheckXpsPage(string prefix, int i, DocumentPage source, IChecking checking)
        {
            using (MemoryStream result_stream = new MemoryStream())
            {
                XpsHelper.SaveDocumentPageToPng(source,result_stream);
                PreCheckPngStream(string.Format("{0}.page.{1}",prefix,i+1),checking,result_stream, null);
            }
        }

        [DllImport("msvcrt.dll", CallingConvention=CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);

        public void LogError(string prefix, MemoryStream result_stream, string message)
        {
            IChecking checking= Start(message);
            checking.CheckedFile= prefix + ".err.etalon.png";
            checking.FailDescription= message;
            string result_fname= prefix + ".err.result.png";
            File.Delete(result_fname);
            File.WriteAllBytes(result_fname, result_stream.GetBuffer());
        }

        public void ClearErrors(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                foreach (string fname in Directory.GetFiles(path, "*.result.png"))
                {
                    File.Delete(fname);
                }
            }
        }

        public void PreCheckPngStream(string prefix, MemoryStream result_stream, double? MaxPngBytesDiff)
        {
            IChecking checking= Start("pre check screenshot");
            PreCheckPngStream(prefix,checking,result_stream, MaxPngBytesDiff);
        }

        void PreCheckPngStream(string prefix, IChecking checking, MemoryStream result_stream, double? MaxPngBytesDiff)
        {
            string etalon_fname= prefix + ".etalon.png";
            string result_fname= prefix + ".result.png";
            checking.CheckedFile= etalon_fname;
            byte[] result_bytes= result_stream.GetBuffer();
            PrepareResultFile(result_fname, etalon_fname);
            if (!File.Exists(etalon_fname))
            {
                checking.FailDescription= "etalon image not exist!";
                File.WriteAllBytes(result_fname, result_bytes);
            }
            else
            {
                byte[] etalon_bytes = File.ReadAllBytes(etalon_fname);
                if (etalon_bytes.Length==result_bytes.Length && 0 == memcmp(etalon_bytes, result_bytes, result_bytes.Length))
                {
                    File.WriteAllBytes(result_fname, result_bytes);
                }
                else
                {
                    PreCheckPngBytes(prefix, checking, result_stream, etalon_bytes, result_bytes, result_fname, etalon_fname, MaxPngBytesDiff);
                }
            }
            checking.SafeFixFailTime();
        }

        void PrepareResultFile(string result_fname, string etalon_fname)
        {
            File.Delete(result_fname);
        }

        static void PreCheckPngBytes(string prefix, IChecking checking, MemoryStream result_stream, 
            byte[] etalon_bytes, byte[] result_bytes, string result_fname, string etalon_fname, double? MaxPngBytesDiff)
        {
            long count_bytes, count_bytes_diff;
            using (MemoryStream etalon_stream = new MemoryStream(etalon_bytes))
            using (Bitmap result_image = (Bitmap)Image.FromStream(result_stream))
            using (Bitmap etalon_image = (Bitmap)Image.FromStream(etalon_stream))
            {
                if (result_image.Size.Width != etalon_image.Size.Width || result_image.Size.Height != etalon_image.Size.Height)
                {
                    checking.FailDescription = "etalon image has different size!";
                    File.WriteAllBytes(result_fname, result_bytes);
                    using (Bitmap diff_image = ImageHelper.PixelDiff(etalon_image, result_image, out count_bytes, out count_bytes_diff))
                    {
                        diff_image.Save(prefix + ".zdiff.png", ImageFormat.Png);
                        File.WriteAllBytes(prefix + ".zorig.png", result_bytes);
                    }
                }
                else
                {
                    using (Bitmap diff_image = ImageHelper.PixelDiff(etalon_image, result_image, out count_bytes, out count_bytes_diff))
                    {
                        if (0 == count_bytes_diff)
                        {
                            File.WriteAllBytes(result_fname, result_bytes);
                        }
                        else
                        {
                            diff_image.Save(prefix + ".zdiff.png", ImageFormat.Png);
                            File.WriteAllBytes(prefix + ".zorig.png", result_bytes);
                            PreCheckPngDiff(count_bytes, count_bytes_diff, result_fname, etalon_fname, result_bytes, checking, MaxPngBytesDiff);
                        }
                    }
                }
            }
        }

       //const double DefaultMaxPngBytesDiff= 0.00001;
        const double DefaultMaxPngBytesDiff= 0.0002;

        static void PreCheckPngDiff(long count_bytes, long count_bytes_diff, 
                                    string result_fname, string etalon_fname,
                                    byte[] result_bytes, IChecking checking, double? MaxPngBytesDiff)
        {
            double dcount_bytes = count_bytes;
            double dcount_bytes_diff = count_bytes_diff;
            double ddiff = dcount_bytes_diff / dcount_bytes;
            double max_diff = MaxPngBytesDiff.GetValueOrDefault(DefaultMaxPngBytesDiff);
            if (ddiff < max_diff)
            {
                File.Copy(etalon_fname,result_fname);
            }
            else
            {
                checking.FailDescription= string.Format("color difference is {0:0.00000000}, should be less than {1:0.00000000}", ddiff, max_diff);
                File.WriteAllBytes(result_fname, result_bytes);
            }
        }

        public void PreCheckXps(string xpsFileName)
        {
            string prefix= xpsFileName.Substring(0,xpsFileName.Length-4);
            using (System.Windows.Xps.Packaging.XpsDocument xpsDoc= new System.Windows.Xps.Packaging.XpsDocument(xpsFileName, System.IO.FileAccess.Read))
            {
                System.Windows.Documents.FixedDocumentSequence docSeq= xpsDoc.GetFixedDocumentSequence();
                DocumentPaginator pages= docSeq.DocumentPaginator;
                int pageCount = pages.PageCount;
                for (int i = 0; i < pageCount; i++)
                {
                    IChecking checking= Start("pre check page " + (i+1).ToString());
                    DocumentPage source = pages.GetPage(i);
                    PreCheckXpsPage(prefix,i,source,checking);
                }
            }
        }
    }
}
