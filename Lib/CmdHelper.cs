﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using System.Diagnostics;

using log4net;

namespace WBT
{
    public interface IPlayer
    {
        string ScenarioPath { set; get; }
        bool BatchMode { get; set; }
        bool EchoMode { set; }

        string PathForResultFiles { set; }
        string BaseUrl { set; }
        string PreUrl { set; get; }
        string PreCall { set; get; }
        string CheckingFile { set; }

        string PathToIncludeFrom { set; }

        Action<string> show_the_line { set; }
        Action protect_browser { set; }
        Action unprotect_browser { set; }
        Action<string, bool> show_log_line_check_if_empty { set; }

        event EventHandler Stop;

        int checkings_Count { get; }
        int checkings_CountFailed { get; }
        event EventHandler CheckingsChanged;

        event EventHandler ThreadChanged;

        bool signal_to_stread_to_stop { get; set; }
        bool signal_to_thread_to_step { set; }

        bool thread_is_active { get; }
        bool thread_is_paused { get; set; }

        Action OnRestart { get; set; }

        void StartScenario();
        void Log(string text);
        bool ProcessCommandLineDoStop(string line, bool quiet);
        void StopAsyncActiveX();
        void Clear();
    }

    public interface ICheckable
    {
        SignalingCheckings Checkings { get; }
    }

    public partial class Player : IPlayer, ICheckable
    {
        static ILog log= LogManager.GetLogger(typeof(Player));

        public bool EchoMode { set; private get; }
        public bool BatchMode { set; get; }

        public string PathForResultFiles { set; private get; }
        public string PathToIncludeFrom { set; private get; }
        public string PreUrl { set; get; }
        public string PreCall { set; get; }
        public string CheckingFile { set; private get; }
        public string ScenarioPath { set; get; }

        public Action<string> show_the_line { set; private get; }
        public Action protect_browser { set; private get; }
        public Action unprotect_browser { set; private get; }
        public Action<string, bool> show_log_line_check_if_empty { set; private get; }

        public string BaseUrl
        {
            private get { return _BaseUrl; }
            set
            {
                _BaseUrl= value;
                if (null != _BaseUrl && _BaseUrl.StartsWith(txtFileUrlPrefix))
                {
                    string path= _BaseUrl.Substring(txtFileUrlPrefix.Length);
                    _BaseUrl= txtFileUrlPrefix + Path.GetFullPath(path).Replace("\\","/");
                }
            }
        }

        public bool signal_to_stread_to_stop { get; set; }
        public bool signal_to_thread_to_step { private get; set; }

        public bool thread_is_paused { get; set; }
        public bool thread_is_active { get; private set; }

        public event EventHandler ThreadChanged;
        public event EventHandler Stop;

        public SignalingCheckings Checkings { get; private set; }
        public int checkings_Count { get { return Checkings.Count; } }
        public int checkings_CountFailed { get { return Checkings.CountFailed; } }
        public event EventHandler CheckingsChanged;

        public Action OnRestart { get; set; }

        public Player(WebBrowser b, Form f)
        {
            form = f;
            browser = b;
            Checkings = new SignalingCheckings();
            storedLines.Log =
            Checkings.Log = delegate (string message) { Log(message); };
            Checkings.LogFailed = delegate (string message) { LogToWindow(message); };
            Checkings.Changed += Checkings_Changed;
        }

        private void Checkings_Changed(object sender, EventArgs e)
        {
            if (null!= CheckingsChanged)
                CheckingsChanged.Invoke(sender,e);
        }

        public void StartScenario()
        {
            LogToWindowIfNotEmpty(@"


------------- start scenario ------------- { ");
            UseScenario(ScenarioPath);
            storedLines.Clear();
            Checkings.ClearAndSignalChange();
            m_ToHideOnShot.Clear();
            signal_to_stread_to_stop = false;
            m_Thread = new Thread(AsyncMain);
            m_Thread.SetApartmentState(ApartmentState.STA);
            m_Thread.Start();
        }

        public void StopAsyncActiveX()
        {
            try
            {
                form.Invoke(new MethodInvoker(delegate () {
                    HtmlDocument document = browser.Document;
                    document.InvokeScript("CPW_StopAsyncThread");
                }));
            }
            catch (Exception e)
            {
                log.Error("can not CPW_StopAsyncThread", e);
            }
        }

        public virtual void Log(string text)
        {
            LogToWindow(text);
            Console.Out.WriteLine(text);
        }

        public bool ProcessCommandLineDoStop(string line, bool quiet)
        {
            return ProcessCommandLineCheckOrDoStop(line, false, quiet);
        }

        public void Clear()
        {
            BatchMode = false;
            PathForResultFiles = null;
            PathToIncludeFrom = null;
            _BaseUrl = null;
            PreUrl = null;
            PreCall = null;
            CheckingFile = null;
            ScenarioPath = null;
            m_ScenarioStream = null;
            m_ScenarioReader = null;
            EchoMode = false;

            RevertBrowserToDefault(quiet: true);
        }

        void UseScenario(string path)
        {
            if (null != path)
            {
                if (!Path.IsPathRooted(path))
                    path = Path.Combine(PathForResultFiles, path);
                ScenarioPath = path;

                TextReader oldScenarioReader = m_ScenarioReader;
                FileStream oldScenarioStream = m_ScenarioStream;

                m_ScenarioStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                m_ScenarioReader = new System.IO.StreamReader(m_ScenarioStream);

                string txt_scenario = m_ScenarioReader.ReadToEnd();
                StringReader txt_scenario_reader = new StringReader(txt_scenario);

                m_ScenarioStream.Close();
                m_ScenarioStream.Dispose();
                m_ScenarioStream = null;

                m_ScenarioReader.Close();
                m_ScenarioReader.Dispose();
                m_ScenarioReader = null;

                m_ScenarioReader = txt_scenario_reader;

                Console.SetIn(m_ScenarioReader);

                if (null != oldScenarioReader)
                {
                    oldScenarioReader.Close();
                    oldScenarioReader.Dispose();
                }

                if (null != oldScenarioStream)
                {
                    oldScenarioStream.Close();
                    oldScenarioStream.Dispose();
                }
            }
        }

        Form form;
        WebBrowser browser;

        const string txtFileUrlPrefix = "file://";

        FileStream m_ScenarioStream;
        TextReader m_ScenarioReader;

        Thread m_Thread;

        StoredLines storedLines = new StoredLines();
        ShotHideHelper m_ToHideOnShot = new ShotHideHelper();

        string _BaseUrl;

        bool CheckJS(string line)
        {
            return HtmlHelper.CheckJS(browser,line);
        }

        bool WaitJs_DoStop_ProcessExceptions(string line, bool check_mode, bool quiet)
        {
            line= line.TrimStart(' ').Substring(2);

            if (!check_mode)
                Log("jw: {0}",line);

            return !WaitFor
            (
                () => { return CheckJS(line);},
                !check_mode ? "ok" : null,
                "!{1} for {0} millseconds",
                line
            );
        }

        bool ExecuteJs_DoStop_ProcessExceptions(string line, bool check_mode, bool quiet)
        {
            try
            {
                line= line.TrimStart(' ').Substring(2);
                ExecuteJS(line,check_mode);
                return false;
            }
            catch (Exception e)
            {
                return ProcessException(e);
            }
        }

        bool ExecuteJe_DoStop_ProcessExceptions(string line, bool check_mode, bool quiet)
        {
            try
            {
                line= line.TrimStart(' ').Substring(2);
                ExecuteJavaScriptLine(line);
                return false;
            }
            catch (Exception e)
            {
                return ProcessException(e);
            }
        }

        bool ExecuteJr_DoStop_ProcessExceptions(string line, bool check_mode, bool quiet)
        {
            try
            {
                line= line.TrimStart(' ').Substring(2);
                ExecuteJavaScriptLinePrintResult(line);
                return false;
            }
            catch (Exception e)
            {
                return ProcessException(e);
            }
        }

        bool ExecuteCommand_DoStop_ProcessExceptions(string cmd
            , string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, bool check_mode, bool quiet)
        {
            try
            {
                return !check_mode 
                    ? ExecuteCommand_DoStop(cmd, arg1, arg2, arg3, arg4, arg5, arg6, quiet)
                    : CheckCommand_DoStop(cmd, arg1, arg2, arg3, arg4, arg5, arg6);
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        bool ProcessException(Exception exception)
        {
            Log(exception.ToString());
            log.Error(exception);
            if (!BatchMode && !signal_to_stread_to_stop)
                throw new Exception("Can not process exception in command loop",exception);
            SafeExit();
            return true;
        }

        bool ExecuteCommand_DoStop(string cmd, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, bool quiet)
        {
            switch (cmd)
            {
                // Общие команды навигации
                case "exit": OnExitFromScenario(); return true;
                case "break": OnBreak(); return false;
                case "goto": GotoUrl(arg1); return false;
                case "wait_loaded": WaitLoaded(); return false;
                case "goto_wait": GotoUrlAndWaitLoaded(arg1); return false;
                case "sleep": Sleep(arg1); return false;

                // Команды ожидания – синхронизации
                case "wait_id": WaitForElementWithId(arg1); return false;
                case "wait_text": return WaitForElementWithText(arg1, arg2);
                case "wait_id_text": return WaitForElementWithIdContainsText(arg1,arg2);
                case "wait_id_value": WaitForElementWithIdWithValue(arg1,arg2); return false;
                case "wait_id_full_value": WaitForElementWithIdWithValueFullText(arg1, arg2); return false;
                case "wait_text_disappeared": WaitForElementWithTextDisappeared(arg1); return false;
                case "wait_value": WaitForElementWithValue(arg1); return false;

                // Команды для имитации действий пользователя
                case "click_text": ClickText(arg1); return false;
                case "async_click_text": return AsyncClickText(arg1);
                case "click_id": return ClickId(arg1);
                case "async_click_id": return AsyncClickId(arg1);
                case "focus_id": FocusId(arg1); return false;
                case "click_id_value": ClickIdValue(arg1,arg2); return false;
                case "click_value": ClickValue(arg1); return false;
                case "double_click_text": DoubleClickText(arg1); return false;
                case "double_click_id": DoubleClickId(arg1); return false;
                case "keydown_enter_id": KeyDownEnterId(arg1); return false;

                case "wait_click_text": return WaitForElementWithTextAndClickIt(arg1);
                case "wait_click_full_text": WaitForElementWithFullTextAndClickIt(arg1); return false;
                case "wait_click_first_text": return WaitForFirstElementWithTextAndClickIt(arg1);
                case "wait_click_hidden_full_text": WaitForElementWithHiddenFullTextAndClickIt(arg1); return false;
                case "wait_click_value": WaitForElementWithValueAndClickIt(arg1); return false;
                case "wait_click_class": WaitForElementWithClassAndClickIt(arg1); return false;
                case "wait_click_class_if_present_class": WaitForElementWithClassAndClickItIfPresentClass(arg1,arg2); return false;
                case "wait_click_id": WaitForElementWithIdAndClickIt(arg1); return false;
                case "wait_double_click_id": WaitForElementWithIdAndDoubleClickIt(arg1); return false;
                case "wait_double_click_tr_text": WaitForTrWithTextAndClickIt(arg1); return false;

                case "type_id": TypeId(arg1, arg2); return false;
                case "set_value_id": SetValueId(arg1, arg2); return false;
                case "set_value_id_from_file": SetValueIdFromFile(arg1, arg2); return false;
                case "wait_type_id": WaitIdType(arg1, arg2); return false;
                case "set_attr_id": SetAttrId(arg1, arg2, arg3); return false;

                // Команды снятия контрольных снимков
                case "dump_html": DumpHtml(arg1); return false;
                case "shot_hide_class": m_ToHideOnShot.AddClassName(arg1); return false;
                case "shot_hide_id": m_ToHideOnShot.AddId(arg1); return false;
                case "shot_hide_attribute_value": m_ToHideOnShot.AddAttributeValue(arg1,arg2); return false;
                case "shot_hide_no": m_ToHideOnShot.Clear(); return false;
                case "shot_check_png": ShotCheckPng(arg1,arg2); return false;
                case "fix_links_color": FixLinksColor(); return false;
                case "check_span_id": CheckSpanId(arg1); return false;
                case "dump_js": DumpJsResult(arg1, arg2); break;

                case "browser_width": ChangeBrowserWidth(arg1); return false;
                case "browser_default": RevertBrowserToDefault(); return false;

                // Работа с windows – специфичными функциями
                case "print_xps":       PrintXps(arg1); return false;
                case "print_check_xps": return !PrintCheckXps(arg1);
                case "print_cancel": return !PrintCancel();
                case "check_opened_xps": return !CheckOpenedXps(arg1);
                case "wait_press_dlg_btn": PressDialogButton(arg1,arg1,arg2,arg2,arg3); return false;
                case "wait_file_dlg_enter_path": return EnterPathForFileDialog(arg1,arg2,arg3);
                case "wait_file_dlg_enter_multi_open": return EnterMultiOpenForFileDialog(arg1, arg2);
                case "wait_enter_random_textbox_dlg": return EnterRandomTextDialogTextBox(arg1, arg2, arg3);
                case "wait_enter_textbox_dlg": return EnterTextDialogTextBox(arg1, arg2, arg3);
                case "wait_dlg_text": return WaitForDialogWithText(arg1, arg2, arg3);

                // Именованные наборы команд и include
                case "include": Include(arg1, arg2); return false;
                case "start_store_lines_as": storedLines.StartStoreLineAs(arg1, quiet); return false;
                /* stop_store_lines  */
                case "play_stored_lines": PlayStoredLines(arg1, arg2, arg3, arg4, arg5, arg6); return false;
                case "check_stored_lines": CheckStoredLines(arg1, arg2, arg3, arg4, arg5, arg6); return false;
                case "execute_javascript_line": ExecuteJavaScriptLine(arg1); return false;
                case "execute_javascript_stored_lines": ExecuteJavaScriptStoredLines(arg1); return false;
                case "execute_javascript_from_file": ExecuteJavaScriptFromFile(arg1); return false;
                case "call_js_width_text_file_content": CallJsWithTextFileResult(arg1, arg2); break;
                case "exec_check_txt": ExecCheckTxt(arg1, arg2); return false;

                // Неописанное
                // case "stop_async_activex": StopAsyncActiveX(); return false; // deprecated!

                default: LogError("unknown command \"{0}\"!",cmd); break;
            }
            return false;
        }

        void ExecCheckTxt(string file_path_to_execute, string file_path_to_out)
        {
            if (string.IsNullOrEmpty(file_path_to_execute))
            {
                Log("can not execute \"{0}\"", file_path_to_execute);
            }
            else
            {
                Log("compare output of \"{0}\" with etalon \"{1}\"", file_path_to_execute,file_path_to_out);
                string fpath = file_path_to_execute;
                if (!Path.IsPathRooted(fpath))
                    fpath= Path.Combine(PathForResultFiles, file_path_to_execute);
                if (!File.Exists(fpath))
                {
                    Log("can not find file \"{0}\"", fpath);
                }
                else
                {
                    LogToWindow(string.Format("to execute \"{0}\"", fpath));
                    Process process = new Process();
                    process.StartInfo.FileName = fpath;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.Start();

                    string txt_result = process.StandardOutput.ReadToEnd();
                    string txt_error = process.StandardError.ReadToEnd();

                    int millisecondsToWait = 10000;
                    if (process.WaitForExit(millisecondsToWait))
                    {
                        Log("finished with code {0}", process.ExitCode);
                    }
                    else
                    {
                        Log("not finished for {0} milliseconds", millisecondsToWait);
                    }

                    if (!string.IsNullOrEmpty(txt_error))
                        Log(txt_error);

                    string fpath_out = file_path_to_out;
                    if (!Path.IsPathRooted(fpath_out))
                        fpath_out = Path.Combine(PathForResultFiles, file_path_to_out);
                    string fpath_ext= Path.GetExtension(fpath_out);
                    string prefix = fpath_out.Substring(0,fpath_out.Length-fpath_ext.Length);
                    string fpath_result = string.Format("{0}.result{1}",prefix,fpath_ext);
                    string fpath_etalon = string.Format("{0}.etalon{1}", prefix, fpath_ext);
                    if (!File.Exists(fpath_etalon))
                    {
                        Log("can not find etalon file {0}", fpath_etalon);
                    }
                    else
                    {
                        string txt_etalon = File.ReadAllText(fpath_etalon);
                        if (txt_etalon!=txt_result)
                        {
                            Log("the result does not match the etalon {0}", fpath_etalon);
                        }
                        else
                        {
                            Log("result and etalon are equal");
                        }
                    }
                    LogToWindow(string.Format("out to \"{0}\"", fpath_result));
                    File.WriteAllText(fpath_result, txt_result);
                }
            }
        }

        int? default_BrowserWidth;
        void ChangeBrowserWidth(string swidth, bool quiet= false)
        {
            int width;
            if (!int.TryParse(swidth,out width))
            {
                LogError("can not parse \"{0}\" as width", swidth);
            }
            else if (width>2000)
            {
                LogError("too large width {0}", width);
            }
            {
                form.Invoke(new MethodInvoker(delegate
                    {
                        if (null == default_BrowserWidth)
                            default_BrowserWidth = browser.Width;
                        browser.Width = width;
                        if (!quiet)
                            Log("set browser width to {0}", width);
                    }
                ));
            }
        }

        void RevertBrowserToDefault(bool quiet= false)
        {
            if (null != default_BrowserWidth)
            {
                int width = default_BrowserWidth.Value;
                form.Invoke(new MethodInvoker(delegate
                {
                    browser.Width = width;
                }));
                if (!quiet)
                    Log("set browser width to default {0}", width);
            }
        }

        void OnBreak()
        {
            Log("break!");
            thread_is_paused= true;
            if (null!= unprotect_browser)
                unprotect_browser.Invoke();
        }

        void OnExitFromScenario()
        {
            if (!string.IsNullOrEmpty(CheckingFile))
            {
                Checkings checkings= new Checkings();
                checkings.Capacity= Checkings.Count;
                checkings.AddRange(Checkings.Select(c => new Checking(c)));
                checkings.FixFileNamesAsRelativePathForRootPath(Path.GetDirectoryName(CheckingFile));
                checkings.WriteToXmlFile(CheckingFile);
            }
            Log("exit");
        }

        bool CheckCommand_DoStop(string cmd, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            switch (cmd)
            {
                case "click_id_value": CheckClickIdValue(arg1,arg2); return false;
                case "type_id": CheckTypeId(arg1,arg2); return false;
                case "set_value_id": CheckTypeId(arg1,arg2); return false;

                case "wait_click_text": return WaitForElementWithTextAndClickIt(string.IsNullOrEmpty(arg2) ? arg1 : arg2);
                case "wait_text": return WaitForElementWithText(arg1, arg2);
                case "wait_text_disappeared": WaitForElementWithTextDisappeared(arg1); return false;

                case "play_stored_lines": CheckStoredLines(arg1, arg2, arg3, arg4, arg5, arg6); return false;

                default: Log("unknown command!"); break;
            }
            return false;
        }

        void FixLinksColor()
        {
            Log("fix links color");
            form.Invoke(new MethodInvoker(delegate
                {
                    HtmlHelper.FixLinksColor(browser,false);
                }
            ));
        }

        void LogLinesAction(string prefix, string name, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            if (string.IsNullOrEmpty(arg2))
            {
                Log("{1} stored lines \"{0}\"", name, prefix);
            }
            else if (string.IsNullOrEmpty(arg3))
            {
                Log("{1} stored lines \"{0}\" (\"{2}\")", name, prefix, arg2);
            }
            else if (string.IsNullOrEmpty(arg4))
            {
                Log("{1} stored lines \"{0}\" (\"{2}\",\"{3}\")", name, prefix, arg2, arg3);
            }
            else if (string.IsNullOrEmpty(arg5))
            {
                Log("{1} stored lines \"{0}\" (\"{2}\",\"{3}\",\"{4}\")", name, prefix, arg2, arg3, arg4);
            }
            else if (string.IsNullOrEmpty(arg6))
            {
                Log("{1} stored lines \"{0}\" (\"{2}\",\"{3}\",\"{4}\",\"{5}\")", name, prefix, arg2, arg3, arg4, arg5);
            }
            else
            {
                Log("{1} stored lines \"{0}\" (\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\")", name, prefix, arg2, arg3, arg4, arg5, arg6);
            }
        }

        void PlayStoredLines(string name, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            List<string> slines;
            if (!storedLines.linesByName.TryGetValue(name, out slines))
            {
                Log("can not find stored lines with name \"{0}\"",name);
            }
            else
            {
                LogLinesAction("start play",name,arg2,arg3,arg4,arg5,arg6);
                if (!string.IsNullOrEmpty(arg2))
                {
                    List<string> fixed_lines = new List<string>();
                    foreach (string sline in slines)
                    {
                        string line = fix_stored_line(sline, arg2, arg3, arg4, arg5, arg6);
                        fixed_lines.Add(line);
                    }
                    slines = fixed_lines;
                }
                PlayStoredLinesInternal(slines);
                LogLinesAction("stop play", name, arg2, arg3, arg4, arg5, arg6);
            }
        }

        static string fix_stored_line(string sline, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            string line = sline.Trim();
            if (!string.IsNullOrEmpty(arg2))
            {
                line = line.Replace("%1", arg2);
                if (!string.IsNullOrEmpty(arg3))
                {
                    line = line.Replace("%2", arg3);
                    if (!string.IsNullOrEmpty(arg4))
                    {
                        line = line.Replace("%3", arg4);
                        if (!string.IsNullOrEmpty(arg5))
                        {
                            line = line.Replace("%4", arg5);
                            if (!string.IsNullOrEmpty(arg6))
                            {
                                line = line.Replace("%5", arg6);
                            }
                        }
                    }
                }
            }
            return line;
        }

        void CheckStoredLines(string name, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            List<string> slines;
            if (!storedLines.linesByName.TryGetValue(name, out slines))
            {
                Log("can not find stored lines with name \"{0}\"",name);
            }
            else
            {
                LogLinesAction("start check", name, arg2, arg3, arg4, arg5, arg6);
                foreach (string sline in slines)
                {
                    string line= fix_stored_line(sline, arg2, arg3, arg4, arg5, arg6);
                    using (LineViewer.CreateToCheck(show_the_line, line))
                    {
                        if (CheckCommandLineDoStop(line))
                            break;
                    }
                }
                LogLinesAction("stop check", name, arg2, arg3, arg4, arg5, arg6);
            }
        }

        void ExecuteJavaScriptLinePrintResult(string line)
        {
            Log("execute javascript line: {0}",line);
            form.Invoke(new MethodInvoker(delegate{
                string res= HtmlHelper.ExecuteJavascriptLineReturnResult(browser,line);
                Log("result: {0}",res);
            }));
        }

        void ExecuteJavaScriptLine(string line)
        {
            Log("execute javascript line: {0}",line);
            form.Invoke(new MethodInvoker(delegate{HtmlHelper.ExecuteJavascript(browser,line);}));
        }

        void ExecuteJS_CheckMode(string line)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                string res = HtmlHelper.ExecuteJS(browser, line, true);
                if (!string.IsNullOrEmpty(res) && "#null" != res)
                {
                    Log("js: {0}", line);
                    Log(res);
                }
            }));
        }

        void ExecuteJS_ExecuteMode(string line)
        {
            Log("js: {0}", line);
            form.Invoke(new MethodInvoker(delegate
            {
                string res = HtmlHelper.ExecuteJS(browser, line, false);
                if ("wait_long_process!" != res)
                {
                    Log(res);
                }
                else
                {
                    object ms_waited;
                    bool checked_ok= WaitFor
                    (
                        out ms_waited,
                        () => 
                        { 
                            res= HtmlHelper.ExecuteJS(browser, "wbt_CheckIfLongProcessFinished();", false);
                            if ("wait_long_process!" == res)
                            {
                                return false;
                            }
                            else
                            {
                                Log(res);
                                return true;
                            }
                        }
                    );
                    if (!checked_ok)
                        Log(string.Format("not finished for {0} milliseconds!!!", ms_waited));
                }
            }));
        }

        void ExecuteJS(string line, bool check_mode)
        {
            if (check_mode)
            {
                ExecuteJS_CheckMode(line);
            }
            else
            {
                ExecuteJS_ExecuteMode(line);
            }
        }

        void ExecuteJavaScriptStoredLines(string name)
        {
            List<string> slines;
            if (!storedLines.linesByName.TryGetValue(name, out slines))
            {
                Log("can not find stored lines with name \"{0}\"",name);
            }
            else
            {
                Log("start javascript stored lines \"{0}\"",name);
                StringBuilder sb= new StringBuilder();
                foreach (string sline in slines)
                {
                    sb.AppendLine(sline);
                }
                form.Invoke(new MethodInvoker(delegate{HtmlHelper.ExecuteJavascript(browser,sb.ToString());}));
                Log("stop javascript stored lines \"{0}\"",name);
            }
        }

        void ExecuteJavaScriptFromFile(string file_name)
        {
            string full_file_name = Path.IsPathRooted(file_name) ? file_name : Path.Combine(PathForResultFiles, file_name);
            if (!File.Exists(full_file_name))
            {
                Log("can not find file with name \"{0}\"", full_file_name);
            }
            else
            {
                string content = File.ReadAllText(full_file_name);
                if (string.IsNullOrEmpty(content))
                {
                    Log("file is empty, name of file \"{0}\"", full_file_name);
                }
                else
                {
                    Log("start javascript from file \"{0}\"", file_name);
                    form.Invoke(new MethodInvoker(delegate { HtmlHelper.ExecuteJavascript(browser, content); }));
                    Log("stop javascript from file \"{0}\"", file_name);
                }
            }
        }

        void GotoUrl(string url)
        {
            if (null!= BaseUrl)
            {
                Log("navigate to \"{0}\"", url);
                string full_url = BaseUrl + url;
                Uri uri = new Uri(full_url);
                browser.Navigate(full_url);
            }
        }

        void GotoUrlAndWaitLoaded(string url)
        {
            GotoUrl(url);
            WaitLoaded();
        }

        protected virtual void SafeExit()
        {
            if (null != Stop)
                Stop(this, EventArgs.Empty);

            if (BatchMode)
            {
                StopAsyncActiveX();
                form.Invoke(new MethodInvoker(delegate()
                {
                    form.Close();
                }));
                Application.Exit();
            }
        }

        void Log(string tpl, params object[] p)
        {
            Log(string.Format(tpl,p));
        }

        void LogToWindowCheckIfEmpty(string text, bool ifnotempty)
        {
            log.Debug(text);
            if (null!=show_log_line_check_if_empty)
                show_log_line_check_if_empty.Invoke(text,ifnotempty);
        }

        void LogToWindow(string text)
        {
            LogToWindowCheckIfEmpty(text, false);
        }

        void LogToWindowIfNotEmpty(string text)
        {
            LogToWindowCheckIfEmpty(text, true);
        }

        void EchoCmd(string cmd, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            StringBuilder txt= new StringBuilder();
            txt.Append("c\"");
            txt.Append(cmd);
            if (!string.IsNullOrEmpty(arg1))
            {
                txt.Append("\" \"");
                txt.Append(arg1);
                if (!string.IsNullOrEmpty(arg2))
                {
                    txt.Append("\" \"");
                    txt.Append(arg2);

                    if (!string.IsNullOrEmpty(arg3))
                    {
                        txt.Append("\" \"");
                        txt.Append(arg3);

                        if (!string.IsNullOrEmpty(arg4))
                        {
                            txt.Append("\" \"");
                            txt.Append(arg4);

                            if (!string.IsNullOrEmpty(arg5))
                            {
                                txt.Append("\" \"");
                                txt.Append(arg5);

                                if (!string.IsNullOrEmpty(arg6))
                                {
                                    txt.Append("\" \"");
                                    txt.Append(arg6);
                                }
                            }
                        }
                    }
                }
            }
            txt.Append("\"");
            Log(txt.ToString());
        }

        void SafePreGotoUrl()
        {
            if ("." == PreUrl)
            {
                GotoUrlAndWaitLoaded("");
            }
            else if (!string.IsNullOrEmpty(PreUrl))
            {
                GotoUrlAndWaitLoaded(PreUrl);
            }
        }

        public void SafePreCall()
        {
            if (!string.IsNullOrEmpty(PreCall))
            {
                string fpath= PreCall;
                if (!Path.IsPathRooted(fpath))
                    fpath= Path.Combine(PathForResultFiles,PreCall);
                Path.IsPathRooted(PreCall);
                LogToWindow(string.Format("precall \"{0}\"", fpath));
                Process process = new Process();
                process.StartInfo.FileName = fpath;
                process.StartInfo.WindowStyle= ProcessWindowStyle.Hidden;
                process.Start();
                int millisecondsToWait= 5000;
                if (process.WaitForExit(millisecondsToWait))
                {
                    LogToWindow(string.Format("finished with code {0}", process.ExitCode));
                }
                else
                {
                    LogToWindow(string.Format("not finished for {0} milliseconds", millisecondsToWait));
                }
            }
        }

        void AsyncMain()
        {
            thread_is_active= true;
            SafeSignalThreadChanged();
            Checkings.ClearErrors(PathForResultFiles);
            try
            {
                if (null==OnRestart)
                    SafePreCall();
                SafePreGotoUrl();
                if (null!=ScenarioPath)
                {
                    PlayLines(TextReaderLines(Console.In), false);
                    Console.In.Close();
                    SafeExit();
                }
            }
            finally
            {
                thread_is_active= false;
                signal_to_stread_to_stop= false;
                SafeSignalThreadChanged();
            }
        }

        void PlayLines(IEnumerable<string> slines, bool quiet)
        {
            foreach (string line in slines)
            {
                string trimmed_line= line.Trim();
                using (LineViewer.CreateToExecute(show_the_line, trimmed_line))
                {
                    if (signal_to_stread_to_stop || null == line)
                        return;
                    while (thread_is_paused && !signal_to_thread_to_step)
                    {
                        SafeSignalThreadChanged();
                        Thread.Sleep(100);
                        SafeSignalThreadChanged();
                        if (signal_to_stread_to_stop)
                            return;
                    }
                    if (!storedLines.SafeStoreLine(line))
                    {
                        try
                        {
                            if (ProcessCommandLineDoStop(line,quiet))
                                return;
                        }
                        finally
                        {
                            signal_to_thread_to_step = false;
                        }
                    }
                }
            }
        }

        IEnumerable<string> TextReaderLines(TextReader reader)
        {
            for (string sline = reader.ReadLine(); null != sline; sline = reader.ReadLine())
            {
                yield return sline;
            }
        }

        void PlayStoredLinesInternal(List<string> slines)
        {
            PlayLines(slines,false);
        }

        void IncludeInternal(string file_path, bool quiet)
        {
            string content= File.ReadAllText(file_path);
            using (StringReader reader = new StringReader(content))
            {
                PlayLines(TextReaderLines(reader),quiet);
            }
        }

        void SafeSignalThreadChanged()
        {
            if (null!= ThreadChanged)
            {
                try
                {
                    ThreadChanged(this, null);
                }
                catch (Exception ex)
                {
                    log.Error("can not show change in thread", ex);
                }
            }
        }

        bool CheckCommandLineDoStop(string line)
        {
            return ProcessCommandLineCheckOrDoStop(line,true,false);
        }

        class LineViewer : IDisposable
        {
            Action<string> m_show_line;
            internal LineViewer(Action<string> show_line, string line, bool check_mode)
            {
                m_show_line = show_line;
                string txt= !check_mode ? line : ("check " + line);
                ShowCommandLine(txt);
            }
            public void Dispose()
            {
                try
                {
                    ShowCommandLine("");
                }
                catch (Exception e)
                {
                    log.Error("can not clear executing line",e);
                }
            }
            void ShowCommandLine(string txt)
            {
                if (null!= m_show_line)
                    m_show_line.Invoke(txt);
            }
            static internal LineViewer CreateToExecute(Action<string> show_line, string line)
            {
                return new LineViewer(show_line, line,false);
            }
            static internal LineViewer CreateToCheck(Action<string> show_line, string line)
            {
                return new LineViewer(show_line, line,true);
            }
        }

        bool ProcessCommandLineCheckOrDoStop(string line, bool check_mode, bool quiet)
        {
            line = line.Trim();
            if (0 != line.Length)
            {
                if (line.StartsWith("#"))
                {
                    Log(line);
                }
                else
                {
                    string cmd, arg1, arg2, arg3, arg4, arg5, arg6;
                    CommandLineHelper.GetCommandAndArguments(line, out cmd, out arg1, out arg2, out arg3, out arg4, out arg5, out arg6);
                    if (EchoMode && !quiet)
                        EchoCmd(cmd, arg1, arg2, arg3, arg4, arg5, arg6);
                    if ("js"==cmd)
                    {
                        return ExecuteJs_DoStop_ProcessExceptions(line,check_mode,quiet);
                    }
                    else if ("jw"==cmd)
                    {
                        return WaitJs_DoStop_ProcessExceptions(line,check_mode,quiet);
                    }
                    else if ("je"==cmd)
                    {
                        return ExecuteJe_DoStop_ProcessExceptions(line,check_mode,quiet);
                    }
                    else if ("jr"==cmd)
                    {
                        return ExecuteJr_DoStop_ProcessExceptions(line,check_mode,quiet);
                    }
                    else if (ExecuteCommand_DoStop_ProcessExceptions(cmd, arg1, arg2, arg3, arg4, arg5, arg6, check_mode, quiet))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        Size CalcShotSize()
        {
            HtmlDocument document = browser.Document;
            Rectangle ScrollRectangle= document.Body.ScrollRectangle;
            int bwidth = ScrollRectangle.Right;
            browser.Width = bwidth;
            int bheight= ScrollRectangle.Bottom;
            try
            {
                foreach (HtmlElement element in document.All)
                {
                    int height = element.ScrollRectangle.Bottom;
                    if (bheight < height)
                        bheight = height;
                }
            }
            catch (Exception ex)
            {
                log.Error("can not iterate elements for screenshot", ex);
            }
            if (10000 < bheight)
                bheight = 10000;
            return new Size(bwidth, bheight + 20);
        }

        void ShotPng(Stream stream, bool fix_links_important)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlHelper.FixLinksColor(browser,fix_links_important);
                int oldHeight= browser.Height;
                int oldWidth= browser.Width;
                try
                {
                    HtmlDocument document = browser.Document;
                    browser.Size = CalcShotSize();
                    Rectangle rect = new Rectangle(0, 0, browser.Width, browser.Height);
                    using (Bitmap bitmap = new Bitmap(browser.Width, browser.Height))
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        IntPtr hdc = graphics.GetHdc();
                        try
                        {
                            IViewObject viewObject = (IViewObject)document.DomDocument;
                            viewObject.Draw(1, -1, (IntPtr)0, (IntPtr)0, (IntPtr)0, hdc, rect, (IntPtr)0, (IntPtr)0, 0);
                        }
                        finally
                        {
                            graphics.ReleaseHdc(hdc);
                        }
                        m_ToHideOnShot.Hide(browser, graphics);
                        bitmap.Save(stream,System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
                finally
                {
                    browser.Height = oldHeight;
                    browser.Width = oldWidth;
                }
            }));
        }

        void ShotErrorPng(string message_tpl, params object[] p)
        {
            ShotErrorPng(string.Format(message_tpl,p));
        }

        void ShotErrorPng(string message)
        {
            using (MemoryStream result_stream = new MemoryStream())
            {
                ShotPng(result_stream,false);
                string prefix= Path.Combine(PathForResultFiles,DateTime.Now.ToString("HHmmssfff").Replace(":",""));
                Checkings.LogError(prefix,result_stream,message);
            }
        }

        double? PreShotCheckPng_get_MaxPngBytesDiff(string args)
        {
            double? MaxPngBytesDiff= null;
            if (!string.IsNullOrEmpty(args) && "true"!=args)
            {
                string [] parts = args.Split(',');
                foreach (string part in parts)
                {
                    string[] parts2 = part.Split(':');
                    if (2!= parts2.Length)
                    {
                        LogError("can not splite \"{0}\" by :!",part);
                    }
                    else
                    {
                        string par = parts2[0];
                        switch (par)
                        {
                            case "width": ChangeBrowserWidth(parts2[1], quiet: true); break;
                            case "max-diff": MaxPngBytesDiff = double.Parse(parts2[1], System.Globalization.CultureInfo.InvariantCulture); break;
                            default: LogError("Неизвестный аргумент {0}, возможно используется устаревшая версия wbt",par); break;
                        }
                    }
                }
            }
            return MaxPngBytesDiff;
        }

        void ShotCheckPng(string fspath, string args)
        {
            double? MaxPngBytesDiff= PreShotCheckPng_get_MaxPngBytesDiff(args);
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
            string ext= Path.GetExtension(fpath);
            string prefix= fpath.Substring(0,fpath.Length-ext.Length);
            using (MemoryStream result_stream = new MemoryStream())
            {
                bool bfix_links_important = "true"== args;
                ShotPng(result_stream, bfix_links_important);
                if (string.IsNullOrEmpty(args))
                {
                    Log("shot \"{0}\"", fspath);
                }
                else
                {
                    Log("shot \"{0}\" {1}", fspath, args);
                }
                try
                {
                    Checkings.PreCheckPngStream(prefix, result_stream, MaxPngBytesDiff);
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    Log("{0} catched DirectoryNotFoundException for file \"{1}\"", DateTime.Now, prefix);
                }
            }
            PostShotCheckPng(args);
        }

        void PostShotCheckPng(string args)
        {
            if (!string.IsNullOrEmpty(args) && "true" != args)
                RevertBrowserToDefault(quiet:true);
        }

        void Sleep(uint milliseconds)
        {
            Log("sleep {0} milliseconds",milliseconds);
            //Thread.Sleep((int)milliseconds);
            Application.DoEvents();
            WinHelper.SleepEx(milliseconds,false);
            Application.DoEvents();
        }

        void Sleep(string pause)
        {
            uint milliseconds= uint.Parse(pause);
            Sleep(milliseconds);
        }

        void DumpValueForId(string id, string fspath)
        {
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
        }

        void DumpHtml(string fspath)
        {
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
            form.Invoke(new MethodInvoker(delegate
            {
                File.WriteAllText(fpath,browser.DocumentText);
                Log("документ вид сохранён в \"{0}\"",fspath);
            }));
        }

        void CallJsWithTextFileResult(string js_function_name, string fspath)
        {
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
            string txt= File.ReadAllText(fpath);
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlHelper.ExecuteJsFunctionWithArg(browser,js_function_name,txt);
                Log("вызвана функция \"{0}\" с содержимым файла \"{1}\"",js_function_name,fspath);
            }));
        }

        void DumpJsResult(string js_function_name, string fspath)
        {
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
            form.Invoke(new MethodInvoker(delegate
            {
                string res= HtmlHelper.ExecuteJsFunction(browser,js_function_name);
                if (null == res)
                {
                    File.Delete(fpath);
                }
                else
                {
                    File.WriteAllText(fpath, res);
                }
                Log("результат вызова функции \"{0}\" сохранён в \"{1}\"",js_function_name,fspath);
            }));
        }

        void WaitLoaded()
        {
            bool res= WaitFor
            (
                () => { return WebBrowserReadyState.Complete == browser.ReadyState;},
                "web document completed",
                "web document is not completed for {0} millseconds"
            );
            if (!res)
            {
                WebBrowserReadyState ready_state= WebBrowserReadyState.Uninitialized;
                form.Invoke(new MethodInvoker(delegate
                {
                    ready_state= browser.ReadyState;
                }));
                Log("current document state is {0}", ready_state);
            }
        }

        bool AsyncClickText(string txt)
        {
            return !AsyncClick
            (
                ()=>{return HtmlHelper.FindLastElementWithText(browser,txt);},
                "text \"{0}\" is clicked",
                "text \"{0}\" is NOT present",
                txt
            );
        }

        void ClickText(string txt)
        {
            Click
            (
                ()=>{return HtmlHelper.FindLastElementWithText(browser,txt);},
                "text \"{0}\" is clicked",
                "text \"{0}\" is NOT present",
                txt
            );
        }

        void DoubleClickText(string txt)
        {
            DoubleClick
            (
                ()=>{return HtmlHelper.FindLastElementWithText(browser,txt);},
                "text \"{0}\" is double clicked",
                "text \"{0}\" is NOT present",
                txt
            );
        }

        

        void KeyDownEnterId(string element_id)
        {
            KeydownEnter
            (
                ()=>{return browser.Document.GetElementById(element_id);},
                "id {0} trigger keydown enter",
                "id {0} is NOT present",
                element_id
            );
        }

        void DoubleClickId(string element_id)
        {
            DoubleClick
            (
                ()=>{return browser.Document.GetElementById(element_id);},
                "id {0} is double clicked",
                "id {0} is NOT present",
                element_id
            );
        }

        bool ClickId(string element_id)
        {
            return !Click
            (
                ()=>{return browser.Document.GetElementById(element_id);},
                "id \"{0}\" is clicked",
                "id \"{0}\" is NOT present",
                element_id
            );
        }

        bool AsyncClickId(string element_id)
        {
            return !AsyncClick
            (
                ()=>{return browser.Document.GetElementById(element_id);},
                "async click id \"{0}\"",
                "id \"{0}\" is NOT present",
                element_id
            );
        }

        void FocusId(string element_id)
        {
            Focus
            (
                ()=>{return browser.Document.GetElementById(element_id);},
                "id \"{0}\" is focused",
                "id \"{0}\" is NOT present",
                element_id
            );
        }

        void CheckSpanId(string element_id)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= browser.Document.GetElementById(element_id);
                if (null == element)
                {
                    LogError("can not find element with id {0}",element_id);
                }
                else
                {
                    element.Focus();
                    Log("text of \"{0}\" is \"{1}\"",element_id,element.InnerText);
                }
            }));
        }

        bool CheckClickIdValue(string element_id, string value)
        {
            bool check_ok= false;
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= HtmlHelper.FindElementWithIdValue(browser,element_id,value);
                if (null == element)
                {
                    ShotErrorPng("can not find element with id {0} and value \"{1}\"",element_id,value);
                }
                else
                {
                    string c= element.GetAttribute("checked");
                    if ("True" == c)
                    {
                        check_ok= true;
                    }
                    else
                    {
                        ShotErrorPng("id {0} with value \"{1}\" should be checked!",element_id,value);
                    }
                }
            }));
            return check_ok;
        }

        void ClickIdValue(string element_id, string value)
        {
            Click
            (
                ()=>{return HtmlHelper.FindElementWithIdValue(browser,element_id,value);},
                "id \"{0}\" with value \"{1}\" is clicked",
                "id \"{0}\" with value \"{1}\" is NOT present",
                element_id, value
            );
        }

        void ClickValue(string atxt)
        {
            string txt= atxt.Replace("_"," ");
            Click
            (
                ()=>{return HtmlHelper.FindElementWithValue(browser,txt);},
                "value \"{0}\" is clicked",
                "value \"{0}\" is NOT present",
                txt
            );
        }

        delegate HtmlElement FindElement();
        bool Click(FindElement findElement, string tpl_ok, string tpl_fail, params object[] p)
        {
            bool res= false;
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= findElement();
                if (null == element)
                {
                    LogError(tpl_fail, p);
                }
                else
                {
                    res= true;
                    HtmlHelper.ClickElement(element);
                    Log(tpl_ok,p);
                }
            }));
            return res;
        }

        bool AsyncClick(FindElement findElement, string tpl_ok, string tpl_fail, params object[] p)
        {
            bool res= false;
            HtmlElement element= null;
            form.Invoke(new MethodInvoker(delegate
            {
                element= findElement();
                if (null == element)
                {
                    LogError(tpl_fail, p);
                }
                else
                {
                    res= true;
                    Log(tpl_ok,p);
                }
            }));
            if (res)
            {
                form.BeginInvoke(new MethodInvoker(delegate
                {
                    HtmlHelper.ClickElement(element);
                }));
            }
            return res;
        }

        void Focus(FindElement findElement, string tpl_ok, string tpl_fail, params object[] p)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= findElement();
                if (null == element)
                {
                    LogError(tpl_fail,p);
                }
                else
                {
                    element.Focus();
                    Log(tpl_ok,p);
                }
            }));
        }

        void DoubleClick(FindElement findElement, string tpl_ok, string tpl_fail, params object[] p)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= findElement();
                if (null == element)
                {
                    LogError(tpl_fail,p);
                }
                else
                {
                    HtmlHelper.DoubleClickElement(browser,element);
                    Log(tpl_ok,p);
                }
            }));
        }

        void KeydownEnter(FindElement findElement, string tpl_ok, string tpl_fail, params object[] p)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= findElement();
                if (null == element)
                {
                    LogError(tpl_fail,p);
                }
                else
                {
                    HtmlHelper.KeydownEnter(browser,element);
                    Log(tpl_ok,p);
                }
            }));
        }

        void LogError(string txt_tpl, params object[] p)
        {
            string msg= string.Format(txt_tpl,p);
            LogError(msg);
        }

        void LogError(string txt)
        {
            string error_txt = string.Format("{0} time:{1}", txt, DateTime.Now.ToString());
            Log(error_txt);
            ShotErrorPng(txt);
        }

        bool WaitFor(out object ms_waited, OkFinishWaiting okFinishWaiting, bool without_invoke, TimeSpan? ts= null)
        {
            if (null == ts)
                ts = new TimeSpan(0, 0, 10);
            return WaitFor(ts.Value, out ms_waited, okFinishWaiting, without_invoke);
        }

        bool WaitFor(TimeSpan max_pause, out object ms_waited, OkFinishWaiting okFinishWaiting, bool without_invoke)
        {
            DateTime start= DateTime.Now;
            DateTime finish= start + max_pause;
            DateTime t = start;
            bool checking= false;
            bool checked_ok= false;
            for (; !checked_ok && (t < finish) && (!thread_is_paused || signal_to_thread_to_step) && !signal_to_stread_to_stop; t = DateTime.Now)
            {
                Application.DoEvents();
                if (!checking)
                {
                    if (without_invoke)
                    {
                        if (okFinishWaiting())
                            checked_ok = true;
                    }
                    else
                    {
                        checking = true;
                        form.Invoke(new MethodInvoker(delegate
                        {
                            if (okFinishWaiting())
                                checked_ok = true;
                            checking = false;
                        }));
                    }
                }
            }
            ms_waited = (t - start).TotalMilliseconds;
            return checked_ok;
        }

        bool WaitFor_WithoutInvoke(TimeSpan max_pause, out object ms_waited, OkFinishWaiting okFinishWaiting)
        {
            return WaitFor(max_pause, out ms_waited, okFinishWaiting, true);
        }

        bool WaitFor_WithoutInvoke(out object ms_waited, OkFinishWaiting okFinishWaiting)
        {
            return WaitFor(out ms_waited, okFinishWaiting, true);
        }

        bool WaitFor(out object ms_waited, OkFinishWaiting okFinishWaiting, TimeSpan? ts= null)
        {
            return WaitFor(out ms_waited, okFinishWaiting, false, ts);
        }

        delegate bool OkFinishWaiting();

        bool WaitFor(OkFinishWaiting okFinishWaiting, Action on_ok, string tpl_fail, TimeSpan? ts, params object [] p)
        {
            object ms_waited;
            bool checked_ok= WaitFor(out ms_waited, okFinishWaiting, ts);
            if (checked_ok)
            {
                on_ok();
            }
            else
            {
                if (null == p || 0 == p.Length)
                {
                    LogError(tpl_fail, ms_waited);
                }
                else
                {
                    LogError(tpl_fail, (new object[] { ms_waited }).Concat(p).ToArray());
                }
            }
            return checked_ok;
        }

        bool WaitFor(OkFinishWaiting okFinishWaiting, string tpl_ok, string tpl_fail, params object [] p)
        {
            return WaitFor(okFinishWaiting, delegate (){if (null!=tpl_ok)Log(tpl_ok,p);}, tpl_fail, null, p);
        }

        bool WaitForTs(OkFinishWaiting okFinishWaiting, string tpl_ok, string tpl_fail, TimeSpan? ts, params object[] p)
        {
            return WaitFor(okFinishWaiting, delegate () { if (null != tpl_ok) Log(tpl_ok, p); }, tpl_fail, ts, p);
        }

        void WaitForElementWithId(string element_id)
        {
            WaitFor
            (
                () => { return null != browser.Document.GetElementById(element_id);},
                "id {0} is present",
                "id {1} is NOT present for {0} millseconds",
                element_id
            );
        }

        void WaitForElementWithIdAndClickIt(string element_id)
        {
            WaitFor
            (
                () => { return SafeClick(browser.Document.GetElementById(element_id));},
                "id {0} is clicked",
                "id {1} is NOT present for {0} millseconds",
                element_id
            );
        }

        void WaitForTrWithTextAndClickIt(string txt)
        {
            WaitFor
            (
                () => { return SafeDoubleClick(HtmlHelper.FindTrWithText(browser,txt));},
                "tr with text \"{0}\" is double  clicked",
                "tr with text \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithIdAndDoubleClickIt(string element_id)
        {
            WaitFor
            (
                () => { return SafeDoubleClick(browser.Document.GetElementById(element_id));},
                "id {0} is double  clicked",
                "id {1} is NOT present for {0} millseconds",
                element_id
            );
        }

        void WaitForElementWithIdWithValueFullText(string element_id, string atxt)
        {
            string text= atxt.Replace("_"," ");
            WaitFor
            (
                () =>
                {
                    HtmlElement element= browser.Document.GetElementById(element_id);
                    if (null == browser.Document.GetElementById(element_id))
                    {
                        return false;
                    }
                    else
                    {
                        string txt= HtmlHelper.GetElementValue(element);
                        return txt==text;
                    }
                },
                "element with id \"{0}\" and value full text \"{1}\" is present",
                "element with id \"{1}\" and value full text \"{2}\" is NOT present for {0} millseconds",
                element_id,text
            );
        }

        void WaitForElementWithIdWithValue(string element_id, string atxt)
        {
            string text= atxt.Replace("_"," ");
            WaitFor
            (
                () =>
                {
                    HtmlElement element= browser.Document.GetElementById(element_id);
                    if (null == browser.Document.GetElementById(element_id))
                    {
                        return false;
                    }
                    else
                    {
                        string txt= HtmlHelper.GetElementValue(element);
                        return null!=txt && txt.Contains(text);
                    }
                },
                "element with id \"{0}\" and value \"{1}\" is present",
                "element with id \"{1}\" and value \"{2}\" is NOT present for {0} millseconds",
                element_id,text
            );
        }

        void WaitForElementWithValue(string atxt)
        {
            string txt= atxt.Replace("_"," ");
            WaitFor
            (
                () => { return null != HtmlHelper.FindElementWithValue(browser,txt);},
                "value \"{0}\" is present",
                "value \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithTextDisappeared(string txt)
        {
            WaitFor
            (
                () => { return null == HtmlHelper.FindLastElementWithText(browser,txt);},
                "text \"{0}\" is not present",
                "text \"{1}\" is present for {0} millseconds",
                txt
            );
        }

        bool WaitForElementWithIdContainsText(string element_id, string text)
        {
            return !WaitFor
            (
                () =>
                {
                    HtmlElement element= browser.Document.GetElementById(element_id);
                    if (null == browser.Document.GetElementById(element_id))
                    {
                        return false;
                    }
                    else
                    {
                        string txt= element.InnerText;
                        return null!=txt && txt.Contains(text);
                    }
                },
                "id {0} with text \"{1}\" is present",
                "id {1} with text \"{2}\" is NOT present for {0} millseconds",
                element_id,
                text
            );
        }

        bool WaitForElementWithText(string txt, string spause_seconds)
        {
            TimeSpan? ts = null;
            if (!string.IsNullOrEmpty(spause_seconds))
            {
                int pause_seconds;
                if (int.TryParse(spause_seconds, out pause_seconds))
                    ts = new TimeSpan(0, 0, pause_seconds);
            }
            return !WaitForTs
            (
                () => { return null != HtmlHelper.FindLastElementWithText(browser,txt);},
                "text \"{0}\" is present",
                "text \"{1}\" is NOT present for {0} millseconds",
                ts,
                txt
            );
        }

        void WaitIdType(string element_id, string txt)
        {
            WaitFor
            (
                () => { return SafeType(browser.Document.GetElementById(element_id),txt);},
                "id {0} is typed",
                "id {1} is NOT present for {0} millseconds",
                element_id
            );
        }

        void Include(string arg1, string arg2)
        {
            Include(arg1,"quiet"==arg2);
        }

        void Include(string afile_path, bool quiet)
        {
            string lPathToIncludeFrom= string.IsNullOrEmpty(PathToIncludeFrom) ? PathForResultFiles : PathToIncludeFrom;
            string file_path= Path.IsPathRooted(afile_path) ? afile_path : Path.Combine(lPathToIncludeFrom,afile_path);
            try
            {
                PathToIncludeFrom= Path.GetDirectoryName(file_path);
                if (!File.Exists(file_path))
                {
                    Log("can not find file \"{0}\"", file_path);
                }
                else
                {
                    Log("start play lines from file \"{0}\"", afile_path);
                    IncludeInternal(file_path, quiet);
                    Log("stop play lines from file \"{0}\"", afile_path);
                }
            }
            finally
            {
                PathToIncludeFrom= lPathToIncludeFrom;
            }
        }

        bool WaitForDialogWithText(string dlg_caption, string elem_class, string elem_text)
        {
            return !WaitFor
            (
                () => { return !FindDialogWithElementText(dlg_caption, elem_class, elem_text);},
                "text \"{0}\" is present",
                "text \"{1}\" is NOT present for {0} millseconds",
                elem_text
            );
        }

        void SetAttrId(string element_id, string name, string value)
        {
            WaitFor
            (
                () => { return SafeSetAttrId(browser.Document.GetElementById(element_id),name,value);},
                string.Format("change attr {1} for id {0} to \"{2}\"","{0}",name,value),
                "id {1} is NOT present for {0} millseconds",
                element_id
            );
        }

        bool CheckTypeId(string element_id, string txt)
        {
            bool ok_check= false;
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= browser.Document.GetElementById(element_id);
                if (null == element)
                {
                    ShotErrorPng("can not find element with id {0}",element_id);
                }
                else
                {
                    string value= HtmlHelper.GetElementValue(element);
                    if (value == txt)
                    {
                        ok_check= true;
                    }
                    else
                    {
                        ShotErrorPng("wrong value of id {0} should be \"{1}\" instead of \"{2}\"",element_id,txt,value);
                    }
                }
            }));
            return ok_check;
        }

        void TypeId(string element_id, string txt)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= browser.Document.GetElementById(element_id);
                if (null == element)
                {
                    Log("can not find element with id {0}",element_id);
                }
                else
                {
                    HtmlHelper.TypeElement(browser,element, txt);
                    Log("typed \"{0}\" to id {1}",txt,element_id);
                }
            }));
        }

        void SetValueIdFromFile(string element_id, string file_name)
        {
            string full_file_name = Path.IsPathRooted(file_name) ? file_name : Path.Combine(PathForResultFiles, file_name);
            string value= File.ReadAllText(full_file_name);
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= browser.Document.GetElementById(element_id);
                if (null == element)
                {
                    Log("can not find element with id {0}",element_id);
                }
                else
                {
                    HtmlHelper.SetValueElement(browser,element, value);
                    Log("set value from file \"{0}\" for id {1}",file_name,element_id);
                }
            }));
        }

        void SetValueId(string element_id, string value)
        {
            form.Invoke(new MethodInvoker(delegate
            {
                HtmlElement element= browser.Document.GetElementById(element_id);
                if (null == element)
                {
                    Log("can not find element with id {0}",element_id);
                }
                else
                {
                    HtmlHelper.SetValueElement(browser,element, value);
                    Log("set value \"{0}\" for id {1}",value,element_id);
                }
            }));
        }

        bool SafeClick(HtmlElement element)
        {
            if (null==element)
            {
                return false;
            }
            else
            {
                HtmlHelper.ClickElement(element);
                return true;
            }
        }

        bool SafeDoubleClick(HtmlElement element)
        {
            if (null==element)
            {
                return false;
            }
            else
            {
                HtmlHelper.DoubleClickElement(browser,element);
                return true;
            }
        }

        bool SafeType(HtmlElement element, string txt)
        {
            if (null==element)
            {
                return false;
            }
            else
            {
                HtmlHelper.TypeElement(browser,element,txt);
                return true;
            }
        }

        bool SafeSetAttrId(HtmlElement element, string name, string value)
        {
            if (null==element)
            {
                return false;
            }
            else
            {
                HtmlHelper.SetAttrId(browser,element,name,value);
                return true;
            }
        }

        bool WaitForElementWithTextAndClickIt(string txt)
        {
            return !WaitFor
            (
                () => {return SafeClick(HtmlHelper.FindLastElementWithText(browser,txt));},
                "text \"{0}\" is clicked",
                "text \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        bool WaitForFirstElementWithTextAndClickIt(string txt)
        {
            return !WaitFor
            (
                () => { return SafeClick(HtmlHelper.FindFirstElementWithText(browser, txt)); },
                "text \"{0}\" is clicked",
                "text \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithFullTextAndClickIt(string atxt)
        {
            string txt= atxt.Replace("_"," ");
            WaitFor
            (
                () => {return SafeClick(HtmlHelper.FindLastElementWithFullText(browser,txt));},
                "element with text \"{0}\" is clicked",
                "element with text \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithHiddenFullTextAndClickIt(string atxt)
        {
            string txt = atxt.Replace("_", " ");
            WaitFor
            (
                () => { return SafeClick(HtmlHelper.FindLastElementWithHiddenFullText(browser, txt)); },
                "element with text \"{0}\" is clicked",
                "element with text \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithValueAndClickIt(string atxt)
        {
            string txt= atxt.Replace("_"," ");
            WaitFor
            (
                () => { return SafeClick(HtmlHelper.FindElementWithValue(browser,txt));},
                "value \"{0}\" is clicked",
                "value \"{1}\" is NOT present for {0} millseconds",
                txt
            );
        }

        void WaitForElementWithClassAndClickItIfPresentClass(string classNameToClick, string classNameToFind)
        {
            form.Invoke(new MethodInvoker(delegate(){
                if (null != HtmlHelper.FindElementWithClass(browser,classNameToFind))
                    WaitForElementWithClassAndClickIt(classNameToClick);
            }));
        }

        void WaitForElementWithClassAndClickIt(string className)
        {
            WaitFor
            (
                () => { return SafeClick(HtmlHelper.FindElementWithClass(browser,className));},
                "class \"{0}\" is clicked",
                "class \"{1}\" is NOT present for {0} millseconds",
                className
            );
        }
    }

    namespace pipe
    {
        public interface IPlayer : WBT.IPlayer
        {
            bool PipeServerMode { get;  set; }

            void SafeStartListenPipe();
        }

        public class Player : WBT.Player, IPlayer
        {
            NamedPipeApiHelper pipe_api_helper;

            Action<string[], pipe.IPlayer> m_ProcessArguments;

            public bool PipeServerMode { set; get; }

            public Player(WebBrowser b, Form f, Action<string[], pipe.IPlayer> ProcessArguments)
                : base(b, f)
            {
                m_ProcessArguments = ProcessArguments;
            }

            public void SafeStartListenPipe()
            {
                if (null != pipe_api_helper)
                {
                    pipe_api_helper.ReStartListenPipe();
                }
                else
                {
                    pipe_api_helper = new NamedPipeApiHelper(this, m_ProcessArguments);
                    pipe_api_helper.StartListenPipe();
                }
            }

            protected override void SafeExit()
            {
                if (PipeServerMode)
                {
                    SafeStartListenPipe();
                }
                else
                {
                    base.SafeExit();
                }
            }

            public override void Log(string text)
            {
                base.Log(text);
                if (null != pipe_api_helper)
                    pipe_api_helper.WriteLine(text);
            }
        }
    }
}
