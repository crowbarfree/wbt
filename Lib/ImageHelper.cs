﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace WBT
{
    public class ImageHelper
    {
        public static void DiffPng(string png_path1, string png_path2, string diff_path)
        {
            long bytes, bytes_diff;
            using (Bitmap a = (Bitmap)Image.FromFile(png_path1))
            using (Bitmap b = (Bitmap)Image.FromFile(png_path2))
            using (Bitmap diff = PixelDiff(a, b, out bytes, out bytes_diff))
            {
                diff.Save(diff_path,ImageFormat.Png);
            }
            double dbytes= bytes;
            double dbytes_diff= bytes_diff;
            double ddiff= (dbytes_diff*100)/dbytes;
            Console.Out.WriteLine("{0} bytes are different from {1}",bytes_diff,bytes);
            Console.Out.WriteLine("{0}% bytes are different",ddiff);
        }

        public static unsafe Bitmap SafeCreateBitmap(Size sz)
        {
            try
            {
                return new Bitmap(sz.Width, sz.Height, PixelFormat.Format32bppArgb);
            }
            catch (Exception ex)
            {
                string msg= string.Format("can not create bitmap w:{0},h:{1}", sz.Width, sz.Height);
                throw new Exception(msg, ex);
            }
        }

        public static unsafe Bitmap PixelDiff(Bitmap a, Bitmap b, out long aData_bytes, out long bytes_diff)
        {
            Size output_size= new Size(Math.Min(a.Width,b.Width),Math.Min(a.Height,b.Height));
            Bitmap output = SafeCreateBitmap(output_size);
            Rectangle rect = new Rectangle(Point.Empty, output_size);
            using (Extensions.DisposableImageData aData = a.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
            using (Extensions.DisposableImageData bData = b.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
            using (Extensions.DisposableImageData outputData = output.LockBitsDisposable(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb))
            {
                byte* aPtr = (byte*)aData.Scan0;
                byte* bPtr = (byte*)bData.Scan0;
                byte* outputPtr = (byte*)outputData.Scan0;
                int aData_length = Math.Min(aData.Stride * aData.Height,bData.Stride * bData.Height);
                aData_bytes= aData_length*255;
                bytes_diff= 0;
                for (int i = 0; i < aData_length; i++)
                {
                    // For alpha use the average of both images (otherwise pixels with the same alpha won't be visible)
                    if ((i + 1) % 4 == 0)
                    {
                        *outputPtr = (byte)((*aPtr + *bPtr) / 2);
                    }
                    else
                    {
                        byte color = (byte)~(*aPtr ^ *bPtr);
                        if (color != 255)
                            color = 0;
                        *outputPtr = color;
                    }
                    if ((*aPtr) != (*bPtr))
                        bytes_diff += Math.Abs((*aPtr) - (*bPtr));

                    outputPtr++;
                    aPtr++;
                    bPtr++;
                }
            }
            return output;
        }
    }

    static class Extensions
    {
        public static DisposableImageData LockBitsDisposable(this Bitmap bitmap, Rectangle rect, ImageLockMode flags, PixelFormat format)
        {
            return new DisposableImageData(bitmap, rect, flags, format);
        }

        public class DisposableImageData : IDisposable
        {
            private readonly Bitmap _bitmap;
            private readonly BitmapData _data;

            internal DisposableImageData(Bitmap bitmap, Rectangle rect, ImageLockMode flags, PixelFormat format)
            {
                _bitmap = bitmap;
                _data = bitmap.LockBits(rect, flags, format);
            }

            public void Dispose()
            {
                _bitmap.UnlockBits(_data);
            }

            public IntPtr Scan0
            {
                get { return _data.Scan0; }
            }

            public int Stride
            {
                get { return _data.Stride; }
            }

            public int Width
            {
                get { return _data.Width; }
            }

            public int Height
            {
                get { return _data.Height; }
            }

            public PixelFormat PixelFormat
            {
                get { return _data.PixelFormat; }
            }

            public int Reserved
            {
                get { return _data.Reserved; }
            }
        }
    }
}
