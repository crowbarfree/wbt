﻿using System;
using System.Drawing;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    public interface IGrizzly
    {
        void Bind_and_Show(IWin32Window owner, Control control, IPlayer player);
        void SafeShow();
        void SafeHide();
    }

    public partial class Grizzly : Form, IGrizzly
    {
        static ILog log = LogManager.GetLogger(typeof(Grizzly));

        public Grizzly()
        {
            InitializeComponent();
        }

        Control m_ProtectedControl= null;

        public void SafeClose()
        {
            if (InvokeRequired)
                Invoke(new MethodInvoker(delegate { Close(); }));
        }

        public void SafeShow()
        {
            if (!Visible)
                Show(m_ProtectedControl);
        }

        public void SafeHide()
        {
            try
            {
                m_ProtectedControl.Invoke(new MethodInvoker(delegate
                {
                    if (Visible)
                        Hide();
                }));
            }
            catch (Exception ex)
            {
                log.Error("can not SafeUnprotectWebBrowser", ex);
            }
        }

        public void Bind_and_Show(IWin32Window owner, Control control, IPlayer player)
        {
            Bind(control);
            Bind(player);
            Show(owner);
        }

        public void Bind(Control uc)
        {
            m_ProtectedControl= uc;
            m_ProtectedControl.SizeChanged += new EventHandler(m_UserControl_SizeChanged);
            Control rootParent= RootParent();
            rootParent.SizeChanged += new EventHandler(Parent_SizeChanged);
            rootParent.Move += new EventHandler(Parent_Move);

            Form rootForm = rootParent as Form;
            if (null!= rootForm)
            {
                Grizzly grizzly= this;
                rootForm.Shown += (obj, eventArgs) => { grizzly.UpdateLocation(); };
                rootForm.FormClosing += (obj, eventArgs) => { grizzly.SafeClose(); };
            }

            UpdateLocation();
        }

        public void Bind(IPlayer player)
        {
            player.protect_browser = () => { SafeShow(); };
            player.unprotect_browser = () => { SafeHide(); };
            player.Stop += delegate { SafeHide(); };
        }

        Control RootParent()
        {
            Control res= m_ProtectedControl;
            while (null != res.Parent)
                res= res.Parent;
            return res;
        }

        void Parent_Move(object sender, EventArgs e)
        {
            UpdateLocation();
        }

        void Parent_SizeChanged(object sender, EventArgs e)
        {
            UpdateLocation();
        }

        void m_UserControl_SizeChanged(object sender, EventArgs e)
        {
            UpdateLocation();
        }

        public void UpdateLocation()
        {
            Size= m_ProtectedControl.ClientSize;
            if (null != m_ProtectedControl.Parent)
                Location= m_ProtectedControl.Parent.PointToScreen(m_ProtectedControl.Location);
        }

        private void Grizzly_Paint(object sender, PaintEventArgs e)
        {
            System.Drawing.Size size= Size;
            int grid_size= 100;
            using (Graphics g = CreateGraphics())
            {
                using (Pen grid_pen = new Pen(Color.Green, 1))
                {
                    int start_x = -size.Width;
                    int start_y = 2 * size.Width;
                    for (int x = start_x; x < start_y; x += grid_size)
                    {
                        g.DrawLine(grid_pen, x, 0, x + size.Height, size.Height);
                        g.DrawLine(grid_pen, x, 0, x - size.Height, size.Height);
                    }
                }
                using (Pen frame_pen = new Pen(Color.Green, 6))
                {
                    g.DrawLine(frame_pen, 0, 0, size.Width, 0);
                    g.DrawLine(frame_pen, 0, size.Height, size.Width, size.Height);
                    g.DrawLine(frame_pen, 0, 0, size.Width, 0);
                    g.DrawLine(frame_pen, 0, 0, 0, size.Height);
                    g.DrawLine(frame_pen, size.Width, 0, size.Width, size.Height);
                }
            }
        }

        private void Grizzly_Shown(object sender, EventArgs e)
        {
            UpdateLocation();
        }
    }
}
