﻿
namespace WBT
{
    partial class DevPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPagePlayer = new System.Windows.Forms.TabPage();
            this.btnStep = new System.Windows.Forms.Button();
            this.linkLog = new System.Windows.Forms.LinkLabel();
            this.linkCheckings = new System.Windows.Forms.LinkLabel();
            this.btnPauseResume = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.tabPageParameters = new System.Windows.Forms.TabPage();
            this.labelPreCall = new System.Windows.Forms.Label();
            this.textBoxPreCall = new System.Windows.Forms.TextBox();
            this.labelStartURL = new System.Windows.Forms.Label();
            this.textBoxStartUrl = new System.Windows.Forms.TextBox();
            this.labelScenario = new System.Windows.Forms.Label();
            this.textBoxScenario = new System.Windows.Forms.TextBox();
            this.textBoxCurrentUrl = new System.Windows.Forms.TextBox();
            this.labelCurrentUrl = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabPagePlayer.SuspendLayout();
            this.tabPageParameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPagePlayer);
            this.tabControl.Controls.Add(this.tabPageParameters);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(560, 426);
            this.tabControl.TabIndex = 0;
            // 
            // tabPagePlayer
            // 
            this.tabPagePlayer.Controls.Add(this.btnStep);
            this.tabPagePlayer.Controls.Add(this.linkLog);
            this.tabPagePlayer.Controls.Add(this.linkCheckings);
            this.tabPagePlayer.Controls.Add(this.btnPauseResume);
            this.tabPagePlayer.Controls.Add(this.btnRestart);
            this.tabPagePlayer.Controls.Add(this.txtLog);
            this.tabPagePlayer.Controls.Add(this.txtCommand);
            this.tabPagePlayer.Controls.Add(this.btnExecute);
            this.tabPagePlayer.Location = new System.Drawing.Point(4, 22);
            this.tabPagePlayer.Name = "tabPagePlayer";
            this.tabPagePlayer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePlayer.Size = new System.Drawing.Size(552, 400);
            this.tabPagePlayer.TabIndex = 0;
            this.tabPagePlayer.Text = "Player";
            this.tabPagePlayer.UseVisualStyleBackColor = true;
            // 
            // btnStep
            // 
            this.btnStep.Location = new System.Drawing.Point(188, 6);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(85, 23);
            this.btnStep.TabIndex = 45;
            this.btnStep.Text = "Step";
            this.btnStep.UseVisualStyleBackColor = true;
            // 
            // linkLog
            // 
            this.linkLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLog.AutoSize = true;
            this.linkLog.Location = new System.Drawing.Point(518, 40);
            this.linkLog.Name = "linkLog";
            this.linkLog.Size = new System.Drawing.Size(28, 13);
            this.linkLog.TabIndex = 44;
            this.linkLog.TabStop = true;
            this.linkLog.Text = "Log:";
            this.linkLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLog_LinkClicked);
            // 
            // linkCheckings
            // 
            this.linkCheckings.AutoSize = true;
            this.linkCheckings.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkCheckings.Location = new System.Drawing.Point(6, 40);
            this.linkCheckings.Name = "linkCheckings";
            this.linkCheckings.Size = new System.Drawing.Size(71, 13);
            this.linkCheckings.TabIndex = 43;
            this.linkCheckings.TabStop = true;
            this.linkCheckings.Text = "no checkings";
            this.linkCheckings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCheckings_LinkClicked);
            // 
            // btnPauseResume
            // 
            this.btnPauseResume.Location = new System.Drawing.Point(97, 6);
            this.btnPauseResume.Name = "btnPauseResume";
            this.btnPauseResume.Size = new System.Drawing.Size(85, 23);
            this.btnPauseResume.TabIndex = 42;
            this.btnPauseResume.Text = "Pause";
            this.btnPauseResume.UseVisualStyleBackColor = true;
            this.btnPauseResume.Click += new System.EventHandler(this.btnPauseResume_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(6, 6);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(85, 23);
            this.btnRestart.TabIndex = 41;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtLog.Location = new System.Drawing.Point(9, 64);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(537, 294);
            this.txtLog.TabIndex = 40;
            // 
            // txtCommand
            // 
            this.txtCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommand.Enabled = false;
            this.txtCommand.Location = new System.Drawing.Point(9, 371);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(456, 20);
            this.txtCommand.TabIndex = 39;
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Enabled = false;
            this.btnExecute.Location = new System.Drawing.Point(471, 368);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(75, 23);
            this.btnExecute.TabIndex = 38;
            this.btnExecute.Text = "exec";
            this.btnExecute.UseVisualStyleBackColor = true;
            // 
            // tabPageParameters
            // 
            this.tabPageParameters.Controls.Add(this.textBoxCurrentUrl);
            this.tabPageParameters.Controls.Add(this.labelCurrentUrl);
            this.tabPageParameters.Controls.Add(this.textBoxScenario);
            this.tabPageParameters.Controls.Add(this.labelScenario);
            this.tabPageParameters.Controls.Add(this.textBoxStartUrl);
            this.tabPageParameters.Controls.Add(this.labelStartURL);
            this.tabPageParameters.Controls.Add(this.textBoxPreCall);
            this.tabPageParameters.Controls.Add(this.labelPreCall);
            this.tabPageParameters.Location = new System.Drawing.Point(4, 22);
            this.tabPageParameters.Name = "tabPageParameters";
            this.tabPageParameters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageParameters.Size = new System.Drawing.Size(552, 400);
            this.tabPageParameters.TabIndex = 1;
            this.tabPageParameters.Text = "Parameters";
            this.tabPageParameters.UseVisualStyleBackColor = true;
            // 
            // labelPreCall
            // 
            this.labelPreCall.AutoSize = true;
            this.labelPreCall.Location = new System.Drawing.Point(13, 9);
            this.labelPreCall.Name = "labelPreCall";
            this.labelPreCall.Size = new System.Drawing.Size(80, 13);
            this.labelPreCall.TabIndex = 0;
            this.labelPreCall.Text = "Call before start";
            // 
            // textBoxPreCall
            // 
            this.textBoxPreCall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPreCall.Location = new System.Drawing.Point(28, 25);
            this.textBoxPreCall.Multiline = true;
            this.textBoxPreCall.Name = "textBoxPreCall";
            this.textBoxPreCall.ReadOnly = true;
            this.textBoxPreCall.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxPreCall.Size = new System.Drawing.Size(486, 48);
            this.textBoxPreCall.TabIndex = 1;
            this.textBoxPreCall.Text = "path to call before start is absent";
            // 
            // labelStartURL
            // 
            this.labelStartURL.AutoSize = true;
            this.labelStartURL.Location = new System.Drawing.Point(13, 80);
            this.labelStartURL.Name = "labelStartURL";
            this.labelStartURL.Size = new System.Drawing.Size(57, 13);
            this.labelStartURL.TabIndex = 2;
            this.labelStartURL.Text = "Start URL:";
            // 
            // textBoxStartUrl
            // 
            this.textBoxStartUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxStartUrl.Location = new System.Drawing.Point(28, 96);
            this.textBoxStartUrl.Multiline = true;
            this.textBoxStartUrl.Name = "textBoxStartUrl";
            this.textBoxStartUrl.ReadOnly = true;
            this.textBoxStartUrl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxStartUrl.Size = new System.Drawing.Size(486, 48);
            this.textBoxStartUrl.TabIndex = 3;
            this.textBoxStartUrl.Text = "start URL is absent";
            // 
            // labelScenario
            // 
            this.labelScenario.AutoSize = true;
            this.labelScenario.Location = new System.Drawing.Point(13, 153);
            this.labelScenario.Name = "labelScenario";
            this.labelScenario.Size = new System.Drawing.Size(52, 13);
            this.labelScenario.TabIndex = 4;
            this.labelScenario.Text = "Scenario:";
            // 
            // textBoxScenario
            // 
            this.textBoxScenario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxScenario.Location = new System.Drawing.Point(28, 169);
            this.textBoxScenario.Multiline = true;
            this.textBoxScenario.Name = "textBoxScenario";
            this.textBoxScenario.ReadOnly = true;
            this.textBoxScenario.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxScenario.Size = new System.Drawing.Size(486, 48);
            this.textBoxScenario.TabIndex = 5;
            this.textBoxScenario.Text = "scenario is absent";
            // 
            // textBoxCurrentUrl
            // 
            this.textBoxCurrentUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCurrentUrl.Location = new System.Drawing.Point(28, 249);
            this.textBoxCurrentUrl.Multiline = true;
            this.textBoxCurrentUrl.Name = "textBoxCurrentUrl";
            this.textBoxCurrentUrl.ReadOnly = true;
            this.textBoxCurrentUrl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxCurrentUrl.Size = new System.Drawing.Size(486, 48);
            this.textBoxCurrentUrl.TabIndex = 7;
            this.textBoxCurrentUrl.Text = "current URL is absent";
            // 
            // labelCurrentUrl
            // 
            this.labelCurrentUrl.AutoSize = true;
            this.labelCurrentUrl.Location = new System.Drawing.Point(13, 233);
            this.labelCurrentUrl.Name = "labelCurrentUrl";
            this.labelCurrentUrl.Size = new System.Drawing.Size(69, 13);
            this.labelCurrentUrl.TabIndex = 6;
            this.labelCurrentUrl.Text = "Current URL:";
            // 
            // DevPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 450);
            this.Controls.Add(this.tabControl);
            this.Name = "DevPanelForm";
            this.Text = "WBT/WBI developer form";
            this.tabControl.ResumeLayout(false);
            this.tabPagePlayer.ResumeLayout(false);
            this.tabPagePlayer.PerformLayout();
            this.tabPageParameters.ResumeLayout(false);
            this.tabPageParameters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPagePlayer;
        private System.Windows.Forms.TabPage tabPageParameters;
        private System.Windows.Forms.Button btnStep;
        private System.Windows.Forms.LinkLabel linkLog;
        private System.Windows.Forms.LinkLabel linkCheckings;
        private System.Windows.Forms.Button btnPauseResume;
        private System.Windows.Forms.Button btnRestart;
        internal System.Windows.Forms.TextBox txtLog;
        internal System.Windows.Forms.TextBox txtCommand;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.TextBox textBoxPreCall;
        private System.Windows.Forms.Label labelPreCall;
        private System.Windows.Forms.TextBox textBoxStartUrl;
        private System.Windows.Forms.Label labelStartURL;
        private System.Windows.Forms.TextBox textBoxScenario;
        private System.Windows.Forms.Label labelScenario;
        private System.Windows.Forms.TextBox textBoxCurrentUrl;
        private System.Windows.Forms.Label labelCurrentUrl;
    }
}