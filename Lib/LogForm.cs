﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WBT
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
        }

        public void DataLoad(string txt)
        {
            txtLog.Text= txt;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            FindNext();
        }

        private void txtFind_TextChanged(object sender, EventArgs e)
        {
            Find();
        }

        void Find()
        {
            Find(0);
        }

        void FindNext()
        {
            Find(NextPositionToFind());
        }

        int NextPositionToFind()
        {
            int iss= txtLog.SelectionStart+1;
            int len= txtLog.TextLength;
            return iss+1<len ? iss : 0;
        }

        void Find(int ifindfrom)
        {
            string txt= txtFind.Text;
            int iStart= txtLog.Text.IndexOf(txt,ifindfrom);
            if (-1==iStart)
            {
                txtLog.Select(txtLog.SelectionStart,0);
            }
            else
            {
                txtLog.Select(iStart,txt.Length);
                txtLog.ScrollToCaret();
            }
            txtFind.Focus();
        }

        private void txtFindFromBegin_Click(object sender, EventArgs e)
        {
            Find();
        }

        private void LogForm_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessShortCut(e);
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessShortCut(e);
        }

        void ProcessShortCut(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
                FindNext();
        }

        void GotoEnd()
        {
            txtLog.SelectionStart = txtLog.TextLength;
            txtLog.ScrollToCaret();
        }

        private void LogForm_Load(object sender, EventArgs e)
        {
            GotoEnd();
        }
    }
}
