﻿using System;
using System.Text;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    public partial class DevPanelForm : Form
    {
        static ILog log = LogManager.GetLogger(typeof(DevPanelForm));

        public IPlayer player;
        IGrizzly grizzly;

        public DevPanelForm()
        {
            InitializeComponent();
        }

        public Action<string, bool> show_log_line_check_if_empty()
        {
            DevPanelForm devpanel = this;

            return (string text, bool ifnotempty) =>
            {
                try
                {
                    devpanel.Invoke(new MethodInvoker(delegate
                    {
                        if (!ifnotempty || !string.IsNullOrEmpty(devpanel.txtLog.Text))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(DateTime.Now.ToLongTimeString()).Append(" ").Append(text).AppendLine();
                            devpanel.txtLog.AppendText(sb.ToString());
                        }
                    }));
                }
                catch (System.InvalidOperationException ex)
                {
                    log.Error("can not write text to log window", ex);
                }
            };
        }

        public Action<string> show_the_line()
        {
            DevPanelForm devpanel = this;

            return (string txt) =>
            {
                devpanel.Invoke(new MethodInvoker(delegate {
                    devpanel.txtCommand.Text = txt;
                }));
            };
        }

        private void linkCheckings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (0 == player.checkings_Count)
            {
                MessageBox.Show("no checkings to view!");
            }
            else
            {
                CheckingsForm frm = new CheckingsForm();
                ICheckable checkable = player as ICheckable;
                if (null == checkable)
                {
                    MessageBox.Show("undefined checkings to view!");
                }
                else
                {
                    frm.DataLoad(checkable.Checkings);
                    frm.ShowDialog(this);
                }
            }
        }

        private void linkLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LogForm frm = new LogForm();
            frm.DataLoad(txtLog.Text);
            frm.ShowDialog(this);
        }

        bool m_frm_Closing = false;

        public void Relocate_for(Form frm)
        {
            StartPosition = FormStartPosition.Manual;
            Location = new System.Drawing.Point(frm.Location.X + frm.Size.Width, frm.Location.Y);

            Screen screen = Screen.FromControl(frm);
            int width = Size.Width;
            System.Drawing.Rectangle screen_bounds = screen.Bounds;

            if (Location.X + width > screen_bounds.Right)
                width = screen.Bounds.Right - Location.X;
            Size = new System.Drawing.Size(width, frm.Size.Height);

            frm.FormClosing += (obj, eventArgs) => { m_frm_Closing = true; };
            FormClosed+= (obj, eventArgs) => { if (!m_frm_Closing) frm.Close(); };
        }

        public void Bind(WBT.IPlayer player, IGrizzly grizzly, WebBrowser browser)
        {
            this.player = player;
            this.grizzly = grizzly;

            if (!string.IsNullOrEmpty(player.PreCall))
                textBoxPreCall.Text = player.PreCall;
            if (!string.IsNullOrEmpty(player.PreUrl))
                textBoxStartUrl.Text = player.PreUrl;
            if (!string.IsNullOrEmpty(player.ScenarioPath))
                textBoxScenario.Text = player.ScenarioPath;

            player.show_the_line = show_the_line();
            player.show_log_line_check_if_empty = show_log_line_check_if_empty();

            player.CheckingsChanged += Player_CheckingsChanged;
            player.ThreadChanged += Player_ThreadChanged;

            browser.Navigated += (obj, eventArgs) => { textBoxCurrentUrl.Text= browser.Url.ToString(); };
        }

        private void Player_ThreadChanged(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (!player.thread_is_active)
                {
                    labelScenario.Text = "Scenario (inactive):";
                }
                else if (player.signal_to_stread_to_stop)
                {
                    labelScenario.Text = "Scenario (stopping):";
                }
                else if (player.thread_is_paused)
                {
                    labelScenario.Text = "Scenario (paused):";
                }
                else
                {
                    labelScenario.Text = "Scenario (running):";
                }
                btnPauseResume.Text = player.thread_is_paused ? "Resume" : "Pause";
                btnStep.Enabled = player.thread_is_paused;
                btnExecute.Enabled =
                txtCommand.Enabled = player.thread_is_paused;
            }));
        }

        System.Drawing.Color? DefaultLinkColor = null;

        private void Player_CheckingsChanged(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                linkCheckings.Text =
                    0 == player.checkings_Count ? "no checkings" :
                    0 == player.checkings_CountFailed ? string.Format("{0} checkings", player.checkings_Count) :
                    string.Format("{0} checkings, {1} failed", player.checkings_Count, player.checkings_CountFailed);
                if (null == DefaultLinkColor)
                    DefaultLinkColor = linkCheckings.LinkColor;
                linkCheckings.LinkColor = (0 == player.checkings_CountFailed) ? DefaultLinkColor.Value : System.Drawing.Color.Red;
            }));
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            player.thread_is_paused = false;
            grizzly.SafeShow();
            if (null != player.OnRestart)
                player.OnRestart.Invoke();
            player.StartScenario();
        }

        private void btnPauseResume_Click(object sender, EventArgs e)
        {
            player.thread_is_paused = !player.thread_is_paused;
            if (player.thread_is_paused)
            {
                grizzly.SafeHide();
            }
            else
            {
                grizzly.SafeShow();
            }
        }
    }
}
