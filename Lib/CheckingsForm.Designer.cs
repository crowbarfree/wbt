﻿namespace WBT
{
    partial class CheckingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "a",
            "a.png",
            "всё отлично"}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "b",
            "b.png",
            "failed"}, -1);
            this.lvCheckings = new System.Windows.Forms.ListView();
            this.columnHeaderRes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderFailDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClose = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.panelTools = new System.Windows.Forms.Panel();
            this.lSelectAll = new System.Windows.Forms.Label();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.lFix = new System.Windows.Forms.Label();
            this.lDifference = new System.Windows.Forms.Label();
            this.lResult = new System.Windows.Forms.Label();
            this.lPreCheckedResult = new System.Windows.Forms.Label();
            this.lEtalon = new System.Windows.Forms.Label();
            this.btnCopyCheckings = new System.Windows.Forms.Button();
            this.btnClearResults = new System.Windows.Forms.Button();
            this.btnFixResults = new System.Windows.Forms.Button();
            this.lShow = new System.Windows.Forms.Label();
            this.rbShowDifference = new System.Windows.Forms.RadioButton();
            this.rbShowOriginal = new System.Windows.Forms.RadioButton();
            this.rbShowResult = new System.Windows.Forms.RadioButton();
            this.rbShowEtalon = new System.Windows.Forms.RadioButton();
            this.timer_Watch = new System.Windows.Forms.Timer(this.components);
            this.panelImageProperties = new System.Windows.Forms.Panel();
            this.radioButtonSizeModeFit = new System.Windows.Forms.RadioButton();
            this.radioButtonSizeModeRealSize = new System.Windows.Forms.RadioButton();
            this.labelSizeMode = new System.Windows.Forms.Label();
            this.textBoxImageHeight = new System.Windows.Forms.TextBox();
            this.labelImageHeight = new System.Windows.Forms.Label();
            this.textBoxImageWidth = new System.Windows.Forms.TextBox();
            this.labelImageWidth = new System.Windows.Forms.Label();
            this.panelPicture = new System.Windows.Forms.Panel();
            this.labelFile = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.buttonOpenFolder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.panelTools.SuspendLayout();
            this.panelImageProperties.SuspendLayout();
            this.panelPicture.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvCheckings
            // 
            this.lvCheckings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvCheckings.AutoArrange = false;
            this.lvCheckings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderRes,
            this.columnHeaderName,
            this.columnHeaderFailDescription});
            this.lvCheckings.FullRowSelect = true;
            this.lvCheckings.GridLines = true;
            this.lvCheckings.HideSelection = false;
            this.lvCheckings.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6});
            this.lvCheckings.Location = new System.Drawing.Point(556, 93);
            this.lvCheckings.Name = "lvCheckings";
            this.lvCheckings.ShowItemToolTips = true;
            this.lvCheckings.Size = new System.Drawing.Size(605, 540);
            this.lvCheckings.TabIndex = 0;
            this.lvCheckings.UseCompatibleStateImageBehavior = false;
            this.lvCheckings.View = System.Windows.Forms.View.Details;
            this.lvCheckings.SelectedIndexChanged += new System.EventHandler(this.lvCheckings_SelectedIndexChanged);
            // 
            // columnHeaderRes
            // 
            this.columnHeaderRes.Text = "res";
            this.columnHeaderRes.Width = 30;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Shot";
            this.columnHeaderName.Width = 250;
            // 
            // columnHeaderFailDescription
            // 
            this.columnHeaderFailDescription.Text = "Comments";
            this.columnHeaderFailDescription.Width = 320;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(1086, 641);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(530, 613);
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            // 
            // panelTools
            // 
            this.panelTools.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTools.Controls.Add(this.lSelectAll);
            this.panelTools.Controls.Add(this.btnSelectAll);
            this.panelTools.Controls.Add(this.lFix);
            this.panelTools.Controls.Add(this.lDifference);
            this.panelTools.Controls.Add(this.lResult);
            this.panelTools.Controls.Add(this.lPreCheckedResult);
            this.panelTools.Controls.Add(this.lEtalon);
            this.panelTools.Controls.Add(this.btnCopyCheckings);
            this.panelTools.Controls.Add(this.btnClearResults);
            this.panelTools.Controls.Add(this.btnFixResults);
            this.panelTools.Controls.Add(this.lShow);
            this.panelTools.Controls.Add(this.rbShowDifference);
            this.panelTools.Controls.Add(this.rbShowOriginal);
            this.panelTools.Controls.Add(this.rbShowResult);
            this.panelTools.Controls.Add(this.rbShowEtalon);
            this.panelTools.Location = new System.Drawing.Point(556, 12);
            this.panelTools.Name = "panelTools";
            this.panelTools.Size = new System.Drawing.Size(605, 75);
            this.panelTools.TabIndex = 10;
            // 
            // lSelectAll
            // 
            this.lSelectAll.AutoSize = true;
            this.lSelectAll.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lSelectAll.Location = new System.Drawing.Point(389, 33);
            this.lSelectAll.Name = "lSelectAll";
            this.lSelectAll.Size = new System.Drawing.Size(35, 13);
            this.lSelectAll.TabIndex = 22;
            this.lSelectAll.Text = "Ctrl+A";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(368, 49);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 21;
            this.btnSelectAll.Text = "Select all";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // lFix
            // 
            this.lFix.AutoSize = true;
            this.lFix.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lFix.Location = new System.Drawing.Point(471, 33);
            this.lFix.Name = "lFix";
            this.lFix.Size = new System.Drawing.Size(34, 13);
            this.lFix.TabIndex = 20;
            this.lFix.Text = "Ctrl+F";
            // 
            // lDifference
            // 
            this.lDifference.AutoSize = true;
            this.lDifference.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lDifference.Location = new System.Drawing.Point(321, 35);
            this.lDifference.Name = "lDifference";
            this.lDifference.Size = new System.Drawing.Size(36, 13);
            this.lDifference.TabIndex = 19;
            this.lDifference.Text = "Ctrl+D";
            // 
            // lResult
            // 
            this.lResult.AutoSize = true;
            this.lResult.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lResult.Location = new System.Drawing.Point(236, 35);
            this.lResult.Name = "lResult";
            this.lResult.Size = new System.Drawing.Size(36, 13);
            this.lResult.TabIndex = 18;
            this.lResult.Text = "Ctrl+R";
            // 
            // lPreCheckedResult
            // 
            this.lPreCheckedResult.AutoSize = true;
            this.lPreCheckedResult.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lPreCheckedResult.Location = new System.Drawing.Point(107, 35);
            this.lPreCheckedResult.Name = "lPreCheckedResult";
            this.lPreCheckedResult.Size = new System.Drawing.Size(35, 13);
            this.lPreCheckedResult.TabIndex = 17;
            this.lPreCheckedResult.Text = "Ctrl+S";
            // 
            // lEtalon
            // 
            this.lEtalon.AutoSize = true;
            this.lEtalon.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lEtalon.Location = new System.Drawing.Point(46, 35);
            this.lEtalon.Name = "lEtalon";
            this.lEtalon.Size = new System.Drawing.Size(35, 13);
            this.lEtalon.TabIndex = 16;
            this.lEtalon.Text = "Ctrl+E";
            // 
            // btnCopyCheckings
            // 
            this.btnCopyCheckings.Location = new System.Drawing.Point(287, 49);
            this.btnCopyCheckings.Name = "btnCopyCheckings";
            this.btnCopyCheckings.Size = new System.Drawing.Size(75, 23);
            this.btnCopyCheckings.TabIndex = 15;
            this.btnCopyCheckings.Text = "Copy";
            this.btnCopyCheckings.UseVisualStyleBackColor = true;
            this.btnCopyCheckings.Click += new System.EventHandler(this.btnCopyCheckings_Click);
            // 
            // btnClearResults
            // 
            this.btnClearResults.Location = new System.Drawing.Point(530, 50);
            this.btnClearResults.Name = "btnClearResults";
            this.btnClearResults.Size = new System.Drawing.Size(75, 23);
            this.btnClearResults.TabIndex = 1;
            this.btnClearResults.Text = "ClearResults";
            this.btnClearResults.UseVisualStyleBackColor = true;
            this.btnClearResults.Click += new System.EventHandler(this.btnClearResults_Click);
            // 
            // btnFixResults
            // 
            this.btnFixResults.Location = new System.Drawing.Point(449, 49);
            this.btnFixResults.Name = "btnFixResults";
            this.btnFixResults.Size = new System.Drawing.Size(75, 23);
            this.btnFixResults.TabIndex = 0;
            this.btnFixResults.Text = "Fix results";
            this.btnFixResults.UseVisualStyleBackColor = true;
            this.btnFixResults.Click += new System.EventHandler(this.btnFixResults_Click);
            // 
            // lShow
            // 
            this.lShow.AutoSize = true;
            this.lShow.Location = new System.Drawing.Point(0, 0);
            this.lShow.Name = "lShow";
            this.lShow.Size = new System.Drawing.Size(113, 13);
            this.lShow.TabIndex = 14;
            this.lShow.Text = "Show checked image:";
            // 
            // rbShowDifference
            // 
            this.rbShowDifference.AutoSize = true;
            this.rbShowDifference.Location = new System.Drawing.Point(300, 15);
            this.rbShowDifference.Name = "rbShowDifference";
            this.rbShowDifference.Size = new System.Drawing.Size(72, 17);
            this.rbShowDifference.TabIndex = 13;
            this.rbShowDifference.Text = "difference";
            this.rbShowDifference.UseVisualStyleBackColor = true;
            this.rbShowDifference.CheckedChanged += new System.EventHandler(this.rbShowDifference_CheckedChanged);
            // 
            // rbShowOriginal
            // 
            this.rbShowOriginal.AutoSize = true;
            this.rbShowOriginal.Location = new System.Drawing.Point(215, 15);
            this.rbShowOriginal.Name = "rbShowOriginal";
            this.rbShowOriginal.Size = new System.Drawing.Size(79, 17);
            this.rbShowOriginal.TabIndex = 12;
            this.rbShowOriginal.Text = "real resultat";
            this.rbShowOriginal.UseVisualStyleBackColor = true;
            this.rbShowOriginal.CheckedChanged += new System.EventHandler(this.rbShowOriginal_CheckedChanged);
            // 
            // rbShowResult
            // 
            this.rbShowResult.AutoSize = true;
            this.rbShowResult.Checked = true;
            this.rbShowResult.Location = new System.Drawing.Point(87, 15);
            this.rbShowResult.Name = "rbShowResult";
            this.rbShowResult.Size = new System.Drawing.Size(122, 17);
            this.rbShowResult.TabIndex = 11;
            this.rbShowResult.TabStop = true;
            this.rbShowResult.Text = "pre checked resultat";
            this.rbShowResult.UseVisualStyleBackColor = true;
            this.rbShowResult.CheckedChanged += new System.EventHandler(this.rbShowResult_CheckedChanged);
            // 
            // rbShowEtalon
            // 
            this.rbShowEtalon.AutoSize = true;
            this.rbShowEtalon.Location = new System.Drawing.Point(27, 15);
            this.rbShowEtalon.Name = "rbShowEtalon";
            this.rbShowEtalon.Size = new System.Drawing.Size(54, 17);
            this.rbShowEtalon.TabIndex = 10;
            this.rbShowEtalon.Text = "etalon";
            this.rbShowEtalon.UseVisualStyleBackColor = true;
            this.rbShowEtalon.CheckedChanged += new System.EventHandler(this.rbShowEtalon_CheckedChanged);
            // 
            // timer_Watch
            // 
            this.timer_Watch.Tick += new System.EventHandler(this.timer_Watch_Tick);
            // 
            // panelImageProperties
            // 
            this.panelImageProperties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelImageProperties.Controls.Add(this.buttonOpenFolder);
            this.panelImageProperties.Controls.Add(this.textBoxFileName);
            this.panelImageProperties.Controls.Add(this.labelFile);
            this.panelImageProperties.Controls.Add(this.radioButtonSizeModeFit);
            this.panelImageProperties.Controls.Add(this.radioButtonSizeModeRealSize);
            this.panelImageProperties.Controls.Add(this.labelSizeMode);
            this.panelImageProperties.Controls.Add(this.textBoxImageHeight);
            this.panelImageProperties.Controls.Add(this.labelImageHeight);
            this.panelImageProperties.Controls.Add(this.textBoxImageWidth);
            this.panelImageProperties.Controls.Add(this.labelImageWidth);
            this.panelImageProperties.Location = new System.Drawing.Point(12, 636);
            this.panelImageProperties.Name = "panelImageProperties";
            this.panelImageProperties.Size = new System.Drawing.Size(1068, 37);
            this.panelImageProperties.TabIndex = 11;
            // 
            // radioButtonSizeModeFit
            // 
            this.radioButtonSizeModeFit.AutoSize = true;
            this.radioButtonSizeModeFit.Location = new System.Drawing.Point(353, 7);
            this.radioButtonSizeModeFit.Name = "radioButtonSizeModeFit";
            this.radioButtonSizeModeFit.Size = new System.Drawing.Size(115, 17);
            this.radioButtonSizeModeFit.TabIndex = 7;
            this.radioButtonSizeModeFit.Text = "fit to panel (Ctrl+W)";
            this.radioButtonSizeModeFit.UseVisualStyleBackColor = true;
            this.radioButtonSizeModeFit.CheckedChanged += new System.EventHandler(this.radioButtonSizeModeFit_CheckedChanged);
            // 
            // radioButtonSizeModeRealSize
            // 
            this.radioButtonSizeModeRealSize.AutoSize = true;
            this.radioButtonSizeModeRealSize.Checked = true;
            this.radioButtonSizeModeRealSize.Location = new System.Drawing.Point(246, 7);
            this.radioButtonSizeModeRealSize.Name = "radioButtonSizeModeRealSize";
            this.radioButtonSizeModeRealSize.Size = new System.Drawing.Size(101, 17);
            this.radioButtonSizeModeRealSize.TabIndex = 5;
            this.radioButtonSizeModeRealSize.TabStop = true;
            this.radioButtonSizeModeRealSize.Text = "real size (Ctrl+Q)";
            this.radioButtonSizeModeRealSize.UseVisualStyleBackColor = true;
            this.radioButtonSizeModeRealSize.CheckedChanged += new System.EventHandler(this.radioButtonSizeModeRealSize_CheckedChanged);
            // 
            // labelSizeMode
            // 
            this.labelSizeMode.AutoSize = true;
            this.labelSizeMode.Location = new System.Drawing.Point(181, 9);
            this.labelSizeMode.Name = "labelSizeMode";
            this.labelSizeMode.Size = new System.Drawing.Size(59, 13);
            this.labelSizeMode.TabIndex = 4;
            this.labelSizeMode.Text = "Size mode:";
            // 
            // textBoxImageHeight
            // 
            this.textBoxImageHeight.Location = new System.Drawing.Point(134, 6);
            this.textBoxImageHeight.Name = "textBoxImageHeight";
            this.textBoxImageHeight.ReadOnly = true;
            this.textBoxImageHeight.Size = new System.Drawing.Size(41, 20);
            this.textBoxImageHeight.TabIndex = 3;
            this.textBoxImageHeight.Text = "10000";
            // 
            // labelImageHeight
            // 
            this.labelImageHeight.AutoSize = true;
            this.labelImageHeight.Location = new System.Drawing.Point(89, 9);
            this.labelImageHeight.Name = "labelImageHeight";
            this.labelImageHeight.Size = new System.Drawing.Size(39, 13);
            this.labelImageHeight.TabIndex = 2;
            this.labelImageHeight.Text = "height:";
            // 
            // textBoxImageWidth
            // 
            this.textBoxImageWidth.Location = new System.Drawing.Point(42, 6);
            this.textBoxImageWidth.Name = "textBoxImageWidth";
            this.textBoxImageWidth.ReadOnly = true;
            this.textBoxImageWidth.Size = new System.Drawing.Size(41, 20);
            this.textBoxImageWidth.TabIndex = 1;
            this.textBoxImageWidth.Text = "10000";
            // 
            // labelImageWidth
            // 
            this.labelImageWidth.AutoSize = true;
            this.labelImageWidth.Location = new System.Drawing.Point(3, 8);
            this.labelImageWidth.Name = "labelImageWidth";
            this.labelImageWidth.Size = new System.Drawing.Size(35, 13);
            this.labelImageWidth.TabIndex = 0;
            this.labelImageWidth.Text = "width:";
            // 
            // panelPicture
            // 
            this.panelPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPicture.AutoScroll = true;
            this.panelPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPicture.Controls.Add(this.pictureBox);
            this.panelPicture.Location = new System.Drawing.Point(12, 12);
            this.panelPicture.Name = "panelPicture";
            this.panelPicture.Size = new System.Drawing.Size(538, 621);
            this.panelPicture.TabIndex = 12;
            // 
            // labelFile
            // 
            this.labelFile.AutoSize = true;
            this.labelFile.Location = new System.Drawing.Point(474, 9);
            this.labelFile.Name = "labelFile";
            this.labelFile.Size = new System.Drawing.Size(23, 13);
            this.labelFile.TabIndex = 8;
            this.labelFile.Text = "flie:";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileName.Location = new System.Drawing.Point(503, 7);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.ReadOnly = true;
            this.textBoxFileName.Size = new System.Drawing.Size(465, 20);
            this.textBoxFileName.TabIndex = 9;
            // 
            // buttonOpenFolder
            // 
            this.buttonOpenFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenFolder.Location = new System.Drawing.Point(974, 5);
            this.buttonOpenFolder.Name = "buttonOpenFolder";
            this.buttonOpenFolder.Size = new System.Drawing.Size(91, 23);
            this.buttonOpenFolder.TabIndex = 10;
            this.buttonOpenFolder.Text = "show in folder";
            this.buttonOpenFolder.UseVisualStyleBackColor = true;
            this.buttonOpenFolder.Click += new System.EventHandler(this.buttonOpenFolder_Click);
            // 
            // CheckingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1173, 674);
            this.Controls.Add(this.panelPicture);
            this.Controls.Add(this.panelImageProperties);
            this.Controls.Add(this.panelTools);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lvCheckings);
            this.KeyPreview = true;
            this.Name = "CheckingsForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Checkings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CheckingsForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckingsForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.panelTools.ResumeLayout(false);
            this.panelTools.PerformLayout();
            this.panelImageProperties.ResumeLayout(false);
            this.panelImageProperties.PerformLayout();
            this.panelPicture.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvCheckings;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderFailDescription;
        private System.Windows.Forms.ColumnHeader columnHeaderRes;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Panel panelTools;
        private System.Windows.Forms.Button btnClearResults;
        private System.Windows.Forms.Button btnFixResults;
        private System.Windows.Forms.Label lShow;
        private System.Windows.Forms.RadioButton rbShowDifference;
        private System.Windows.Forms.RadioButton rbShowOriginal;
        private System.Windows.Forms.RadioButton rbShowResult;
        private System.Windows.Forms.RadioButton rbShowEtalon;
        private System.Windows.Forms.Button btnCopyCheckings;
        private System.Windows.Forms.Label lPreCheckedResult;
        private System.Windows.Forms.Label lEtalon;
        private System.Windows.Forms.Label lFix;
        private System.Windows.Forms.Label lDifference;
        private System.Windows.Forms.Label lResult;
        private System.Windows.Forms.Timer timer_Watch;
        private System.Windows.Forms.Panel panelImageProperties;
        private System.Windows.Forms.TextBox textBoxImageHeight;
        private System.Windows.Forms.Label labelImageHeight;
        private System.Windows.Forms.TextBox textBoxImageWidth;
        private System.Windows.Forms.Label labelImageWidth;
        private System.Windows.Forms.Panel panelPicture;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Label lSelectAll;
        private System.Windows.Forms.Label labelSizeMode;
        private System.Windows.Forms.RadioButton radioButtonSizeModeFit;
        private System.Windows.Forms.RadioButton radioButtonSizeModeRealSize;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Label labelFile;
        private System.Windows.Forms.Button buttonOpenFolder;
    }
}