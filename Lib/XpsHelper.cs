﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

using System.Security.Cryptography;

using System.IO;
using System.IO.Packaging;

using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;

using System.Diagnostics;

namespace WBT
{
    public class XpsHelper
    {
        static string SafeNormalizeXml(string content)
        {
            System.Xml.Linq.XDocument doc= System.Xml.Linq.XDocument.Parse(content);
            return doc.ToString();
        }

        static void TraceWrongPart(string fpath, Uri uri, string content, string postfix)
        {
            if (!string.IsNullOrEmpty(postfix))
            {
                string dir = fpath.Replace(postfix,"") + "\\";
                string subpath= (uri.ToString() + postfix).Replace("/","\\");
                string new_path = dir + subpath;
                string new_dir = Path.GetDirectoryName(new_path);
                Directory.CreateDirectory(new_dir);
                File.WriteAllText(new_path, SafeNormalizeXml(content));
            }
        }

        static void TraceWrongPart(string fpath, Uri uri, Stream stream, string postfix)
        {
            if (!string.IsNullOrEmpty(postfix))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    string content = reader.ReadToEnd();
                    TraceWrongPart(fpath,uri,content,postfix);
                }
            }
        }

        static void TraceWrongParts(string fpath1, string fpath2, Uri uri, string content1, string content2, string dif_postfix1, string dif_postfix2)
        {
            TraceWrongPart(fpath1,uri,content1,dif_postfix1);
            TraceWrongPart(fpath2,uri,content2,dif_postfix2);
        }

        static void TraceWrongParts(string fpath1, string fpath2, Uri uri, Stream stream1, Stream stream2, string dif_postfix1, string dif_postfix2)
        {
            TraceWrongPart(fpath1,uri,stream1,dif_postfix1);
            TraceWrongPart(fpath2,uri,stream2,dif_postfix2);
        }

        public static bool CompareXps(string fpath1, string fpath2, Action<string> o, string dif_postfix1, string dif_postfix2)
        {
            bool res= true;
            using (Package package1 = Package.Open(fpath1, FileMode.Open))
            using (Package package2 = Package.Open(fpath2, FileMode.Open))
            {
                foreach (PackagePart part1 in package1.GetParts())
                {
                    if (!package2.PartExists(part1.Uri))
                    {
                        o(string.Format("\"{0}\" contans uri \"{1}\" and \"{2}\" does not contain it!",
                                        Path.GetFileName(fpath1),part1.Uri,Path.GetFileName(fpath2)));
                        res= false;
                    }
                    else
                    {
                        PackagePart part2= package2.GetPart(part1.Uri);
                        using (Stream stream1= part1.GetStream())
                        using (Stream stream2 = part2.GetStream())
                        {
                            if (stream1.Length != stream2.Length)
                            {
                                o(string.Format("({0}{1}).Length({2})!=({3}{4}).Length({5})",
                                    Path.GetFileName(fpath1),part1.Uri,stream1.Length,Path.GetFileName(fpath2),part2.Uri,stream2.Length));
                                TraceWrongParts(fpath1,fpath2,part1.Uri,stream1,stream2,dif_postfix1,dif_postfix2);
                                res= false;
                            }
                            else
                            {
                                using (TextReader reader1= new StreamReader(stream1))
                                using (TextReader reader2 = new StreamReader(stream2))
                                {
                                    string content1= reader1.ReadToEnd();
                                    string content2= reader2.ReadToEnd();
                                    if (content1 != content2)
                                    {
                                        o(string.Format("({0}{1}).content!=({2}{3}).content",
                                            Path.GetFileName(fpath1),part1.Uri,Path.GetFileName(fpath2),part2.Uri));
                                        TraceWrongParts(fpath1,fpath2,part1.Uri,content1,content2,dif_postfix1,dif_postfix2);
                                        res= false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return res;
        }

        public static bool CompareXps(string fpath1, string fpath2)
        {
            return CompareXps(fpath1, fpath2, (s) => { Console.Out.WriteLine(s);},null,null);
        }

        public static bool QuickCompareXps(string fpath1, string fpath2)
        {
            using (Package package1 = Package.Open(fpath1, FileMode.Open))
            using (Package package2 = Package.Open(fpath2, FileMode.Open))
            {
                foreach (PackagePart part1 in package1.GetParts())
                {
                    PackagePart part2= package2.GetPart(part1.Uri);
                    if (null == part2)
                    {
                        return false;
                    }
                    else
                    {
                        using (Stream stream1= part1.GetStream())
                        using (Stream stream2 = part2.GetStream())
                        {
                            if (stream1.Length != stream2.Length)
                            {
                                return false;
                            }
                            else
                            {
                                using (TextReader reader1= new StreamReader(stream1))
                                using (TextReader reader2 = new StreamReader(stream2))
                                {
                                    string content1= reader1.ReadToEnd();
                                    string content2= reader2.ReadToEnd();
                                    if (content1 != content2)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        public static void SaveDocumentPagesToImages(string xpsFileName, string dirPath, string prefix, string postfix)
        {
            using (System.Windows.Xps.Packaging.XpsDocument xpsDoc = new System.Windows.Xps.Packaging.XpsDocument(xpsFileName, System.IO.FileAccess.Read))
            {
                System.Windows.Documents.FixedDocumentSequence docSeq= xpsDoc.GetFixedDocumentSequence();
                SaveDocumentPagesToImages(docSeq,dirPath,prefix,postfix);
            }
        }

        public static void SaveDocumentPageToPng(DocumentPage source, Stream stream)
        {
            RenderTargetBitmap renderTarget =
            new RenderTargetBitmap((int)source.Size.Width,
                                    (int)source.Size.Height,
                                    96, // WPF (Avalon) units are 96dpi based
                                    96,
                                    System.Windows.Media.PixelFormats.Default);
            renderTarget.Render(source.Visual);

            PngBitmapEncoder encoder = new PngBitmapEncoder();  // Choose type here ie: JpegBitmapEncoder, etc
            encoder.Frames.Add(BitmapFrame.Create(renderTarget));
            encoder.Interlace= PngInterlaceOption.Off;
            encoder.Save(stream);

            renderTarget.Clear();
        }

        static void SaveDocumentPageToPng(DocumentPage source, string png_fname)
        {
            using (MemoryStream stream= new MemoryStream())
            {
                SaveDocumentPageToPng(source,stream);
                using (FileStream file = new FileStream(png_fname, FileMode.CreateNew))
                {
                    file.Write(stream.GetBuffer(), 0, (int)stream.Length);
                }
            }
        }

        static void SaveDocumentPagesToImages(IDocumentPaginatorSource document, string dirPath, string prefix, string postfix)
        {
            DocumentPaginator pages= document.DocumentPaginator;
            int pageCount = pages.PageCount;
            for (int i = 0; i < pageCount; i++)
            {
                DocumentPage source = pages.GetPage(i);
                SaveDocumentPageToPng(source,dirPath + prefix + (i + 1) + postfix);
            }
        }

        const string txtOpenedXpsWndTitlePrefix= ".xps - Средство просмотра XPS";

        static Process FindProcessForOpenedXps(string prefix, ref string title)
        {
            Process[] processlist = Process.GetProcesses();
            foreach (Process process in processlist)
            {
                title= process.MainWindowTitle;
                if (!string.IsNullOrEmpty(title) && title.StartsWith(prefix) && title.EndsWith(txtOpenedXpsWndTitlePrefix))
                    return process;
            }
            return null;
        }

        public static string FindOpenedXpsAndCloseWindow(string prefix)
        {
            string title= null;
            Process process= FindProcessForOpenedXps(prefix,ref title);
            if (null == process)
            {
                return null;
            }
            else
            {
                process.Kill();
                return title.Substring(0, title.Length - txtOpenedXpsWndTitlePrefix.Length + 4);
            }
        }
    }
}
