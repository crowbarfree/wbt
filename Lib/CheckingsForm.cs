﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WBT
{
    public partial class CheckingsForm : Form
    {
        Checkings m_Checkings;
        DateTime? m_icd_file_toWatch_Time;
        string m_icd_file_path_toWatch;

        public CheckingsForm()
        {
            InitializeComponent();
        }

        public void Watch(string icd_file_path)
        {
            WBT.Checkings checkings= WBT.Checkings.ReadFromXmlFile(icd_file_path);
            checkings.FixFileNamesAsAbsolutePathForRootPath(Path.GetDirectoryName(icd_file_path));
            m_icd_file_toWatch_Time= File.GetLastWriteTime(icd_file_path);
            m_icd_file_path_toWatch= icd_file_path;
            DataLoad(checkings);
        }

        public void DataLoad(Checkings checkings)
        {
            m_Checkings= checkings;

            ListView.ListViewItemCollection items= lvCheckings.Items;
            items.Clear();
            lvCheckings.SelectedIndices.Clear();
            bool selected= false;
            foreach (IChecking checking in m_Checkings)
            {
                items.Add(CreateListViewItem(checking));
                if (!selected && !string.IsNullOrEmpty(checking.FailDescription))
                {
                    selected= true;
                    lvCheckings.SelectedIndices.Add(items.Count - 1);
                }
            }
            if (!selected && 0<items.Count)
                lvCheckings.SelectedIndices.Add(0);
        }

        private void CheckingsForm_Load(object sender, EventArgs e)
        {
            lvCheckings.Select();
            lvCheckings.Focus();
            if (!string.IsNullOrEmpty(m_icd_file_path_toWatch))
                timer_Watch.Start();
        }

        string GetName(IChecking checking)
        {
            return Path.GetFileName(checking.CheckedFile).Replace(".etalon","");
        }

        ListViewItem CreateListViewItem(IChecking checking)
        {
            string name= GetName(checking);
            return new ListViewItem(new string [] {"",name,checking.FailDescription});
        }

        private void lvCheckings_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        string GetFileNameForChecking(IChecking checking)
        {
            if (rbShowDifference.Checked)
            {
                return checking.DifferenceFile();
            }
            else if (rbShowOriginal.Checked)
            {
                return checking.OrigFile();
            }
            else if (rbShowResult.Checked)
            {
                return checking.ResultFile();
            }
            else
            {
                return checking.CheckedFile;
            }
        }

        void UpdatePictureBox()
        {
            if (0 == lvCheckings.SelectedIndices.Count)
            {
                UnshowShot();
            }
            else
            {
                int i= lvCheckings.SelectedIndices[0];
                IChecking checking= m_Checkings[i];
                string fileName= GetFileNameForChecking(checking);
                if (!File.Exists(fileName))
                {
                    UnshowShot();
                }
                else
                {
                    byte [] image_bytes= File.ReadAllBytes(fileName);
                    using (MemoryStream stream = new MemoryStream(image_bytes))
                    {
                        Image image= Image.FromStream(stream);
                        ShowShot(image, fileName);
                    }
                }
            }
        }

        void UnshowShot()
        {
            pictureBox.Image = null;
            textBoxFileName.Text = string.Empty;
            textBoxImageWidth.Text = string.Empty;
            textBoxImageHeight.Text = string.Empty;
        }

        void ShowShot(Image image, string fileName)
        {
            textBoxFileName.Text = Path.GetFullPath(fileName);
            pictureBox.Image = image;
            textBoxImageWidth.Text = image.Width.ToString();
            textBoxImageHeight.Text = image.Height.ToString();

            if (radioButtonSizeModeRealSize.Checked)
            {
                pictureBox.Size = image.Size;
                pictureBox.SizeMode = PictureBoxSizeMode.Normal;
                pictureBox.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            }
            else if (radioButtonSizeModeFit.Checked)
            {
                pictureBox.Width = panelPicture.Width - 6;
                pictureBox.Height = panelPicture.Height - 6;
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            }
        }

        private void rbShowEtalon_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        private void rbShowResult_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        private void rbShowOriginal_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        private void rbShowDifference_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        bool ProcessShortCut(KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.E: rbShowEtalon.Checked= true; break;
                    case Keys.S: rbShowResult.Checked= true; break;
                    case Keys.D: rbShowDifference.Checked= true; break;
                    case Keys.R: rbShowOriginal.Checked= true; break;
                    case Keys.F: OnFixResults(); break;
                    case Keys.A: SelectAllFailedResults(); break;

                    case Keys.Q: ChaneSizeMode(radioButtonSizeModeRealSize);  break;
                    case Keys.W: ChaneSizeMode(radioButtonSizeModeFit); break;
                }
            }
            return false;
        }

        private void ChaneSizeMode(RadioButton btn)
        {
            btn.Checked = true;
            UpdatePictureBox();
        }

        private void SelectAllFailedResults()
        {
            ListView.SelectedIndexCollection selected = lvCheckings.SelectedIndices;
            selected.Clear();
            foreach (ListViewItem li in lvCheckings.Items)
            {
                string fail_description = li.SubItems[2].Text;
                if (!string.IsNullOrEmpty(fail_description))
                    selected.Add(li.Index);
            }
        }

        private void CheckingsForm_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessShortCut(e);
        }

        private void btnFixResults_Click(object sender, EventArgs e)
        {
            OnFixResults();
        }

        void OnFixResults()
        {
            StringBuilder sb= new StringBuilder();
            sb.AppendFormat("Do you really want to fix etalon for {0} {1}:",
                lvCheckings.SelectedIndices.Count,1==lvCheckings.SelectedIndices.Count ? "checking" : "checkings");
            sb.AppendLine();
            foreach (int i in lvCheckings.SelectedIndices)
            {
                IChecking checking= m_Checkings[i];
                sb.AppendFormat("   - \"{0}\"",GetName(checking));
                sb.AppendLine();
            }
            sb.AppendLine("?");
            if (DialogResult.Yes == MessageBox.Show(this, sb.ToString(), "Are you sure?", MessageBoxButtons.YesNo))
            {
                foreach (int i in lvCheckings.SelectedIndices)
                {
                    IChecking checking= m_Checkings[i];
                    ListViewItem item = lvCheckings.Items[i];
                    item.SubItems[2].Text = "";
                    FixResults(checking);
                }
            }
        }

        private void btnClearResults_Click(object sender, EventArgs e)
        {
            OnClearResults();
        }

        void OnClearResults()
        {
            int i= lvCheckings.SelectedIndices[0];
            IChecking checking= m_Checkings[i];
            string message= string.Format("Do you really want to clear results for \"{0}\"?",GetName(checking));
            if (DialogResult.Yes == MessageBox.Show(this,message,"Are you sure?",MessageBoxButtons.YesNo))
                ClearResults(checking);
        }

        void ClearResults(IChecking checking)
        {
            File.Delete(checking.ResultFile());
            File.Delete(checking.OrigFile());
            File.Delete(checking.DifferenceFile());
        }

        void FixResults(IChecking checking)
        {
            string result_file= checking.ResultFile();
            if (File.Exists(result_file))
                File.Copy(result_file,checking.CheckedFile,true);
            ClearResults(checking);
        }

        private void btnCopyCheckings_Click(object sender, EventArgs e)
        {
            StringBuilder sb= new StringBuilder();
            int i= 0;
            foreach (SignalingChecking checking in m_Checkings)
            {
                bool skip= false;
                for (int j= 0; j<i; j++)
                {
                    if (m_Checkings[j].CheckedFile == checking.CheckedFile)
                    {
                        skip= true;
                        break;
                    }
                }
                i++;
                if (!skip)
                {
                    string[] path_parts = checking.CheckedFile.Split(new char[] { '\\' });
                    sb.Append("    <check>");
                    sb.Append(Path.GetFileName(checking.CheckedFile).Replace(".etalon", ""));
                    sb.AppendLine("</check>");
                }
            }
            Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);
        }

        private void timer_Watch_Tick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_icd_file_path_toWatch) && File.Exists(m_icd_file_path_toWatch))
            {
                DateTime t= File.GetLastWriteTime(m_icd_file_path_toWatch);
                if (t != m_icd_file_toWatch_Time)
                {
                    m_icd_file_toWatch_Time= t;
                    WBT.Checkings checkings= WBT.Checkings.ReadFromXmlFile(m_icd_file_path_toWatch);
                    checkings.FixFileNamesAsAbsolutePathForRootPath(Path.GetDirectoryName(m_icd_file_path_toWatch));
                    DataLoad(checkings);
                }
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            SelectAllFailedResults();
        }

        private void radioButtonSizeModeRealSize_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        private void radioButtonSizeModeFit_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePictureBox();
        }

        private void buttonOpenFolder_Click(object sender, EventArgs e)
        {
            string filename = textBoxFileName.Text;
            if (!string.IsNullOrEmpty(filename))
            {
                string args = string.Format("/select,\"{0}\"", filename);
                System.Diagnostics.Process.Start("explorer.exe", args);
            }
        }
    }
}
