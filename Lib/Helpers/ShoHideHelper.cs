﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace WBT
{
    internal class ShotHideHelper
    {
        internal Dictionary<string,int> classNames= new Dictionary<string,int>();
        internal Dictionary<string,int> elementIds= new Dictionary<string,int>();
        internal List<KeyValuePair<string,string>> attributeValues= new List<KeyValuePair<string,string>>();

        internal void Clear()
        {
            classNames.Clear();
            attributeValues.Clear();
            elementIds.Clear();
        }

        internal void Hide(WebBrowser browser, Graphics graphics)
        {
            foreach (HtmlElement element in browser.Document.All)
            {
                if (IsToHide(element))
                    HideElement(browser,graphics,element);
            }
        }

        bool IsToHide(HtmlElement element)
        {
            if (classNames.ContainsKey(HtmlHelper.GetElementClass(element)))
                return true;

            if (null!=element.Id && elementIds.ContainsKey(element.Id))
                return true;

            foreach (KeyValuePair<string,string> attr in attributeValues)
            {
                string value= element.GetAttribute(attr.Key);
                if (value == attr.Value)
                    return true;
            }

            return false;
        }

        void HideElement(WebBrowser browser, Graphics graphics, HtmlElement element)
        {
            graphics.FillRectangle(Brushes.LightGray,HtmlHelper.GetAbsoluteRectangle(element));
        }

        public void AddClassName(string className)
        {
            if (!classNames.ContainsKey(className))
                classNames.Add(className,1);
        }

        public void AddId(string element_id)
        {
            if (!elementIds.ContainsKey(element_id))
                elementIds.Add(element_id,1);
        }

        public void AddAttributeValue(string name, string value)
        {
            attributeValues.Add(new KeyValuePair<string,string>(name,value));
        }
    }
}
