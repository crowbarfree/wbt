﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    public class Arguments
    {
        static ILog log = LogManager.GetLogger(typeof(Arguments));

        public const string ExeFileName = "wbt.exe";
        public static bool MinimizeWindowInBatchMode = true;

        public static FormWindowState WhichWindowState(IPlayer player)
        {
            if (MinimizeWindowInBatchMode)
            {
                if (player.BatchMode)
                {
                    return FormWindowState.Minimized;
                }
                else
                {
                    pipe.IPlayer pipe_player = player as pipe.IPlayer;
                    if (null!= pipe_player && pipe_player.PipeServerMode)
                        return FormWindowState.Minimized;
                }
            }
            return FormWindowState.Normal;
        }

        public static void PrintUsage()
        {
            StringBuilder txt = new StringBuilder();
            txt.AppendLine(ExeFileName);
            txt.AppendLine(" - Программа для тестирования WEB сайтов во встроенном IE WebBrowser (с поддержкой ActiveX)");
            txt.Append("Использование: ");
            txt.Append(ExeFileName);
            txt.AppendLine(" [опции]");
            txt.Append(@"опции могут принимать значения:
-h       Вывести эту подсказку.
-b       Работать в пакетном режиме.
-m       Не минимизировать окно в пакетном режиме
-i       Работать в интерактивном режиме.
-e       Повторять введённые команды на консоль в режиме ""эхо"".
-c=path  Читать команды из файла path.
-d=path  Использовать папку path как базовую для входных и выходных данных.
-bu=url  Использовать url как базовый.
-pg=url  Загрузить сначала страницу url.
-pe=path Выполнить сначала path.
-il=path Сохранить перечень сделанных снимков в файле path
-uf      Использовать протокол file (опцию BaseUrl)
-uh      Использовать протокол http (опцию BaseUrlHTTP)
");
            MessageBox.Show(txt.ToString());
        }

        interface IArgumentParameterProcessor
        {
            string prefix { get; }
            void ProcessParameter(string par);
        }

        static string ParameterOfArgument(string arg, string prefix)
        {
            return ! prefix.EndsWith("=") 
                    ? (arg!=prefix ? null : string.Empty)
                    : (!arg.StartsWith(prefix) ? null : arg.Substring(prefix.Length));
        }

        static bool ProcessParametrizedArgument(string arg, IArgumentParameterProcessor[] processors)
        {
            foreach (IArgumentParameterProcessor processor in processors)
            {
                string parameter = ParameterOfArgument(arg, processor.prefix);
                if (null != parameter)
                {
                    processor.ProcessParameter(parameter);
                    return true;
                }
            }
            return false;
        }

        public class PP : IArgumentParameterProcessor
        {
            public delegate void ProcessParameterDelegate(string txt);

            string _prefix;
            public ProcessParameterDelegate _delegate;

            public string prefix { get { return _prefix; } }
            public void ProcessParameter(string txt)
            {
                _delegate(txt);
            }

            public PP(string aprefix, ProcessParameterDelegate ad)
            {
                _prefix = aprefix;
                _delegate = ad;
            }
        }

        public static bool ProcessArguments(System.Collections.Generic.IEnumerable<string> args, pipe.IPlayer player, Settings settings= null)
        {
            try
            {
                IArgumentParameterProcessor[] processors = new IArgumentParameterProcessor[]
                {
                    new PP("-h",delegate(string txt){Arguments.PrintUsage();})
                    ,new PP("-c=",delegate(string txt){player.ScenarioPath= txt;})
                    ,new PP("-b",delegate(string txt){player.BatchMode= true;})
                    ,new PP("-m",delegate(string txt){MinimizeWindowInBatchMode= false;})
                    ,new PP("-e",delegate(string txt){player.EchoMode= true;})
                    ,new PP("-i",delegate(string txt){player.BatchMode= false;})
                    ,new PP("-ps",delegate(string txt){player.PipeServerMode= true;})
                    ,new PP("-d=",delegate(string txt){player.PathForResultFiles= txt;})
                    ,new PP("-bu=",delegate(string txt){player.BaseUrl= txt;})
                    ,new PP("-pg=",delegate(string txt){player.PreUrl= txt;})
                    ,new PP("-pe=",delegate(string txt){player.PreCall= txt;})
                    ,new PP("-pe",delegate(string txt){player.PreCall= null;})
                    ,new PP("-il=",delegate(string txt){player.CheckingFile= txt;})

                    ,new PP("-uf",delegate(string txt){use_protocol= "file";})
                    ,new PP("-uh",delegate(string txt){use_protocol= "http";})
                };
                foreach (string arg in args)
                {
                    if (!ProcessParametrizedArgument(arg, processors))
                        ProcessSettings(arg, player, settings);
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error("При обработке аргументов ошибка:", ex);
                return false;
            }
        }

        static string use_protocol = "file";
        static void ProcessSettings(string settings_file_path, IPlayer helper, Settings settings)
        {
            if (null== settings)
                settings = Settings.SafeReadFromFile(settings_file_path);
            ProcessSettings(settings_file_path, settings, helper);
        }

        static void ProcessSettings(string settings_file_path, Settings settings, IPlayer helper)
        {
            helper.BaseUrl = settings.BaseUrlFor(use_protocol);
            helper.PreUrl = settings.PreUrl;
            if (string.IsNullOrEmpty(helper.PreCall))
                helper.PreCall = settings.PreCall;
            helper.ScenarioPath = settings.Scenario;
            helper.PathForResultFiles = Path.GetDirectoryName(settings_file_path);
            helper.PathToIncludeFrom = null;
        }
    }
}
