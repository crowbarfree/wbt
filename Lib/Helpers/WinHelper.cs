﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;

namespace WBT
{
    internal class WinHelper
    {
        [DllImport("User32.dll", EntryPoint = "FindWindow", CharSet = CharSet.Auto)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        internal static extern int SendMessage(IntPtr hWnd, uint msg, int wParam, IntPtr lParam);

        internal const uint WM_COMMAND = 0x0111;
        internal const uint BM_CLICK = 0x00F5;
        internal const int BN_CLICKED = 245;
        internal const int IDOK = 1;

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool SetWindowText(IntPtr hwnd, String lpString);

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int uMsg, int wParam, string lParam);

        [DllImport("kernel32.dll")]
        internal static extern uint SleepEx(uint dwMilliseconds, bool bAlertable);

        [DllImport("user32.dll",CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        private const int MOUSEEVENTF_LBUTTONDBLCLK = 0x203;
        public const int MOUSEEVENTF_ABSOLUTE = 0x8000;
        public const int MOUSEEVENTF_MOVE = 0x0001;

        public static void DoMouseDoubleClick(uint x, uint y)
        {
            mouse_event(MOUSEEVENTF_LBUTTONDBLCLK, x, y, 0, 0);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll")]
        public static extern void SetCursorPos(int x, int y);

        public static int KEYEVENTF_EXTENDEDKEY = 0x0001;
        public static int KEYEVENTF_KEYUP = 0x0002;

        [DllImport("user32.dll")]
        public static extern void keybd_event(int bVk, int bScan, int dwFlags, int dwExtraInfo);
    }

    public struct RECT
    {
        public int Left;        // x position of upper-left corner
        public int Top;         // y position of upper-left corner
        public int Right;       // x position of lower-right corner
        public int Bottom;      // y position of lower-right corner
    }

    [ComImport]
    [InterfaceType( ComInterfaceType.InterfaceIsIUnknown )]
    [Guid( "0000010d-0000-0000-C000-000000000046" )]
    internal interface IViewObject
    {
        [PreserveSig]
        int Draw(   [In] [MarshalAs( UnmanagedType.U4 )] int dwDrawAspect, int lindex, IntPtr pvAspect,
                    [In] /*tagDVTARGETDEVICE*/ IntPtr ptd, IntPtr hdcTargetDev, IntPtr hdcDraw,
                    [In] /*COMRECT*/ Rectangle lprcBounds, [In] /*COMRECT*/ IntPtr lprcWBounds, IntPtr pfnContinue,
                    [In] int dwContinue );
 
        [PreserveSig]
        int GetColorSet([In] [MarshalAs( UnmanagedType.U4 )] int dwDrawAspect, int lindex, IntPtr pvAspect,
                        [In] /*tagDVTARGETDEVICE*/ IntPtr ptd, IntPtr hicTargetDev, [Out] /*tagLOGPALETTE*/ IntPtr ppColorSet );
 
        [PreserveSig]
        int Freeze( [In] [MarshalAs( UnmanagedType.U4 )] int dwDrawAspect, int lindex, IntPtr pvAspect, [Out] IntPtr pdwFreeze );
 
        [PreserveSig]
        int Unfreeze( [In] [MarshalAs( UnmanagedType.U4 )] int dwFreeze );
 
        void SetAdvise( [In] [MarshalAs( UnmanagedType.U4 )] int aspects, [In] [MarshalAs( UnmanagedType.U4 )] int advf,
                        [In] [MarshalAs( UnmanagedType.Interface )] /*IAdviseSink*/ IntPtr pAdvSink );
 
        void GetAdvise( [In] [Out] [MarshalAs( UnmanagedType.LPArray )] int[] paspects,
                        [In] [Out] [MarshalAs( UnmanagedType.LPArray )] int[] advf,
                        [In] [Out] [MarshalAs( UnmanagedType.LPArray )] /*IAdviseSink[]*/ IntPtr[] pAdvSink );
    }

    public static class myPrinters
    {
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Name);

        public static string GetDefaultPrinter()
        {
            System.Drawing.Printing.PrinterSettings settings = new System.Drawing.Printing.PrinterSettings();
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }
    }
}
