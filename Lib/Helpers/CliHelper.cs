﻿using System;
using System.Runtime.InteropServices;

namespace WBT
{
    public class CommandLineHelper
    {
        [DllImport("shell32.dll", SetLastError = true)]
        static extern IntPtr CommandLineToArgvW(
            [MarshalAs(UnmanagedType.LPWStr)] string lpCmdLine, out int pNumArgs);

        public static string[] CommandLineToArgs(string commandLine)
        {
            int argc;
            var argv = CommandLineToArgvW(commandLine, out argc);
            if (argv == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception();
            try
            {
                var args = new string[argc];
                for (var i = 0; i < args.Length; i++)
                {
                    var p = Marshal.ReadIntPtr(argv, i * IntPtr.Size);
                    args[i] = Marshal.PtrToStringUni(p);
                }
                return args;
            }
            finally
            {
                Marshal.FreeHGlobal(argv);
            }
        }

        internal static void GetCommandAndArguments(string line, out string cmd, 
            out string arg1, out string arg2, out string arg3, out string arg4, out string arg5, out string arg6)
        {
            string [] line_parts= CommandLineToArgs(line.Trim());
            cmd = line_parts[0];
            arg1 = line_parts.Length > 1 ? line_parts[1] : null;
            arg2 = line_parts.Length > 2 ? line_parts[2] : null;
            arg3 = line_parts.Length > 3 ? line_parts[3] : null;
            arg4 = line_parts.Length > 4 ? line_parts[4] : null;
            arg5 = line_parts.Length > 5 ? line_parts[5] : null;
            arg6 = line_parts.Length > 6 ? line_parts[6] : null;
        }
    }
}
