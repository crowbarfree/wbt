﻿using System;
using System.Drawing;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    public class HtmlHelper
    {
        static ILog log= LogManager.GetLogger(typeof(HtmlHelper));

        public static Rectangle GetAbsoluteRectangle(HtmlElement element)
        {
            Rectangle rect = element.OffsetRectangle;
            HtmlElement currParent = element.OffsetParent;
            while (currParent != null)
            {
                rect.Offset(currParent.OffsetRectangle.Left, currParent.OffsetRectangle.Top);
                currParent = currParent.OffsetParent;
            }
            return rect;
        }

        public static HtmlElement FindLastElementWithText(WebBrowser browser, string txt)
        {
            HtmlDocument doc = browser.Document;
            HtmlElementCollection elements = doc.All;
            for (int i= elements.Count-1; i>=0; i--)
            {
                HtmlElement element = elements[i];
                if (null == element.Children || 0 == element.Children.Count)
                {
                    if (null != element.InnerText && element.InnerText.Contains(txt) &&
                        element.OffsetRectangle.Width != 0 && element.OffsetRectangle.Height != 0)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static HtmlElement FindFirstElementWithText(WebBrowser browser, string txt)
        {
            HtmlDocument doc = browser.Document;
            HtmlElementCollection elements = doc.All;
            foreach (HtmlElement element in elements)
            {
                if (null == element.Children || 0 == element.Children.Count)
                {
                    if (null != element.InnerText && element.InnerText.Contains(txt) &&
                        element.OffsetRectangle.Width != 0 && element.OffsetRectangle.Height != 0)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static HtmlElement FindTrWithText(WebBrowser browser, string txt)
        {
            HtmlDocument doc= browser.Document;
            HtmlElementCollection elements= doc.All;
            foreach (HtmlElement element in elements)
            {
                if ("TR"==element.TagName.ToUpper())
                {
                    if (null != element.InnerText && element.InnerText.Contains(txt) && 
                        element.OffsetRectangle.Width != 0 && element.OffsetRectangle.Height != 0)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static bool ElementIsVisibleInherited(HtmlElement element)
        {
            return ElementIsVisible(element) && (null==element.Parent || ElementIsVisibleInherited(element.Parent));
        }

        public static bool ElementIsVisible(HtmlElement element)
        {
            string style= element.Style;
            return string.IsNullOrEmpty(style) || !style.Replace(" ","").Contains("display:none");
        }

        public static HtmlElement FindElementWithIdValue(WebBrowser browser, string element_id, string value)
        {
            HtmlDocument doc= browser.Document;
            HtmlElementCollection elements= doc.All;
            foreach (HtmlElement element in elements)
            {
                if (element.Id==element_id)
                {
                    string evalue= GetElementValue(element);
                    if (evalue==value)
                        return element;
                }
            }
            return null;
        }

        public static HtmlElement FindLastElementWithFullText(WebBrowser browser, string txt)
        {
            HtmlDocument doc= browser.Document;
            HtmlElementCollection elements= doc.All;
            for (int i = elements.Count - 1; i >= 0; i--)
            {
                HtmlElement element = elements[i];
                if (null == element.Children || 0==element.Children.Count)
                {
                    if (null != element.InnerText && element.InnerText== txt &&
                        element.OffsetRectangle.Width != 0 && element.OffsetRectangle.Height != 0)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static HtmlElement FindLastElementWithHiddenFullText(WebBrowser browser, string txt)
        {
            HtmlDocument doc = browser.Document;
            HtmlElementCollection elements = doc.All;
            for (int i = elements.Count - 1; i >= 0; i--)
            {
                HtmlElement element = elements[i];
                if (null == element.Children || 0 == element.Children.Count)
                {
                    if (null != element.InnerText && element.InnerText == txt)
                    {
                        return element;
                    }
                }
            }
            return null;
        }

        public static string GetElementValue(HtmlElement element)
        {
            return element.GetAttribute("value");
        }

        public static string GetElementClass(HtmlElement element)
        {
            return element.GetAttribute("className");
        }

        public static HtmlElement FindElementWithValue(WebBrowser browser, string txt)
        {
            HtmlDocument doc= browser.Document;
            HtmlElementCollection elements= doc.All;
            foreach (HtmlElement element in elements)
            {
                if (null==element.FirstChild)
                {
                    string value= GetElementValue(element);
                    if (null != value && value.Contains(txt))
                        return element;
                }
            }
            return null;
        }

        public static HtmlElement FindElementWithClass(WebBrowser browser, string txt)
        {
            HtmlDocument doc= browser.Document;
            HtmlElementCollection elements= doc.All;
            foreach (HtmlElement element in elements)
            {
                string value= GetElementClass(element);
                if (null != value && value.Contains(txt))
                    return element;
            }
            return null;
        }

        public static void ClickElement(HtmlElement element)
        {
            element.InvokeMember("click");
        }

        public static void FixLinksColor(WebBrowser browser, bool fix_links_important)
        {
            try
            {
                HtmlDocument document = browser.Document;
                HtmlElement head = document.GetElementsByTagName("head")[0];

                HtmlElement style = document.CreateElement("style");
                style.InnerText = !fix_links_important ? "a:visited{ color:#009fdb; }" : "a:visited{ color:#009fdb !important; }";
                head.AppendChild(style);

                style = document.CreateElement("style");
                string itext= !fix_links_important ? "a{ color:#009fdb; }" : "a{ color:#009fdb !important; }";
                style.InnerText= itext;
                head.AppendChild(style);
            }
            catch (Exception e)
            {
                log.Error("can not FixLinksColor",e);
            }
        }

        public static void KeydownEnter(WebBrowser browser, HtmlElement element)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text",@"function WBT_KeydownEnter(id)
{
    var element= document.getElementById(id);
    var e = jQuery.Event('keydown');
    e.which = 13;
    e.keyCode = 13;
    $(element).trigger(e);
}");
            head.AppendChild(script);
            document.InvokeScript("WBT_KeydownEnter",new object []{element.Id});
        }

        public static void ExecuteJavascript(WebBrowser browser, string txt)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text","function WBT_ExecuteLines(){" + txt + "}");
            head.AppendChild(script);
            document.InvokeScript("WBT_ExecuteLines");
        }

        public static string ExecuteJavascriptLineReturnResult(WebBrowser browser, string txt)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text","function WBT_ExecuteLines(){return " + txt + "}");
            head.AppendChild(script);
            object res= document.InvokeScript("WBT_ExecuteLines");
            return null==res ? "res" : res.ToString();
        }

        public static bool CheckJS(WebBrowser browser, string txt)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text","function WBT_ExecuteLines(){return " + txt + "}");
            head.AppendChild(script);
            return (bool)document.InvokeScript("WBT_ExecuteLines");
        }

        public static string ExecuteJS(WebBrowser browser, string txt, bool check_mode)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text",
                string.Format("function WBT_ExecuteLines(){{wbt_CheckMode= {1}; return {0}}}",txt,check_mode?"true":"false"));
            head.AppendChild(script);
            object res= document.InvokeScript("WBT_ExecuteLines");
            return null==res ? "#null" : res.ToString();
        }

        public static void DoubleClickElement(WebBrowser browser, HtmlElement element)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text","function WBT_DoubleClick(id) { var element= document.getElementById(id); $(element).click(); $(element).dblclick();}");
            head.AppendChild(script);
            document.InvokeScript("WBT_DoubleClick",new object []{element.Id});
            //element.RaiseEvent("ondblclick");
            /*Rectangle rect= GetAbsoluteRectangle(element);
            rect.X+= browser.DisplayRectangle.Left;
            rect.Y+= browser.DisplayRectangle.Top;
            uint x= (uint)((rect.Left + rect.Right)/2);
            uint y= (uint)((rect.Top + rect.Bottom)/2);*/
        }

        public static void SetAttrId(WebBrowser browser, HtmlElement element, string name, string value)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text",@"
function WBT_SetAttr(id,name,value)
{
    var element= document.getElementById(id);
    $(element).attr(name,value).change();
}");
            head.AppendChild(script);
            document.InvokeScript("WBT_SetAttr",new object []{element.Id,name,value});
        }

        public static string ExecuteJsFunction(WebBrowser browser, string js_function_name)
        {
            HtmlDocument document= browser.Document;
            object res= document.InvokeScript(js_function_name,new object []{});
            return null==res ? null : res.ToString();
        }

        public static string ExecuteJsFunctionWithArg(WebBrowser browser, string js_function_name, string arg)
        {
            HtmlDocument document= browser.Document;
            object res= document.InvokeScript(js_function_name,new object []{arg});
            return null==res ? null : res.ToString();
        }

        public static void TypeElement(WebBrowser browser, HtmlElement element, string txt)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text",@"
function WBT_TypeText(id,txt)
{
    $('#' + id)
        .val(txt)
        .keydown()
        .keyup()
        .change();
}");
            head.AppendChild(script);
            document.InvokeScript("WBT_TypeText",new object []{element.Id,txt});

            /*element.InnerText= txt;
            element.InvokeMember("click");
            element.SetAttribute("value",txt);
            browser.Document.InvokeScript(*/
        }

        public static void SetValueElement(WebBrowser browser, HtmlElement element, string value)
        {
            HtmlDocument document= browser.Document;
            HtmlElement head = document.GetElementsByTagName("head")[0];
            HtmlElement script= document.CreateElement("script");
            script.SetAttribute("text","function WBT_SetValue(id,value) { var element= document.getElementById(id); element.value= value; $(element).change();}");
            head.AppendChild(script);
            document.InvokeScript("WBT_SetValue",new object []{element.Id,value});
        }
    }
}
