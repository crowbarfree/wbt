﻿using System;
using System.Text;
using System.IO;
using System.IO.Pipes;

using log4net;

namespace WBT
{
    internal class NamedPipeApiHelper
    {
        static ILog log = LogManager.GetLogger(typeof(NamedPipeApiHelper));

        byte [] m_Buffer= new byte[10000];
        StringBuilder m_StringBuilder= new StringBuilder();
        NamedPipeServerStream m_Stream;
        StreamWriter m_Writer;
        pipe.IPlayer m_Helper;

        Action<string[], pipe.IPlayer> m_ProcessArguments;

        internal NamedPipeApiHelper(pipe.Player helper, Action<string[], pipe.IPlayer> ProcessArguments)
        {
            m_Helper = helper;
            m_ProcessArguments = ProcessArguments;
        }

        internal void StartListenPipe()
        {
            m_Stream = new NamedPipeServerStream("wbt", PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
            m_Stream.BeginWaitForConnection(OnConnect, m_Stream);
        }

        internal void ReStartListenPipe()
        {
            m_Writer.Flush();
            m_StringBuilder.Length = 0;
            m_Stream.Disconnect();
            m_Stream.BeginWaitForConnection(OnConnect, m_Stream);
        }

        public void OnConnect(IAsyncResult iar)
        {
            m_Stream.EndWaitForConnection(iar);
            BeginRead();
        }

        public void BeginRead()
        {
            m_Stream.BeginRead(m_Buffer, 0, m_Buffer.Length, OnRead, m_Stream);
        }

        public void OnRead(IAsyncResult iar)
        {
            int read= m_Stream.EndRead(iar);
            if (0 < read)
            {
                string txt = Encoding.UTF8.GetString(m_Buffer, 0, read);
                m_StringBuilder.Append(txt);
                if (!m_Stream.IsMessageComplete)
                {
                    BeginRead();
                }
                else
                {
                    string[] lines = m_StringBuilder.ToString().Split('\n');
                    string message_line = lines[0];
                    message_line = message_line.Trim();
                    message_line = message_line.TrimEnd('\0', '\n', '\r');
                    OnMessageLine(message_line);
                }
            }
        }

        public void OnMessageLine(string message_line)
        {
            string[] args = CommandLineHelper.CommandLineToArgs(message_line);

            m_Helper.Clear();
            if (null != m_ProcessArguments)
                m_ProcessArguments.Invoke(args, m_Helper);

            m_Helper.StartScenario();
        }

        public void WriteLine(string line)
        {
            if (null == m_Writer)
                m_Writer = new StreamWriter(m_Stream);
            m_Writer.WriteLine(line);
            m_Writer.Flush();
        }
    }
}
