﻿using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace WBT
{
    public class Settings
    {
        public string BaseUrl       { get; set; }
        public string BaseUrlHTTP   { get; set; }

        public string PreUrl        { get; set; }
        public string Scenario      { get; set; }
        public string PreCall       { get; set; }
        public bool   UseIni        { get; set; }

        public string UseProtocol   { get; set; }

        public string ExecutableFilePrefix { get; set; }

        public static Settings ReadFromFile(string file_path)
        {
            JavaScriptSerializer serializer= new JavaScriptSerializer();
            Settings res= serializer.Deserialize<Settings>(File.ReadAllText(file_path));
            res.Fix(file_path);
            return res;
        }

        public static Settings SafeReadFromFile(string settings_file_path)
        {
            if (!Path.IsPathRooted(settings_file_path))
                settings_file_path = Path.Combine(Directory.GetCurrentDirectory(), settings_file_path);
            return Settings.ReadFromFile(settings_file_path);
        }

        public string BaseUrlFor(string use_protocol)
        {
            return "file" == use_protocol
                ? (!string.IsNullOrEmpty(BaseUrl) ? BaseUrl : BaseUrlHTTP)
                : (!string.IsNullOrEmpty(BaseUrlHTTP) ? BaseUrlHTTP : BaseUrl);
        }

        void Fix(string file_path)
        {
            string dir_path= Path.GetDirectoryName(file_path) + "\\";
            if (!string.IsNullOrEmpty(BaseUrl))
                BaseUrl = BaseUrl.Replace("%test_dir%",dir_path);
            if (!string.IsNullOrEmpty(PreCall))
                PreCall= PreCall.Replace("%test_dir%", dir_path);
            if (!string.IsNullOrEmpty(ExecutableFilePrefix))
                ExecutableFilePrefix = ExecutableFilePrefix.Replace("%test_dir%", dir_path);
            if (string.IsNullOrEmpty(Scenario))
                Scenario= "in.txt";
            if (string.IsNullOrEmpty(PreUrl))
                PreUrl= ".";
            if (UseIni)
            {
                Ini_info ini_info = Ini_info.SafeReadForDir(dir_path);
                if (null!= ini_info)
                {
                    BaseUrl = ini_info.BaseUrl;
                    BaseUrlHTTP = ini_info.BaseUrlHTTP;
                    UseProtocol = ini_info.UseProtocol;
                    ExecutableFilePrefix = ini_info.ExecutableFilePrefix;
                }
            }
            if ("http" != UseProtocol)
                UseProtocol = "file";
        }
    }

    public class Ini_info
    {
        public string BaseUrl       { get; set; }
        public string BaseUrlHTTP   { get; set; }
        public string UseProtocol   { get; set; }
        public string ExecutableFilePrefix { get; set; }

        const string ini_file_name = "wbt.ini";

        static Dictionary<string, string> ini_files_for_dirs = new Dictionary<string, string>();
        public static string find_ini_file_path(string dir_path, string root_path = null)
        {
            string ini_file_path;
            if (ini_files_for_dirs.TryGetValue(dir_path, out ini_file_path))
            {
                return ini_file_path;
            }
            else
            {
                ini_file_path = Path.Combine(dir_path, ini_file_name);
                if (File.Exists(ini_file_path))
                {
                    ini_files_for_dirs.Add(dir_path, ini_file_path);
                    return ini_file_path;
                }
                else
                {
                    if (null == root_path)
                        root_path = Path.GetPathRoot(dir_path);
                    ini_file_path= (root_path == dir_path)
                        ? null
                        : find_ini_file_path(Directory.GetParent(dir_path).FullName, root_path);
                    if (null!= ini_file_path)
                        ini_files_for_dirs.Add(dir_path, ini_file_path);
                    return ini_file_path;
                }
            }
        }

        public static Ini_info ReadFromFile(string file_path)
        {
            string txt_Ini_info = File.ReadAllText(file_path);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Ini_info res = serializer.Deserialize<Ini_info>(txt_Ini_info);
            res.Fix(file_path);
            return res;
        }

        public static Ini_info SafeReadForDir(string dir_path)
        {
            string ini_file_path = Ini_info.find_ini_file_path(dir_path);
            if (null == ini_file_path)
            {
                return null;
            }
            else
            {
                try
                {
                    return Ini_info.ReadFromFile(ini_file_path);
                }
                catch
                {
                    List<string> keys_to_delete= new List<string>();
                    foreach (KeyValuePair<string, string> dir_ini in ini_files_for_dirs)
                    {
                        if (ini_file_path == dir_ini.Value)
                            keys_to_delete.Add(dir_ini.Key);
                    }
                    foreach (string key in keys_to_delete)
                        ini_files_for_dirs.Remove(key);
                }
                ini_file_path = Ini_info.find_ini_file_path(dir_path);
                return (null == ini_file_path) 
                    ? null
                    : Ini_info.ReadFromFile(ini_file_path);
            }
        }

        void Fix(string file_path)
        {
            string dir_path = Path.GetDirectoryName(file_path) + "\\";
            if (!string.IsNullOrEmpty(BaseUrl))
                BaseUrl = BaseUrl.Replace("%file_dir%", dir_path);
            if (!string.IsNullOrEmpty(ExecutableFilePrefix))
                ExecutableFilePrefix = ExecutableFilePrefix.Replace("%file_dir%", dir_path);
        }
    }
}
