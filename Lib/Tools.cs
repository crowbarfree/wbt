﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using log4net;

namespace WBT
{
    public static class Tools
    {
        public static void Bind_player_controls(Form frm, WebBrowser browser, IEnumerable<string> args= null, Action OnRestart= null)
        {
            pipe.Player player = new pipe.Player(browser, frm, (_args, _player) => { Arguments.ProcessArguments(_args, _player); });
            Arguments.ProcessArguments(args,player);
            player.OnRestart = OnRestart;

            player.SafePreCall();

            frm.Load += (obj, eventArgs) =>
            {
                IGrizzly grizzly = new Grizzly();
                grizzly.Bind_and_Show(frm, browser, player);

                DevPanelForm dev_form = new DevPanelForm();
                dev_form.Bind(player, grizzly, browser);
                dev_form.Relocate_for(frm);
                dev_form.Show();

                player.StartScenario();
            };
        }
    }
}
