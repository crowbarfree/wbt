﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WBT
{
    public class StoredLines
    {
        public string nameToStoreLines;
        public Dictionary<string,List<string>> linesByName= new Dictionary<string,List<string>>();
        bool m_quiet= false;

        public void Clear()
        {
            nameToStoreLines= null;
            linesByName.Clear();
        }

        public void StartStoreLineAs(string name, bool quiet)
        {
            m_quiet= quiet;
            if (!m_quiet)
                Log(string.Format("start store lines as \"{0}\"",name));
            nameToStoreLines= name;
        }

        public bool SafeStoreLine(string line)
        {
            if (string.IsNullOrEmpty(nameToStoreLines))
            {
                return false;
            }
            else
            {
                StoreLine(line);
                return true;
            }
        }

        public void StoreLine(string line)
        {
            List<string> slines;
            if (!linesByName.TryGetValue(nameToStoreLines, out slines))
            {
                slines= new List<string>();
                linesByName.Add(nameToStoreLines,slines);
            }
            if ("stop_store_lines" != line.Trim())
            {
                slines.Add(line);
            }
            else
            {
                StringBuilder sb= new StringBuilder();
                foreach (string sline in slines)
                    sb.AppendLine(sline);
                if (!m_quiet)
                {
                    Log(@"stored lines:
" + sb.ToString());
                }
                nameToStoreLines= null;
            }
        }

        public Action<string> Log;
    }
}
