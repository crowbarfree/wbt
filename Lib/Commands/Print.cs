﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace WBT
{
    public partial class Player
    {
        bool PressDialogButton(string dlg_caption, string dlg_name_for_log, string btn_caption, string btn_name_for_log, string btn_id)
        {
            object ms_waited;
            IntPtr hwnd= IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate() 
                {
                    hwnd= WinHelper.FindWindow(null, dlg_caption);
                    return (IntPtr.Zero != hwnd);
                });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds",dlg_name_for_log,ms_waited);
                return false;
            }
            else
            {
                Log("found {0} dialog",dlg_name_for_log);
                IntPtr hwnd_button= IntPtr.Zero;
                checked_ok = WaitFor(out ms_waited, delegate()
                    {
                        hwnd_button= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "Button", btn_caption);
                        if (IntPtr.Zero == hwnd_button)
                            hwnd_button= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, null, btn_caption);
                        return (IntPtr.Zero != hwnd_button);
                    });
                if (!checked_ok)
                {
                    Log("can not find {0} button for {1} milliseconds",btn_name_for_log,ms_waited);
                    return false;
                }
                else
                {
                    Log("found {0} button",btn_name_for_log);
                    int ibtn_id= string.IsNullOrEmpty(btn_id) ? WinHelper.IDOK : int.Parse(btn_id);
                    //if (0!=WinHelper.SendMessage(hwnd, WinHelper.WM_COMMAND, (WinHelper.BN_CLICKED << 16) | (ibtn_id & 0xffff), hwnd_button))
                    if (0!=WinHelper.SendMessage(hwnd_button, WinHelper.BM_CLICK, 0, IntPtr.Zero))
                    {
                        Log("can not press {0} button",btn_name_for_log);
                        return false;
                    }
                    else
                    {
                        Thread.Sleep(25);
                        Log("pressed {0} button",btn_name_for_log);
                        return true;
                    }
                }
            }
        }

        bool PressPrintCancel()
        {
            return PressDialogButton("Печать", "print", "Отмена", "cancel", null);
        }

        bool PressPrintPrint()
        {
            return PressDialogButton("Печать", "print", "&Печать", "print", null);
        }

        bool EnterRandomTextDialogTextBox(string dlg_caption, string box_caption, string box_class)
        {
            object ms_waited;
            IntPtr hwnd = IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
            {
                hwnd = WinHelper.FindWindow(null, dlg_caption);
                return (IntPtr.Zero != hwnd);
            });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds", dlg_caption, ms_waited);
                return true;
            }
            else
            {
                Log("found {0} dialog", dlg_caption);
                IntPtr hwnd_textbox = IntPtr.Zero;
                checked_ok = WaitFor(out ms_waited, delegate()
                {
                    hwnd_textbox = WinHelper.FindWindowEx(hwnd, IntPtr.Zero, box_class, box_caption);
                    return (IntPtr.Zero != hwnd_textbox);
                });
                if (!checked_ok)
                {
                    Log("can not find {0} textbox with \"{1}\" for {2} milliseconds", box_class, ms_waited);
                    return true;
                }
                else
                {
                    Log("found {0} with \"{1}\" textbox", box_class, box_caption);
                    if (!WinHelper.SetForegroundWindow(hwnd_textbox))
                    {
                        Log("can not set path edit as foreground");
                    }
                    else
                    {
                        return EnterTextWhileNotOpenDialog(hwnd, dlg_caption);
                    }
                    return true;
                }
            }
        }

        bool EnterTextWhileNotOpenDialog(IntPtr hwnd, string dlg_caption)
        {
            int max_attempt = 500;
            int attempt = 0;
            IntPtr hwnd_new = IntPtr.Zero;
            int timeout = 200;
            do
            {
                string key = attempt % 2 == 0 ? "q" : "w";
                SendKeys.SendWait(key);
                Thread.Sleep(timeout);
                hwnd_new = WinHelper.FindWindow(null, dlg_caption);
                attempt++;
            } while (hwnd_new == hwnd && attempt < max_attempt);
            if (hwnd_new == hwnd)
            {
                Log("can not find {0} dialog for {1} attempt", dlg_caption, attempt);
            }
            return hwnd_new == hwnd;
        }

        bool FindDialogWithElementText(string dlg_caption, string elem_class, string elem_text)
        {
            object ms_waited;
            IntPtr hwnd = IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
            {
                hwnd = WinHelper.FindWindow(null, dlg_caption);
                return (IntPtr.Zero != hwnd);
            });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds", dlg_caption, ms_waited);
                return true;
            }
            else
            {
                Log("found {0} dialog", dlg_caption);
                IntPtr hwnd_textbox = IntPtr.Zero;
                checked_ok = WaitFor(out ms_waited, delegate()
                {
                    hwnd_textbox = WinHelper.FindWindowEx(hwnd, IntPtr.Zero, elem_class, elem_text);
                    return (IntPtr.Zero != hwnd_textbox);
                });
                if (!checked_ok)
                {
                    Log("can not find {0} textbox with \"{1}\" for {2} milliseconds", elem_class, elem_text, ms_waited);
                    return true;
                }
                else
                {
                    Log("found {0} text", dlg_caption);
                    return false;
                }
            }
        }

        bool EnterTextDialogTextBox(string dlg_caption, string box_class, string text)
        {
            object ms_waited;
            IntPtr hwnd = IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
            {
                hwnd = WinHelper.FindWindow(null, dlg_caption);
                return (IntPtr.Zero != hwnd);
            });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds", dlg_caption, ms_waited);
                return true;
            }
            else
            {
                Log("found {0} dialog", dlg_caption);
                IntPtr hwnd_textbox = IntPtr.Zero;
                checked_ok = WaitFor(out ms_waited, delegate()
                {
                    hwnd_textbox = WinHelper.FindWindowEx(hwnd, IntPtr.Zero, box_class, string.Empty);
                    return (IntPtr.Zero != hwnd_textbox);
                });
                if (!checked_ok)
                {
                    Log("can not find {0} textbox with \"{1}\" for {2} milliseconds", box_class, ms_waited);
                    return true;
                }
                else
                {
                    Log("found {0} textbox", box_class);
                    if (!WinHelper.SetForegroundWindow(hwnd_textbox))
                    {
                        Log("can not set path edit as foreground");
                    }
                    else
                    {
                        SendKeys.SendWait(text);
                        return false;
                    }
                    return true;
                }
            }
        }

        bool EnterPathForFileDialog(string dlg_type, string dlg_title, string fspath)
        {
            string fpath = (string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath)).Replace("\\\\","\\");
            if (!EnterPathForFileDialog(fpath, dlg_title, "file", "open" == dlg_type ? "&Открыть" : "Со&хранить", "ok"))
            {
                return true;
            }
            else
            {
                Log("ok enter path \"{0}\"",fspath);
                return false;
            }
        }

        bool EnterPathForFileDialog(string fpath, string dlg_title, string dlg_name_for_log, string btn_title, string btn_name_for_log)
        {
            object ms_waited;
            IntPtr hwnd= IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(new TimeSpan(0,0,60), out ms_waited, delegate() 
                {
                    hwnd= WinHelper.FindWindow(null, dlg_title);
                    return (IntPtr.Zero != hwnd);
                });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds", dlg_name_for_log, ms_waited);
            }
            else
            {
                IntPtr hwnd_button_ok= IntPtr.Zero;
                checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                    {
                        hwnd_button_ok= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "Button", btn_title);
                        return (IntPtr.Zero != hwnd_button_ok);
                    });
                if (!checked_ok)
                {
                    Log("can not find {1} button for {0} milliseconds", ms_waited, btn_name_for_log);
                }
                else
                {
                    IntPtr hwnd_cboxex32= IntPtr.Zero;
                    checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            hwnd_cboxex32= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "ComboBoxEx32", "");
                            return (IntPtr.Zero != hwnd_cboxex32);
                        });
                    if (!checked_ok)
                    {
                        Log("can not find cbox for path for {0} milliseconds", ms_waited);
                    }
                    else
                    {
                        IntPtr hwnd_combobox= IntPtr.Zero;
                        IntPtr hwnd_edit= IntPtr.Zero;
                        checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            hwnd_combobox= WinHelper.FindWindowEx(hwnd_cboxex32, IntPtr.Zero, "ComboBox", "");
                            if (IntPtr.Zero != hwnd_combobox)
                                hwnd_edit= WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                            if (IntPtr.Zero != hwnd_edit)
                            {
                                return true;
                            }
                            else
                            {
                                hwnd_combobox= WinHelper.FindWindowEx(hwnd_cboxex32, IntPtr.Zero, "ComboBox", "");
                                if (IntPtr.Zero != hwnd_combobox)
                                    hwnd_edit= WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                                return (IntPtr.Zero != hwnd_edit);
                            }
                        });
                        if (!checked_ok)
                        {
                            Log("can not find edit for path for {0} milliseconds", ms_waited);
                        }
                        else
                        {
                            if (!WinHelper.SetForegroundWindow(hwnd_edit))
                            {
                                Log("can not set path edit as foreground");
                            }
                            else
                            {
                                SendKeys.SendWait(fpath);
                                SendKeys.SendWait("{ENTER}");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        bool EnterMultiOpenForFileDialog(string dlg_title, string fspath)
        {
            string[] fspaths = fspath.Split(':');
            string[] fpaths = new string[fspaths.Length];
            for (var i = 0; i < fspaths.Length; i++)
            {
                fpaths[i] = (string.IsNullOrEmpty(PathForResultFiles) ? fspaths[i] : Path.Combine(PathForResultFiles, fspaths[i])).Replace("\\\\", "\\");
            }
            if (!EnterMultiOpenForFileDialog(fpaths, dlg_title, "file", "&Открыть", "ok"))
            {
                return true;
            }
            else
            {
                Log("ok enter files \"{0}\"", fpaths);
                return false;
            }
        }

        bool EnterMultiOpenForFileDialog(string[] fpaths, string dlg_title, string dlg_name_for_log, string btn_title, string btn_name_for_log)
        {
            object ms_waited;
            IntPtr hwnd = IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(new TimeSpan(0, 0, 60), out ms_waited, delegate()
            {
                hwnd = WinHelper.FindWindow(null, dlg_title);
                return (IntPtr.Zero != hwnd);
            });
            if (!checked_ok)
            {
                Log("can not find {0} dialog for {1} milliseconds", dlg_name_for_log, ms_waited);
            }
            else
            {
                IntPtr hwnd_button_ok = IntPtr.Zero;
                checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                {
                    hwnd_button_ok = WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "Button", btn_title);
                    return (IntPtr.Zero != hwnd_button_ok);
                });
                if (!checked_ok)
                {
                    Log("can not find {1} button for {0} milliseconds", ms_waited, btn_name_for_log);
                }
                else
                {
                    IntPtr hwnd_cboxex32 = IntPtr.Zero;
                    checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                    {
                        hwnd_cboxex32 = WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "ComboBoxEx32", "");
                        return (IntPtr.Zero != hwnd_cboxex32);
                    });
                    if (!checked_ok)
                    {
                        Log("can not find cbox for path for {0} milliseconds", ms_waited);
                    }
                    else
                    {
                        IntPtr hwnd_combobox = IntPtr.Zero;
                        IntPtr hwnd_edit = IntPtr.Zero;
                        checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            hwnd_combobox = WinHelper.FindWindowEx(hwnd_cboxex32, IntPtr.Zero, "ComboBox", "");
                            if (IntPtr.Zero != hwnd_combobox)
                                hwnd_edit = WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                            if (IntPtr.Zero != hwnd_edit)
                            {
                                return true;
                            }
                            else
                            {
                                hwnd_combobox = WinHelper.FindWindowEx(hwnd_cboxex32, IntPtr.Zero, "ComboBox", "");
                                if (IntPtr.Zero != hwnd_combobox)
                                    hwnd_edit = WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                                return (IntPtr.Zero != hwnd_edit);
                            }
                        });
                        if (!checked_ok)
                        {
                            Log("can not find edit for path for {0} milliseconds", ms_waited);
                        }
                        else
                        {
                            if (!WinHelper.SetForegroundWindow(hwnd_edit))
                            {
                                Log("can not set path edit as foreground");
                            }
                            else
                            {
                                SendKeys.SendWait("\"" + string.Join("\" \"", fpaths) + "\"");
                                SendKeys.SendWait("{ENTER}");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        bool EnterPathForSaveAs(string fpath)
        {
            System.Version version = Environment.OSVersion.Version;
            int winVersion = Environment.OSVersion.Version.Major;
            string title = (version.Major < 6 || version.Major == 6 && version.Minor < 2)
                ? "Сохранить файл как" : "Сохранение результата печати";

            object ms_waited;
            IntPtr hwnd= IntPtr.Zero;
            bool checked_ok = WaitFor_WithoutInvoke(new TimeSpan(0,0,60), out ms_waited, delegate() 
                {
                    hwnd= WinHelper.FindWindow(null, title);
                    return (IntPtr.Zero != hwnd);
                });
            if (!checked_ok)
            {
                Log("can not find SaveAs dialog for {0} milliseconds", ms_waited);
            }
            else
            {
                IntPtr hwnd_button_ok= IntPtr.Zero;
                checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                    {
                        hwnd_button_ok= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "Button", "Со&хранить");
                        return (IntPtr.Zero != hwnd_button_ok);
                    });
                if (!checked_ok)
                {
                    Log("can not find Save button for {0} milliseconds", ms_waited);
                }
                else
                {
                    IntPtr hwnd_panel1= IntPtr.Zero;
                    checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            hwnd_panel1= WinHelper.FindWindowEx(hwnd, IntPtr.Zero, "DUIViewWndClassName", "");
                            return (IntPtr.Zero != hwnd_panel1);
                        });
                    if (!checked_ok)
                    {
                        Log("can not find panel1 for xps path for {0} milliseconds", ms_waited);
                    }
                    else
                    {
                        IntPtr hwnd_panel2= IntPtr.Zero;
                        checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            hwnd_panel2= WinHelper.FindWindowEx(hwnd_panel1, IntPtr.Zero, "DirectUIHWND", "");
                            return (IntPtr.Zero != hwnd_panel2);
                        });
                        if (!checked_ok)
                        {
                            Log("can not find panel2 for xps path for {0} milliseconds", ms_waited);
                        }
                        else
                        {
                            IntPtr hwnd_panel3= IntPtr.Zero;
                            checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                            {
                                hwnd_panel3= WinHelper.FindWindowEx(hwnd_panel2, IntPtr.Zero, "FloatNotifySink", "");
                                return (IntPtr.Zero != hwnd_panel3);
                            });
                            if (!checked_ok)
                            {
                                Log("can not find panel3 for xps path for {0} milliseconds", ms_waited);
                            }
                            else
                            {
                                IntPtr hwnd_panel32= IntPtr.Zero;
                                checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                                {
                                    hwnd_panel32= WinHelper.FindWindowEx(hwnd_panel2, hwnd_panel3, "FloatNotifySink", "");
                                    return (IntPtr.Zero != hwnd_panel32);
                                });
                                if (!checked_ok)
                                {
                                    Log("can not find panel3 2 for xps path for {0} milliseconds", ms_waited);
                                }
                                else
                                {
                                    IntPtr hwnd_combobox= IntPtr.Zero;
                                    IntPtr hwnd_edit= IntPtr.Zero;
                                    checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                                    {
                                        hwnd_combobox= WinHelper.FindWindowEx(hwnd_panel3, IntPtr.Zero, "ComboBox", "");
                                        if (IntPtr.Zero != hwnd_combobox)
                                            hwnd_edit= WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                                        if (IntPtr.Zero != hwnd_edit)
                                        {
                                            return true;
                                        }
                                        else
                                        {
                                            hwnd_combobox= WinHelper.FindWindowEx(hwnd_panel32, IntPtr.Zero, "ComboBox", "");
                                            if (IntPtr.Zero != hwnd_combobox)
                                                hwnd_edit= WinHelper.FindWindowEx(hwnd_combobox, IntPtr.Zero, "Edit", "");
                                            return (IntPtr.Zero != hwnd_edit);
                                        }
                                    });
                                    if (!checked_ok)
                                    {
                                        Log("can not find edit for xps path for {0} milliseconds", ms_waited);
                                    }
                                    else
                                    {
                                        log.DebugFormat("found path field edit {0}", hwnd_edit);
                                        Application.DoEvents();
                                        Thread.Sleep(125);
                                        Application.DoEvents();
                                        WinHelper.SendMessage(hwnd_edit, 0x000C, 0, fpath);
                                        Application.DoEvents();
                                        Thread.Sleep(25);
                                        if (0 != WinHelper.SendMessage(hwnd_button_ok, WinHelper.BM_CLICK, 0, IntPtr.Zero))
                                        {
                                            Log("can not press {0} button", "Save");
                                            return false;
                                        }
                                        else
                                        {
                                            Thread.Sleep(25);
                                            Application.DoEvents();
                                            Thread.Sleep(25);
                                            log.DebugFormat("pressed {0} button", "Save");
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        bool IsFileUsedbyAnotherProcess(string filename)
        {
            try
            {
                using (File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None)) { }
            }
            catch (System.IO.IOException)
            {
                return true;
            }
            return false; 
        }

        string PrepareTestFileName(string [] parts, string substr)
        {
            StringBuilder sb= new StringBuilder();
            for (int i= 0; i<parts.Length-1; i++)
            {
                sb.Append(parts[i]);
                sb.Append(".");
            }
            sb.Append(substr);
            if (parts.Length>1)
            {
                sb.Append(".");
                sb.Append(parts[parts.Length-1]);
            }
            return sb.ToString();
        }

        void WriteToDiffFile(string fname, ref StreamWriter writer, string line)
        {
            if (null == writer)
                writer= new StreamWriter(fname);
            writer.WriteLine(line);
        }

        internal class WindowOpener : IDisposable
        {
            Form m_MainForm;
            FormWindowState oldWindowState;
            internal WindowOpener(Form mainForm)
            {
                m_MainForm= mainForm;

                m_MainForm.Invoke(new MethodInvoker(delegate{
                    oldWindowState= m_MainForm.WindowState;
                    m_MainForm.WindowState= FormWindowState.Normal;
                    //m_MainForm.SafeUnprotectWebBrowser();
                }));
            }

            public void Dispose()
            {
                m_MainForm.Invoke(new MethodInvoker(delegate{
                    //m_MainForm.SafeProtectWebBrowser();
                    m_MainForm.WindowState= oldWindowState;
                }));
            }
        }

        bool PrintCancel()
        {
            using (new WindowOpener(form))
            {
                return PrintCancelInternal();
            }
        }

        bool PrintCheckXps(string fspath)
        {
            using (new WindowOpener(form))
            {
                string result_fpath = PrepareTestResultPath(fspath);

                PrintXpsAbsPath(result_fpath);

                Log("check png pages from xps");
                if (!File.Exists(result_fpath))
                {
                    Log("can not find file \"{0}\"", result_fpath);
                    return false;
                }
                else
                {
                    try
                    {
                        Checkings.PreCheckXps(result_fpath);
                        return true;
                    }
                    catch (Exception exception)
                    {
                        log.ErrorFormat("exception on checking file: \"{0}\"",result_fpath);
                        throw exception;
                    }
                }
            }
        }

        bool CheckOpenedXps(string prefix)
        {
            string fname= null;
            object ms_waited;
            bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
            {
                fname= WBT.XpsHelper.FindOpenedXpsAndCloseWindow(prefix);
                return !string.IsNullOrEmpty(fname);
            });
            if (!checked_ok)
            {
                Log("can not find xps viewer with prefix \"{1}\" for {0} milliseconds",ms_waited,prefix);
                return false;
            }
            else
            {
                string tmp_result_fpath= Path.Combine(Path.GetTempPath(),fname);
                string result_fpath= Path.Combine(PathForResultFiles,prefix + ".xps");
                File.Delete(result_fpath);
                File.Move(tmp_result_fpath,result_fpath);

                Log("check png pages from xps");
                if (!File.Exists(result_fpath))
                {
                    Log("can not find file \"{0}\"",result_fpath);
                    return false;
                }
                else
                {
                    Checkings.PreCheckXps(result_fpath);
                    return true;
                }
            }
        }

        public class DisposableDefaultPrinter : IDisposable
        {
            string oldDefaultPrinter;
            internal DisposableDefaultPrinter(string name)
            {
                oldDefaultPrinter= myPrinters.GetDefaultPrinter();
                if (!myPrinters.SetDefaultPrinter("Microsoft XPS Document Writer"))
                    oldDefaultPrinter= null;
            }
            public void Dispose()
            {
                if (!string.IsNullOrEmpty(oldDefaultPrinter))
                {
                    myPrinters.SetDefaultPrinter(oldDefaultPrinter);
                    oldDefaultPrinter= null;
                }
            }
        }

        IDisposable DefaultPrinter(string name)
        {
            Log("set default printer \"Microsoft XPS Document Writer\"");
            return new DisposableDefaultPrinter(name);
        }

        bool PrintCancelInternal()
        {
            using (DefaultPrinter("Microsoft XPS Document Writer"))
            {
                if (PressPrintCancel())
                {
                    Log("ok cancel print");
                    return true;
                }
            }
            return false;
        }

        bool PrintXpsAbsPathInternal(string fpath)
        {
            using (DefaultPrinter("Microsoft XPS Document Writer"))
            {
                if (PressPrintPrint())
                {
                    if (!EnterPathForSaveAs(fpath))
                    {
                        Log("printing to xps is unsuccessefull");
                    }
                    else
                    {
                        object ms_waited;
                        bool checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                        {
                            return File.Exists(fpath) && 0!=(new FileInfo(fpath)).Length;
                        });
                        if (!checked_ok)
                        {
                            Log("can not store xps file for {0} milliseconds",ms_waited);
                        }
                        else
                        {
                            Thread.Sleep(25);
                            checked_ok = WaitFor_WithoutInvoke(out ms_waited, delegate()
                            {
                                return !IsFileUsedbyAnotherProcess(fpath);
                            });
                            if (!checked_ok)
                            {
                                Log("can not wait to open xps file for {0} milliseconds",ms_waited);
                            }
                            else
                            {
                                Log("store xps file");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        bool PrintXpsAbsPath(string fpath)
        {
            File.Delete(fpath);
            if (File.Exists(fpath))
                Log("can not delete file \"{0}\"",fpath);
            return PrintXpsAbsPathInternal(fpath);
        }

        string PrepareTestResultPath(string fspath)
        {
            string fpath= string.IsNullOrEmpty(PathForResultFiles) ? fspath : Path.Combine(PathForResultFiles,fspath);
            fpath= fpath.Replace("\\\\","\\");
            return fpath;
        }

        void PrintXps(string fspath)
        {
            PrintXpsAbsPath(PrepareTestResultPath(fspath));
        }
    }
}