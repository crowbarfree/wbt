﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

using log4net;

namespace wbt_runner
{
    class Program
    {
        const string wbt_exe_path = "wbt.exe";
        const int milliseconds_to_connect1 = 10;
        const int milliseconds_to_connect2 = 30000;

        static ILog log = LogManager.GetLogger(typeof(Program));

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            log4net.Util.LogLog.QuietMode = true;
            log4net.Config.XmlConfigurator.Configure();
            Console.OutputEncoding = Encoding.Default;
            try
            {
                using (NamedPipeClientStream stream = new NamedPipeClientStream(".", "wbt", PipeDirection.InOut, PipeOptions.Asynchronous))
                {
                    try
                    {
                        stream.Connect(milliseconds_to_connect1);
                    }
                    catch (TimeoutException)
                    {
                        log.Error("TimeoutException: can not conect, let's start wbt..");
                    }
                    catch (IOException)
                    {
                        log.Error("IOException: can not conect, let's start wbt..");
                    }
                    if (!stream.IsConnected)
                    {
                        start_wbt();
                        stream.Connect(milliseconds_to_connect2);
                    }
                    execute_scenario_using_wbt_pipe(args, stream);
                }
                return 0;
            }
            catch (Exception ex)
            {
                Console.Out.Write(ex);
                log.Error(ex);
                return -1;
            }
        }

        private const uint SW_MINIMIZE = 0x06;
        [DllImport("USER32.DLL")]
        public static extern int ShowWindow(uint hWnd, uint cmd);
        static void start_wbt()
        {
            Process process = new Process();

            ProcessStartInfo sinfo = process.StartInfo;
            sinfo.FileName = wbt_exe_path;
            sinfo.Arguments = "-ps";
            sinfo.UseShellExecute = true;

            if (!process.Start())
            {
                Console.Error.WriteLine("can not start wbt!");
                log.Error("can not start wbt!");
            }

            ShowWindow((uint)process.MainWindowHandle, SW_MINIMIZE);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            log.Error("CurrentDomain_UnhandledException");
            log.ErrorFormat("ExceptionObject={0}", e.ExceptionObject);
        }

        static void execute_scenario_using_wbt_pipe(string[] args, NamedPipeClientStream stream)
        {
            StreamWriter writer = new StreamWriter(stream);
            {
                string args_line = string.Join(" ", args);
                writer.WriteLine(args_line);
                string dir= Environment.CurrentDirectory;
                writer.WriteLine(dir);
                writer.Flush();
            }

            StreamReader reader = new StreamReader(stream);
            {
                string txt = reader.ReadToEnd();
                string [] lines = txt.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                if (0 != lines.Length)
                {
                    int lines_count = lines.Length;
                    for (var i = 0; i < lines_count; i++)
                    {
                        string line = lines[i];
                        if (i!=(lines_count-1) || !string.IsNullOrEmpty(line))
                            Console.Out.WriteLine(line);
                    }
                }
            }
        }
    }
}
